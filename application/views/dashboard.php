<?php
if (isset($_POST['reset1'])) {
    unset($_SESSION['sess_mth']);
    unset($_SESSION['sess_yar']);
    echo '<script>
    window.location.href = "' . base_url('dashboard') . '"
  </script>';
}

if (isset($_POST['search1'])) {
    $_SESSION['sess_mth'] = $_POST['month'];
    $_SESSION['sess_yar'] = $_POST['year'];
}

if (isset($_POST['reset2'])) {
    unset($_SESSION['sess_cat']);
    unset($_SESSION['sess_mth2']);
    unset($_SESSION['sess_yar2']);
    echo '<script>
    window.location.href = "' . base_url('dashboard') . '"
  </script>';
}

if (isset($_POST['search2'])) {
    $_SESSION['sess_cat'] = $_POST['category'];
    $_SESSION['sess_mth2'] = $_POST['month2'];
    $_SESSION['sess_yar2'] = $_POST['year2'];
}

?>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <?php
                            if ($pwd == 'bbf2dead374654cbb32a917afd236656') {
                            ?>
                                <div class="col-md-12 col-xl-12">
                                    <div class="card">
                                        <div class="card-block">
                                            <h4 class="mb-4"><i class="feather icon-alert-triangle m-r-5" style="color: #f44236;"></i>WARNING</h4>
                                            <div class="alert alert-danger" align="center" role="alert">
                                                <h5 style="color: #994442;">Please <b>edit the password</b> to use this features !! <br>
                                                    After set new password, <b>automatic system restart and login again.</b><br></h5>
                                                <button type="button" class="btn btn-dark m-t-10" title="Click Here" data-toggle="modal" data-target="#modalEditPass"><i class="feather icon-navigation"></i>Click Here</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            } elseif ($sess_level == 'admin') {
                            ?>
                                <div class="col-md-6 col-xl-5">
                                    <div class="card daily-sales">
                                        <div class="card-block">
                                            <h6 class="mb-4">TOTAL ORDER 
                                            <?php 
                                            // $_SERVER['HTTP_REFERER'] 
                                            
                                            ?>
                                            </h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order/pending') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Pending<?= '<b class="m-l-5">' . $odr_pending . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order/proses') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Proses<?= '<b class="m-l-5">' . $odr_proses . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order/spora') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Spora<?= '<b class="m-l-5">' . $odr_press . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order/ossgform') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Oss/Gform<?= '<b class="m-l-5">' . $odr_ossgform . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order/portfull') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Port Full<?= '<b class="m-l-5">' . $odr_portfull . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order/paid') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Paid<?= '<b class="m-l-5">' . $odr_paid . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order/unpaid') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Unpaid<?= '<b class="m-l-5">' . $odr_unpaid . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order/cancel') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Cancel<?= '<b class="m-l-5">' . $odr_cancel . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order/presales') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Presales<?= '<b class="m-l-5">' . $odr_presales . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order/reject') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Reject<?= '<b class="m-l-5">' . $odr_reject . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order/complete') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Complete<?= '<b class="m-l-5">' . $odr_complete . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="progress m-b-10" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <i class="feather icon-info m-r-10"></i><?= 'Total <b>' . $odr_total . '</b>' ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-5">
                                    <div class="card daily-sales">
                                        <div class="card-block">
                                            <h6 class="mb-4">TOTAL CREDENTIAL</h6>
                                            <div class="row d-flex align-items-center mb-3">
                                                <div class="col-9">
                                                    <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-user-check text-c-green f-20 m-r-20"></i>Admin<?= '<b class="m-l-5">' . $crd_admin . '</b>' ?></h4>
                                                </div>
                                            </div>

                                            <div class="row d-flex align-items-center mb-3">
                                                <div class="col-9">
                                                    <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-users text-c-green f-20 m-r-20"></i>Leader<?= '<b class="m-l-5">' . $crd_leader . '</b>' ?></h4>
                                                </div>
                                            </div>

                                            <div class="row d-flex align-items-center mb-3">
                                                <div class="col-9">
                                                    <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-users text-c-green f-20 m-r-20"></i>Sales<?= '<b class="m-l-5">' . $crd_user . '</b>' ?></h4>
                                                </div>
                                            </div>
                                            <div class="progress m-b-10" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <i class="feather icon-info m-r-10"></i><?= 'Total <b>' . $crd_total . '</b>' ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-xl-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>SALES REPORT USER</h5>
                                        </div>
                                        <div class="card-body">
                                            <?= form_open('search-sales-report', 'method="post"') ?>
                                            <div class="row mb-3">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <select class="form-control" name="month">
                                                            <option value="">Select Month</option>
                                                            <option value="1" <?php if ($this->session->sess_mth == '1') echo 'selected'; ?>>Januari</option>
                                                            <option value="2" <?php if ($this->session->sess_mth == '2') echo 'selected'; ?>>February</option>
                                                            <option value="3" <?php if ($this->session->sess_mth == '3') echo 'selected'; ?>>March</option>
                                                            <option value="4" <?php if ($this->session->sess_mth == '4') echo 'selected'; ?>>April</option>
                                                            <option value="5" <?php if ($this->session->sess_mth == '5') echo 'selected'; ?>>May</option>
                                                            <option value="6" <?php if ($this->session->sess_mth == '6') echo 'selected'; ?>>June</option>
                                                            <option value="7" <?php if ($this->session->sess_mth == '7') echo 'selected'; ?>>July</option>
                                                            <option value="8" <?php if ($this->session->sess_mth == '8') echo 'selected'; ?>>August</option>
                                                            <option value="9" <?php if ($this->session->sess_mth == '9') echo 'selected'; ?>>September</option>
                                                            <option value="10" <?php if ($this->session->sess_mth == '10') echo 'selected'; ?>>October</option>
                                                            <option value="11" <?php if ($this->session->sess_mth == '11') echo 'selected'; ?>>November</option>
                                                            <option value="12" <?php if ($this->session->sess_mth == '12') echo 'selected'; ?>>Desember</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <select class="form-control" name="year">
                                                            <option value="">Select Year</option>
                                                            <?php
                                                            $year = date('Y');
                                                            for ($i = 2015; $i <= $year; $i++) {
                                                            ?>
                                                                <option value="<?= $i ?>" <?php if ($this->session->sess_yar == $i) echo 'selected'; ?>><?= $i ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary" name="search1" title="Search">Search</button>
                                                    <button type="submit" class="btn btn-danger" name="reset1" title="Reset">Reset</button>
                                                </div>
                                            </div>
                                            <?= form_close() ?>
                                            <?php
                                            if ($c_sales_all->num_rows() < 1) {
                                                echo '<div class="alert alert-danger">No Data Match..</div>';
                                            }
                                            ?>
                                            <canvas id="chartSales"></canvas>
                                            <?php
                                            $nama = "";
                                            $total = null;
                                            foreach ($c_sales_all->result() as $item) {
                                                $nm = $item->name;
                                                $nama .= "'$nm'" . ", ";
                                                $tot = $item->total;
                                                $total .= "$tot" . ", ";
                                            }
                                            ?>
                                            <script>
                                                var ctx = document.getElementById('chartSales').getContext('2d');
                                                var chart = new Chart(ctx, {
                                                    // The type of chart we want to create
                                                    type: 'bar',
                                                    // The data for our dataset
                                                    data: {
                                                        labels: [<?= strtoupper($nama) ?>],
                                                        datasets: [{
                                                            label: 'UNIFI SALES REPORT',
                                                            barThickness: 50,
                                                            // backgroundColor: ['rgb(255, 99, 132)', 'rgba(56, 86, 255, 0.87)', 'rgb(60, 179, 113)', 'rgb(175, 238, 239)'],
                                                            backgroundColor: 'rgb(63, 77, 103)',
                                                            borderColor: ['rgb(63, 77, 103)'],
                                                            data: [<?= $total; ?>]
                                                        }]
                                                    },
                                                    // Configuration options go here
                                                    options: {
                                                        scales: {
                                                            yAxes: [{
                                                                ticks: {
                                                                    beginAtZero: true
                                                                }
                                                            }]
                                                        }
                                                    }
                                                });
                                            </script>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-xl-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>SALES REPORT BY CATEGORY PRODUCT</h5>
                                        </div>
                                        <div class="card-body">
                                            <?= form_open('search-sales-report-category', 'method="post"') ?>
                                            <div class="row mb-3">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <!-- <select class="form-control" name="category" required oninvalid="this.setCustomValidity('Please Select Category')" oninput="this.setCustomValidity('')"> -->
                                                        <select class="form-control" name="category">
                                                            <option value="">Select Category</option>
                                                            <?php
                                                            foreach ($category as $cat) {
                                                            ?>
                                                                <option value="<?= $cat->id ?>" <?php if ($this->session->sess_cat == $cat->id) {
                                                                                                    echo 'selected';
                                                                                                } ?>><?= $cat->name ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <!-- <select class="form-control" name="month" required oninvalid="this.setCustomValidity('Please Select Month')" oninput="this.setCustomValidity('')"> -->
                                                        <select class="form-control" name="month2">
                                                            <option value="">Select Month</option>
                                                            <option value="1" <?php if ($this->session->sess_mth2 == '1') echo 'selected'; ?>>Januari</option>
                                                            <option value="2" <?php if ($this->session->sess_mth2 == '2') echo 'selected'; ?>>February</option>
                                                            <option value="3" <?php if ($this->session->sess_mth2 == '3') echo 'selected'; ?>>March</option>
                                                            <option value="4" <?php if ($this->session->sess_mth2 == '4') echo 'selected'; ?>>April</option>
                                                            <option value="5" <?php if ($this->session->sess_mth2 == '5') echo 'selected'; ?>>May</option>
                                                            <option value="6" <?php if ($this->session->sess_mth2 == '6') echo 'selected'; ?>>June</option>
                                                            <option value="7" <?php if ($this->session->sess_mth2 == '7') echo 'selected'; ?>>July</option>
                                                            <option value="8" <?php if ($this->session->sess_mth2 == '8') echo 'selected'; ?>>August</option>
                                                            <option value="9" <?php if ($this->session->sess_mth2 == '9') echo 'selected'; ?>>September</option>
                                                            <option value="10" <?php if ($this->session->sess_mth2 == '10') echo 'selected'; ?>>October</option>
                                                            <option value="11" <?php if ($this->session->sess_mth2 == '11') echo 'selected'; ?>>November</option>
                                                            <option value="12" <?php if ($this->session->sess_mth2 == '12') echo 'selected'; ?>>Desember</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <!-- <select class="form-control" name="year" oninvalid="this.setCustomValidity('Please Select Year')" oninput="this.setCustomValidity('')"> -->
                                                        <select class="form-control" name="year2">
                                                            <option value="">Select Year</option>
                                                            <?php
                                                            $year = date('Y');
                                                            for ($i = 2015; $i <= $year; $i++) {
                                                            ?>
                                                                <option value="<?= $i ?>" <?php if ($this->session->sess_yar2 == $i) echo 'selected'; ?>><?= $i ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary" name="search2" title="Search">Search</button>
                                                    <button type="submit" class="btn btn-danger" name="reset2" title="Reset">Reset</button>
                                                </div>
                                            </div>
                                            <?= form_close() ?>
                                            <?php
                                            if ($c_cat_all->num_rows() < 1) {
                                                echo '<div class="alert alert-danger">No Data Match..</div>';
                                            }
                                            ?>
                                            <canvas id="chartSalesByCategory"></canvas>
                                            <?php
                                            $nama = "";
                                            $total = null;
                                            foreach ($c_cat_all->result() as $item) {
                                                $nm = $item->name_sales;
                                                $cat_name = ' - (' . $item->name . ')';
                                                $nama .= "'$nm$cat_name'" . ", ";
                                                $tot = $item->total;
                                                $total .= "$tot" . ", ";
                                            }
                                            ?>
                                            <script>
                                                var ctx = document.getElementById('chartSalesByCategory').getContext('2d');
                                                var chart = new Chart(ctx, {
                                                    // The type of chart we want to create
                                                    type: 'bar',
                                                    // The data for our dataset
                                                    data: {
                                                        labels: [<?= strtoupper($nama) ?>],
                                                        datasets: [{
                                                            label: 'UNIFI SALES REPORT BY CATEGORY PRODUCT',
                                                            barThickness: 50,
                                                            // backgroundColor: ['rgb(255, 99, 132)', 'rgba(56, 86, 255, 0.87)', 'rgb(60, 179, 113)', 'rgb(175, 238, 239)'],
                                                            backgroundColor: 'rgb(255, 99, 132)',
                                                            borderColor: ['rgb(63, 77, 103)'],
                                                            data: [<?= $total; ?>]
                                                        }]
                                                    },
                                                    // Configuration options go here
                                                    options: {
                                                        barValueSpacing: 20,
                                                        scales: {
                                                            yAxes: [{
                                                                ticks: {
                                                                    min: 0,
                                                                    // beginAtZero: true
                                                                }
                                                            }]
                                                        }
                                                    }
                                                });
                                            </script>
                                        </div>
                                    </div>
                                </div>

                            <?php
                            } elseif ($sess_level == 'leader') {
                            ?>
                                <div class="col-md-6 col-xl-5">
                                    <div class="card daily-sales">
                                        <div class="card-block">
                                            <h6 class="mb-4">TOTAL ORDER</h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-leader/pending') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Pending<?= '<b class="m-l-5">' . $odr_pending_ldr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-leader/proses') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Proses<?= '<b class="m-l-5">' . $odr_proses_ldr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-leader/spora') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Spora<?= '<b class="m-l-5">' . $odr_press_ldr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-leader/ossgform') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Oss/Gform<?= '<b class="m-l-5">' . $odr_ossgform_ldr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-leader/portfull') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Port Full<?= '<b class="m-l-5">' . $odr_portfull_ldr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-leader/paid') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Paid<?= '<b class="m-l-5">' . $odr_paid_ldr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-leader/unpaid') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Unpaid<?= '<b class="m-l-5">' . $odr_unpaid_ldr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-leader/cancel') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Cancel<?= '<b class="m-l-5">' . $odr_cancel_ldr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-leader/presales') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Presales<?= '<b class="m-l-5">' . $odr_presales_ldr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-leader/reject') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Reject<?= '<b class="m-l-5">' . $odr_reject_ldr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-leader/complete') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Complete<?= '<b class="m-l-5">' . $odr_complete_ldr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="progress m-b-10" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <i class="feather icon-info m-r-10"></i><?= 'Total <b>' . $odr_total_ldr . '</b>' ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php
                            } else {
                            ?>
                                <div class="col-md-6 col-xl-5">
                                    <div class="card daily-sales">
                                        <div class="card-block">
                                            <h6 class="mb-4">TOTAL ORDER</h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-user/pending') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Pending<?= '<b class="m-l-5">' . $odr_pending_usr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-user/proses') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Proses<?= '<b class="m-l-5">' . $odr_proses_usr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-user/spora') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Spora<?= '<b class="m-l-5">' . $odr_press_usr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-user/ossgform') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Oss/Gform<?= '<b class="m-l-5">' . $odr_ossgform_usr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-user/portfull') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Port Full<?= '<b class="m-l-5">' . $odr_portfull_usr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-user/paid') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Paid<?= '<b class="m-l-5">' . $odr_paid_usr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-user/unpaid') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Unpaid<?= '<b class="m-l-5">' . $odr_unpaid_usr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-user/cancel') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Cancel<?= '<b class="m-l-5">' . $odr_cancel_usr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-user/presales') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Presales<?= '<b class="m-l-5">' . $odr_presales_usr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-user/reject') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Reject<?= '<b class="m-l-5">' . $odr_reject_usr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0"><i class="feather icon-clipboard f-20 m-r-20" style="color: #04a9f5;"></i><a href="<?= base_url('view-order-user/complete') ?>" class="tag-order" data-toggle="tooltip" data-placement="top" title="Click To View">Complete<?= '<b class="m-l-5">' . $odr_complete_usr . '</b>' ?></a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="progress m-b-10" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <i class="feather icon-info m-r-10"></i><?= 'Total <b>' . $odr_total_usr . '</b>' ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php
                            }
                            ?>
                        </div>
                        <!-- [ Main Content ] end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- [ Main Content ] end -->

<!-- Modal Edit Password -->
<div class="modal fade" id="modalEditPass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open('save_edit_password', 'method="post"') ?>
            <div class="modal-body">
                <input type="hidden" class="form-control" name="hash" value="<?= $this->session->userdata('hash') ?>">
                <div class="form-group">
                    <label>Default Password</label>
                    <input type="text" class="form-control" value="ABC123" readonly>
                </div>
                <div class="form-group">
                    <label>New Password</label>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" name="password" placeholder="Set New Password" id="password" required oninvalid="this.setCustomValidity('Input new password')" oninput="setCustomValidity('')">
                        <div class="input-group-append">
                            <span class="input-group-text" id="passIcon"></span>
                        </div>
                    </div>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="showPassword" onclick="showPass();">
                    <label class="custom-control-label" for="showPassword">Show Password</label>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="feather icon-x-circle"></i>Cancel</button> -->
                <button type="submit" class="btn btn-primary"><i class="feather icon-check-circle"></i>Update & Restart</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>