<div class="row">
	<div class="m-b-26" style="width: 100%;">
		<label>Package <span style="color: red;">*</span></label>
		<select class="form-control orderSubPackage" name="package" required oninvalid="this.setCustomValidity('Select Package')" oninput="setCustomValidity('')">
			<option value="">Nothing Selected</option>
			<?php
			$get = $this->db->order_by('id_pkg', 'desc')->get_where('tb_package', array('type' => 'S', 'active' => 'Y'))->result();
			foreach ($get as $val) {
			?>
				<option value="<?= $val->id_pkg ?>"><?= $val->package ?></option>
			<?php
			}
			?>
		</select>
	</div>
</div>
<div class="row">
	<div class="m-b-26" style="width: 100%;">
		<label>Sub Package <span style="color: red;">*</span></label>
		<select class="form-control orderSubPackageBefore">
			<option value="">Select Package First</option>
		</select>
		<div class="subPackageList"></div>
	</div>
</div>
<div class="row">
	<div class="wrap-input100 m-b-26">		
		<label>Company Name <span style="color: red;">*</span></label>
		<input class="input100" type="text" name="compy_name" placeholder="Company Name" required oninvalid="this.setCustomValidity('Input Company Name')" oninput="this.setCustomValidity('')">
	</div>		
</div>
<div class="row">
	<div class="wrap-input100 m-b-26">		
		<label>Company Reg No <span style="color: red;">*</span></label>
		<input class="input100" type="text" name="compy_regno" placeholder="Company Reg No" required oninvalid="this.setCustomValidity('Input Company Reg No')" oninput="this.setCustomValidity('')">
	</div>		
</div>
<div class="row">
	<div class="wrap-input100 m-b-26">		
		<label>Director Name <span style="color: red;">*</span></label>
		<input class="input100" type="text" name="dir_name" placeholder="Director Name" required oninvalid="this.setCustomValidity('Input Director Name')" oninput="this.setCustomValidity('')">
	</div>		
</div>
<div class="row">
	<div class="wrap-input100 m-b-26">		
		<label>Director IC <span style="color: red;">*</span></label>
		<input class="input100 numberIc" type="text" name="dir_ic" placeholder="123456-00-1122" maxlength="14"  required oninvalid="this.setCustomValidity('Input Director IC')" oninput="this.setCustomValidity('')">
	</div>		
</div>
<div class="row">
	<small class="red"><strong>Note :</strong> If You are not director please prepare authorization later from your company.</small>
</div>
<div class="row">
	<div class="wrap-input100 m-b-26">		
		<label>Authorisation Person</label>
		<input class="input100" type="text" name="auth_person" placeholder="Authorization Person">
	</div>		
</div>
<div class="row">
	<div class="wrap-input100 m-b-26">		
		<label>IC Number</label>
		<input class="input100 numberIc" type="text" name="auth_ic_number" placeholder="123456-00-1122" maxlength="14">
	</div>		
</div>