<div class="row">
	<div class="m-b-26" style="width: 100%;">
		<label>Package <span style="color: red;">*</span></label>
		<select class="form-control orderSubPackage" name="package" required oninvalid="this.setCustomValidity('Select Package')" oninput="setCustomValidity('')">
			<option value="">Nothing Selected</option>
			<?php
			$get = $this->db->order_by('id_pkg', 'desc')->get_where('tb_package', array('type' => 'C', 'active' => 'Y'))->result();
			foreach ($get as $val) {
			?>
				<option value="<?= $val->id_pkg ?>"><?= $val->package ?></option>
			<?php
			}
			?>
		</select>
	</div>
</div>
<div class="row">
	<div class="m-b-26" style="width: 100%;">
		<label>Sub Package <span style="color: red;">*</span></label>
		<select class="form-control orderSubPackageBefore">
			<option value="">Select Package First</option>
		</select>
		<div class="subPackageList"></div>
	</div>
</div>