<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Report Pdf</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>

                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-md-12 col-xl-12">
                                <div class="accordion" id="accordionExample">
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="mb-0"><a href="#!" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">By Sales</a></h5>
                                        </div>
                                        <div id="collapseOne" class=" card-body collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <?= form_open('report-pdf-by-sales', 'method="post" target="_blank"') ?>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Sales</label>
                                                        <select class="form-control" name="sales">
                                                            <option value="">Select Sales</option>
                                                            <?php
                                                            foreach ($sales->result() as $item) {
                                                                $no = $item->awal . $item->tengah . $item->akhir;
                                                            ?>
                                                                <option value="<?= $item->id ?>"><?= ucwords(strtolower($item->name)) ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Month</label>
                                                        <select class="form-control" name="month">
                                                            <?php
                                                            $month = (isset($_POST['month'])) ? $_POST['month'] : '';
                                                            ?>
                                                            <option value="">Select Month</option>
                                                            <option value="1" <?php if ($month == '1') {
                                                                                    echo 'selected';
                                                                                } ?>>January</option>
                                                            <option value="2" <?php if ($month == '2') {
                                                                                    echo 'selected';
                                                                                } ?>>February</option>
                                                            <option value="3" <?php if ($month == '3') {
                                                                                    echo 'selected';
                                                                                } ?>>March</option>
                                                            <option value="4" <?php if ($month == '4') {
                                                                                    echo 'selected';
                                                                                } ?>>April</option>
                                                            <option value="5" <?php if ($month == '5') {
                                                                                    echo 'selected';
                                                                                } ?>>May</option>
                                                            <option value="6" <?php if ($month == '6') {
                                                                                    echo 'selected';
                                                                                } ?>>June</option>
                                                            <option value="7" <?php if ($month == '7') {
                                                                                    echo 'selected';
                                                                                } ?>>July</option>
                                                            <option value="8" <?php if ($month == '8') {
                                                                                    echo 'selected';
                                                                                } ?>>August</option>
                                                            <option value="9" <?php if ($month == '9') {
                                                                                    echo 'selected';
                                                                                } ?>>September</option>
                                                            <option value="10" <?php if ($month == '10') {
                                                                                    echo 'selected';
                                                                                } ?>>October</option>
                                                            <option value="11" <?php if ($month == '11') {
                                                                                    echo 'selected';
                                                                                } ?>>November</option>
                                                            <option value="12" <?php if ($month == '12') {
                                                                                    echo 'selected';
                                                                                } ?>>December</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Year</label>
                                                        <select class="form-control" name="year">
                                                            <option value="">Select Year</option>
                                                            <?php
                                                            $year = date('Y');
                                                            for ($i = $year; $i >= 2015; $i--) {
                                                            ?>
                                                                <option value="<?= $i ?>" <?php if ($this->session->sess_yar2 == $i) echo 'selected'; ?>><?= $i ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <button class="btn btn-info mt-4" type="submit" title="Export To Pdf"><i class="feather icon-file-text"></i>Export Pdf</button>
                                                </div>
                                            </div>
                                            <?= form_close() ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0"><a href="#!" class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Collapsible Group Item #2</a></h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse card-body" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                        eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                                        sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore
                                        sustainable VHS.
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h5 class="mb-0"><a href="#!" class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Collapsible Group Item #3</a></h5>
                                    </div>
                                    <div id="collapseThree" class="card-body collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                        eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                                        sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore
                                        sustainable VHS.
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- [ Main Content ] end -->
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- [ Main Content ] end -->