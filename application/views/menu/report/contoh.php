<?php
$rowsdata = $data->row();
$nmsales = (!empty($rowsdata->name_sales) == '') ? '' : ' - ' . $rowsdata->name_sales;
$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle('SALES REPORT ' . strtoupper($nmsales));
$pdf->SetHeaderMargin(30);
$pdf->SetTopMargin(10);
$pdf->setFooterMargin(20);
$pdf->SetAutoPageBreak(true);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->AddPage();
if ($data->num_rows() < 1) {
    $name_sales = '';
} else {
    $name_sales = $data->row()->name_sales;
}
$html = '<table border="0" style="font-size: 8px;"> 
            <tr>
                <th rowspan="4" width="50%"><img src="' . base_url('assets/images/setting/' . $sett->logo_login) . '" width="80px"></th>
                <th align="right"></th>
            </tr>
            <tr>
                <th width="50%" align="right">' . $sett->alamat . '</th>
            </tr>
            <tr>
                <th align="right">Telp : ' . $sett->telephone . '</th>
            </tr>
            <tr>
                <th align="right">Faks : ' . $sett->faks . '</th>
            </tr>
        </table>
        <hr>
        <br>
        <br>SALES REPORT
        <br>Sales Name : ' . strtoupper($name_sales) . '
        <br>
        <br><table cellspacing="1" bgcolor="#666666" cellpadding="2" style="font-size: 11px;">
                <tr bgcolor="#ffffff">
                    <th width="5%" align="center">No.</th>
                    <th width="65%" align="left">Name of Applicant</th>
                    <th width="15%" align="left">Monthly</th>
                    <th width="15%" align="left">Price</th>
                </tr>';

if ($data->num_rows() < 1) {
    $html .= '<tr bgcolor="#ffffff">
                <td colspan="4" align="center">No Data Match !!</td>
            </tr>
            </table>';
} else {
    $i = 1;
    $tot = 0;
    foreach ($data->result() as $row) {
        $html .= '<tr bgcolor="#ffffff">
    <td align="center">' . $i . '</td>
    <td align="left">' . $row->name_customer . '</td>
    <td align="right">' . date_format(date_create($row->date), 'd-M') . '</td>
    <td align="right">' . number_format($row->price, 2) . '</td>
    </tr>';
        $tot += $row->price;
        $i++;
    }

    $html .= '<tr bgcolor="#ffffff">
            <td colspan="3" align="right">TOTAL (RM)</td>
            <td align="right">' . number_format($tot, 2) . '</td>
        </tr>
        </table>';
}

$pdf->SetFont('helvetica', '', 10);
$html .= '<br>
        <br>
        <br>Checked By : 
        <br>Issued By : 
        <br>
        <br>Approve By :
        <br>Date :
        <br><table cellspacing="1" style="border: 1px solid #000;" cellpadding="2">
            <tr>
                <td colspan="2" width="50%">Received By</td>
            </tr>
            <tr>
                <td width="20%">Name</td>
                <td width="30%">' . strtoupper($name_sales) . '</td>
            </tr>
            <tr>
                 <td width="20%">Date</td>
                <td width="30%">' . date('d/m/Y') . '</td>
            </tr>
        </table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Sales Report' . strtoupper($nmsales) . '.pdf', 'I');
exit();
