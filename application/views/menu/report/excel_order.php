<style>
    .dataTables_filter {
        display: none;
    }

    .as-row {
        white-space: nowrap;
    }

    .btn-excel,
    .btn-excel:hover {
        color: #fff;
        background-color: #0f8e08;
        border-color: #0f8e08;
    }
</style>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Export Complete Order</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Complete Order</h5>
                                        <!-- <a href="<?= base_url('add_zone') ?>" class="btn btn-primary"><i class="feather icon-plus"></i>Add New</a> -->
                                    </div>
                                    <div class="card-body">
                                        <?= form_open('excel-order', 'method="post" class="mb-3"') ?>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Month</label>
                                                    <select class="form-control" name="month">
                                                        <?php
                                                        $month = (isset($_POST['month'])) ? $_POST['month'] : '';
                                                        ?>
                                                        <option value="">Select Month</option>
                                                        <option value="all" <?php if ($month == 'all') echo 'selected'; ?>>All Data</option>
                                                        <option value="1" <?php if ($month == '1') {
                                                                                echo 'selected';
                                                                            } ?>>January</option>
                                                        <option value="2" <?php if ($month == '2') {
                                                                                echo 'selected';
                                                                            } ?>>February</option>
                                                        <option value="3" <?php if ($month == '3') {
                                                                                echo 'selected';
                                                                            } ?>>March</option>
                                                        <option value="4" <?php if ($month == '4') {
                                                                                echo 'selected';
                                                                            } ?>>April</option>
                                                        <option value="5" <?php if ($month == '5') {
                                                                                echo 'selected';
                                                                            } ?>>May</option>
                                                        <option value="6" <?php if ($month == '6') {
                                                                                echo 'selected';
                                                                            } ?>>June</option>
                                                        <option value="7" <?php if ($month == '7') {
                                                                                echo 'selected';
                                                                            } ?>>July</option>
                                                        <option value="8" <?php if ($month == '8') {
                                                                                echo 'selected';
                                                                            } ?>>August</option>
                                                        <option value="9" <?php if ($month == '9') {
                                                                                echo 'selected';
                                                                            } ?>>September</option>
                                                        <option value="10" <?php if ($month == '10') {
                                                                                echo 'selected';
                                                                            } ?>>October</option>
                                                        <option value="11" <?php if ($month == '11') {
                                                                                echo 'selected';
                                                                            } ?>>November</option>
                                                        <option value="12" <?php if ($month == '12') {
                                                                                echo 'selected';
                                                                            } ?>>December</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Year</label>
                                                    <select class="form-control" name="year">
                                                        <option value="">Select Year</option>
                                                        <option value="all" <?php if ($month == 'all') echo 'selected'; ?>>All Data</option>
                                                        <?php
                                                        $year = date('Y');
                                                        for ($i = $year; $i >= 2021; $i--) {
                                                            $slc = $i == $_POST['year'] ? 'selected' : null;
                                                        ?>
                                                            <option value="<?= $i ?>" <?= $slc ?>><?= $i ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <button class="btn btn-info" type="submit" name="cari" style="margin-top: 1.9rem;"><i class="feather icon-search"></i>Search</button>
                                                <?php
                                                if (isset($_POST['cari']) && $_POST['month'] !== '' || isset($_POST['cari']) && $_POST['year'] !== '') {
                                                    $m = $_POST['month'] ? $_POST['month'] : 0;
                                                    $y = $_POST['year'] ? $_POST['year'] : 0;
                                                    echo '<a href="' . base_url('ex-order/' . $m . '/' . $y) . '" class="btn btn-excel" style="margin-top: 1.9rem;"><i class="feather icon-file-text"></i>Export to Excel</a>';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <?= form_close() ?>
                                        <?php
                                        if (isset($_POST['cari'])) {
                                            if ($_POST['month'] == '' && $_POST['year'] == '') {
                                                echo '<div class="alert alert-danger"><i class="feather icon-alert-circle"></i> Please, <b>select filter first</b> to show data !!</div>';
                                            } else {
                                        ?>
                                                <div class="table-responsive">
                                                    <table class="table table-hover" id="tbExcelOrder">
                                                        <thead>
                                                            <tr>
                                                                <th>NO.</th>
                                                                <th>CUSTOMERS NAME</th>
                                                                <th>IC NO</th>
                                                                <th>CONTACT</th>
                                                                <th>EMAIL</th>
                                                                <th>ADDRESS</th>
                                                                <th>PACKAGE</th>
                                                                <th>COMPLETE DATE</th>
                                                                <!-- <th><i class="feather icon-settings"></i></th> -->
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $mth = '';
                                                            $yr = '';
                                                            if ($_POST['month']) {
                                                                if ($_POST['month'] !== '' && $_POST['month'] !== 'all') {
                                                                    $mth = " AND MONTH(a.approve_date) = '$_POST[month]' ";
                                                                }
                                                            }
                                                            if ($_POST['year']) {
                                                                if ($_POST['year'] !== '' && $_POST['year'] !== 'all') {
                                                                    $yr = " AND YEAR(a.approve_date) = '$_POST[year]' ";
                                                                }
                                                            }
                                                            $sql = "SELECT
                                                                        a.id,
                                                                        a.name_customer,
                                                                        a.number_ic,
                                                                        a.mobile_phone,
                                                                        a.email,
                                                                        a.address,
                                                                        a.category_product,
                                                                        b.`name` AS nm_product,
                                                                        a.approve,
                                                                        a.approve_by,
                                                                        c.`name`,
                                                                        DATE_FORMAT( a.approve_date, '%Y-%m-%d' ) AS acc_date 
                                                                    FROM
                                                                        `tb_order` a
                                                                        LEFT JOIN tb_category_product b ON a.category_product = b.id
                                                                        LEFT JOIN tb_user c ON a.approve_by = c.`hash` 
                                                                    WHERE
                                                                        a.approve = 'complete' 
                                                                        $mth
                                                                        $yr
                                                                    ORDER BY
                                                                        a.id DESC";

                                                            $no = 1;
                                                            $mydata = $this->db->query("SELECT
                                                                                    a.id,
                                                                                    a.name_customer,
                                                                                    a.number_ic,
                                                                                    a.mobile_phone,
                                                                                    a.email,
                                                                                    a.address,
                                                                                    a.category_product,
                                                                                    b.`name` AS nm_product,
                                                                                    a.approve,
                                                                                    a.approve_by,
                                                                                    c.`name`,
                                                                                    DATE_FORMAT( a.approve_date, '%Y-%m-%d' ) AS acc_date 
                                                                                FROM
                                                                                    `tb_order` a
                                                                                    LEFT JOIN tb_category_product b ON a.category_product = b.id
                                                                                    LEFT JOIN tb_user c ON a.approve_by = c.`hash` 
                                                                                WHERE
                                                                                    a.approve = 'complete' 
                                                                                    $mth
                                                                                    $yr
                                                                                ORDER BY
                                                                                    a.id DESC")->result();
                                                            foreach ($mydata as $dt) {
                                                            ?>
                                                                <tr>
                                                                    <td><?= $no ?></td>
                                                                    <td><?= $dt->name_customer ?></td>
                                                                    <td class="as-row"><?= $dt->number_ic ?></td>
                                                                    <td><?= $dt->mobile_phone ?></td>
                                                                    <td><?= $dt->email ?></td>
                                                                    <td><?= $dt->address ?></td>
                                                                    <td><?= $dt->nm_product ?></td>
                                                                    <td><?= $dt->acc_date ?></td>
                                                                </tr>
                                                            <?php
                                                                $no++;
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                        <?php
                                            }
                                        } else {
                                            echo '<div class="alert alert-danger"><i class="feather icon-alert-circle"></i> Please, <b>select filter first</b> to show data !!</div>';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Hapus -->
<div class="modal fade" id="modalDeleteZone" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
            </div>
            <div class="modal-body text-center">
                <?= form_open('delete_zone', 'method="post"') ?>
                <input type="hidden" name="id" id="idZone">
                Are you sure delete this data ?
                <br>
                <!-- </div>
          <div class="modal-footer"> -->
                <button type="button" class="btn btn-danger my-2" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary">Yes</button>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>