<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Form Order</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Order</h5>
                                    </div>
                                    <div class="card-body">
                                        <?= form_open_multipart('save_order', 'method="post"') ?>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Order Number</label>
                                                    <input type="text" class="form-control" name="order_num" placeholder="Order Number">
                                                    <!-- <small class="form-text text-muted">Input order number.</small> -->
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Customer Name</label>
                                                    <input type="text" class="form-control" name="name_cust" placeholder="Customer Name" required oninvalid="this.setCustomValidity('Input Customer Name')" oninput="setCustomValidity('')">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>IC Number</label>
                                                    <input type="text" class="form-control" name="ic_num" placeholder="IC Number" maxlength="12" required oninvalid="this.setCustomValidity('Input IC Number')" oninput="setCustomValidity('')">
                                                    <small class="form-text text-c-red">Input without strip (-)</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Passport</label>
                                                    <input type="text" class="form-control" name="passport_num" placeholder="Passport Number" maxlength="12" required oninvalid="this.setCustomValidity('Input Passport Number')" oninput="setCustomValidity('')">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Installation Date</label>
                                                    <div class="input-group mb-3">
                                                        <input type="text" class="form-control" id="date" name="date" placeholder="dd/mm/yyyy">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon2"><i class="feather icon-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Zone</label>
                                                    <select class="form-control" name="zone" required oninvalid="this.setCustomValidity('Select Zone')" oninput="setCustomValidity('')">
                                                        <option value="">Nothing Selected</option>
                                                        <?php
                                                        foreach ($zone as $zone_name) {
                                                        ?>
                                                            <option value="<?= $zone_name->id ?>"><?= $zone_name->name ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Sales</label>
                                                    <?php
                                                    if ($level == 'user') {
                                                    ?>
                                                        <input type="hidden" class="form-control" name="sales" value="<?= $this->session->userdata('id') ?>">
                                                        <input type="text" class="form-control" value="<?= $this->session->userdata('name') ?>" disabled>
                                                    <?php
                                                    } elseif ($level == 'leader') {
                                                    ?>
                                                        <select class="form-control" name="sales" required oninvalid="this.setCustomValidity('Select Sales')" oninput="setCustomValidity('')">
                                                            <option value="">Nothing Selected</option>
                                                            <?php
                                                            foreach ($sales_ldr as $sales_name) {
                                                            ?>
                                                                <option value="<?= $sales_name->id ?>"><?= $sales_name->level . ' &rarr; ' . ucwords(strtolower($sales_name->name)) ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    <?php
                                                    } elseif ($level == 'admin') {
                                                    ?>
                                                        <select class="form-control" name="sales" required oninvalid="this.setCustomValidity('Select Sales')" oninput="setCustomValidity('')">
                                                            <option value="">Nothing Selected</option>
                                                            <?php
                                                            foreach ($sales as $sales_name) {
                                                            ?>
                                                                <option value="<?= $sales_name->id ?>"><?= $sales_name->level . ' &rarr; ' . ucwords(strtolower($sales_name->name)) ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Category Product</label>
                                                    <select class="form-control" name="cat_product" required oninvalid="this.setCustomValidity('Select Category Product)" oninput="setCustomValidity('')">
                                                        <option value="">Nothing Selected</option>
                                                        <?php
                                                        foreach ($cat_product as $cat) {
                                                        ?>
                                                            <option value="<?= $cat->id ?>"><?= $cat->name ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Segment</label>
                                                    <!-- <input type="text" class="form-control" name="segment" placeholder="Segment" required oninvalid="this.setCustomValidity('Input Segment')" oninput="setCustomValidity('')"> -->
                                                    <select class="form-control" name="segment" required oninvalid="this.setCustomValidity('Select Segment)" oninput="setCustomValidity('')">
                                                        <option value="">Nothing Selected</option>
                                                        <?php
                                                        foreach ($segment as $sgt) {
                                                        ?>
                                                            <option value="<?= $sgt->id ?>"><?= $sgt->name ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Mobile Phone</label>
                                                    <div class="input-group mb-3">
                                                        <input type="text" class="form-control" name="mobile_phone" placeholder="Mobile Phone Number" required oninvalid="this.setCustomValidity('Input Mobile Phone Number')" oninput="setCustomValidity('')">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="feather icon-smartphone"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Del Number</label>
                                                    <input type="text" class="form-control" name="del_number" placeholder="Del Number">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <div class="input-group mb-3">
                                                        <input type="email" class="form-control" name="email" placeholder="Email Address" required oninvalid="this.setCustomValidity('Input Email Address')" oninput="setCustomValidity('')">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="feather icon-at-sign"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <!-- <input type="text" class="form-control" name="address" placeholder="Address"> -->
                                                    <textarea class="form-control" name="address" rows="3" required oninvalid="this.setCustomValidity('Input Address')" oninput="setCustomValidity('')"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Spora</label>
                                                    <input type="text" class="form-control" name="press" placeholder="Spora">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Price</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">RM</span>
                                                        </div>
                                                        <input type="text" name="harga_paket" class="form-control" placeholder="1" required oninvalid="this.setCustomValidity('Input Harga Paket')" oninput="setCustomValidity('')">
                                                    </div>
                                                    <small class="form-text text-c-red">Use point (.)</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-2">
                                            <div class="col-md-4">
                                                <label>Type</label>
                                                <div class="form-group">
                                                    <?php
                                                    $no = 1;
                                                    foreach ($type as $typ) {
                                                    ?>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="customRadioInline<?= $no ?>" name="type_order" class="custom-control-input" value="<?= $typ->id ?>">
                                                            <label class="custom-control-label" for="customRadioInline<?= $no ?>"><?= $typ->value ?></label>
                                                        </div>
                                                    <?php
                                                        $no++;
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Google Form</label>
                                                <div class="form-group">
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="radioGF1" name="google_form" class="custom-control-input" value="Y">
                                                        <label class="custom-control-label" for="radioGF1">Yes</label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="radioGF2" name="google_form" class="custom-control-input" value="N">
                                                        <label class="custom-control-label" for="radioGF2">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>E-Form ID</label>
                                                    <input type="text" class="form-control" name="eform_id" placeholder="E-Form ID">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group card-upload">
                                                    <label>Attachment File 1</label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input upload1" name="uploadImg[]" multiple="multiple">
                                                        <label class="custom-file-label">Choose file..</label>
                                                    </div>
                                                    <small style="color: red; font-size: 11px;">Max size file 5 megabyte. File <strong>jpeg, jpg, png, pdf</strong></small>
                                                    <div class="text-center">
                                                        <img src="#" class="img-responsive" id="upload1" alt="image1" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group card-upload">
                                                    <label>Attachment File 2</label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input upload2" name="uploadImg[]" multiple="multiple">
                                                        <label class="custom-file-label">Choose file..</label>
                                                    </div>
                                                    <small style="color: red; font-size: 11px;">Max size file 5 megabyte. File <strong>jpeg, jpg, png, pdf</strong></small>
                                                    <div class="text-center">
                                                        <img src="#" class="img-responsive" id="upload2" alt="image2" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group card-upload">
                                                    <label>Attachment File 3</label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input upload3" name="uploadImg[]" multiple="multiple">
                                                        <label class="custom-file-label">Choose file..</label>
                                                    </div>
                                                    <small style="color: red; font-size: 11px;">Max size file 5 megabyte. File <strong>jpeg, jpg, png, pdf</strong></small>
                                                    <div class="text-center">
                                                        <img src="#" class="img-responsive" id="upload3" alt="image3" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group card-upload">
                                                    <label>Attachment File 4</label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input upload4" name="uploadImg[]" multiple="multiple">
                                                        <label class="custom-file-label">Choose file..</label>
                                                    </div>
                                                    <small style="color: red; font-size: 11px;">Max size file 5 megabyte. File <strong>jpeg, jpg, png, pdf</strong></small>
                                                    <div class="text-center">
                                                        <img src="#" class="img-responsive" id="upload4" alt="image4" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group card-upload">
                                                    <label>Attachment File 5</label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input upload5" name="uploadImg[]" multiple="multiple">
                                                        <label class="custom-file-label">Choose file..</label>
                                                    </div>
                                                    <small style="color: red; font-size: 11px;">Max size file 5 megabyte. File <strong>jpeg, jpg, png, pdf</strong></small>
                                                    <div class="text-center">
                                                        <img src="#" class="img-responsive" id="upload5" alt="image5" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <?php
                                            if ($level == 'admin') {
                                                echo '<a href="' . base_url('view_order') . '" class="btn btn-secondary"><i class="feather icon-arrow-left"></i>Back</a>';
                                            } elseif ($level == 'leader') {
                                                echo '<a href="' . base_url('view_order_leader') . '" class="btn btn-secondary"><i class="feather icon-arrow-left"></i>Back</a>';
                                            }
                                            ?>
                                            <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                        <?= form_close() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- [ Main Content ] end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- [ Main Content ] end -->