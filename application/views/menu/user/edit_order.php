<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Form Edit Order</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Edit Order</h5>
                                    </div>
                                    <div class="card-body">
                                        <?php
                                        echo form_open_multipart('order/save_edit_order', 'method="post"');
                                        foreach ($order as $odr) { ?>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Order Number</label>
                                                        <input type="hidden" class="form-control" name="id" value="<?= $odr->id ?>">
                                                        <input type="text" class="form-control" name="order_num" placeholder="Order Number" value="<?= $odr->number_order ?>">
                                                        <!-- <small class="form-text text-muted">Input order number.</small> -->
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Customer Name</label>
                                                        <input type="text" class="form-control" name="name_cust" placeholder="Customer Name" value="<?= $odr->name_customer ?>" required oninvalid="this.setCustomValidity('Input Customer Name')" oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>IC Number</label>
                                                        <input type="text" class="form-control" name="ic_num" placeholder="IC Number" maxlength="12" value="<?= $odr->new_numberic ?>" required oninvalid="this.setCustomValidity('Input IC Number')" oninput="setCustomValidity('')">
                                                        <small class="form-text text-c-red">Input without strip (-)</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Passport</label>
                                                        <input type="text" class="form-control" name="passport_num" placeholder="Passport Number" maxlength="12" value="<?= $odr->number_passport ?>" required oninvalid="this.setCustomValidity('Input Passport Number')" oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Installation Date</label>
                                                        <div class="input-group mb-3">
                                                            <input type="text" class="form-control" id="date" name="date" placeholder="dd/mm/yyyy" value="<?= date_format(date_create($odr->date), "d/m/Y") ?>">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text" id="basic-addon2"><i class="feather icon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Zone</label>
                                                        <select class="form-control" name="zone" required oninvalid="this.setCustomValidity('Select Zone')" oninput="setCustomValidity('')">
                                                            <option value="">Nothing Selected</option>
                                                            <?php
                                                            foreach ($zone as $zone_name) {
                                                            ?>
                                                                <option value="<?= $zone_name->id ?>" <?php if ($zone_name->id == $odr->zone) {
                                                                                                            echo 'selected';
                                                                                                        } ?>><?= $zone_name->name ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Sales</label>
                                                        <?php
                                                        if ($level == 'user') {
                                                        ?>
                                                            <input type="hidden" class="form-control" name="sales" value="<?= $this->session->userdata('id') ?>">
                                                            <input type="text" class="form-control" value="<?= $this->session->userdata('name') ?>" disabled>
                                                        <?php
                                                        } elseif ($level == 'leader') {
                                                        ?>
                                                            <select class="form-control" name="sales" required oninvalid="this.setCustomValidity('Select Sales')" oninput="setCustomValidity('')">
                                                                <option value="">Nothing Selected</option>
                                                                <?php
                                                                foreach ($sales_ldr as $sales_name) {
                                                                ?>
                                                                    <option value="<?= $sales_name->id ?>" <?php if ($sales_name->id == $odr->sales) {
                                                                                                                echo 'selected';
                                                                                                            } ?>><?= strtoupper($sales_name->level) . ' &rarr; ' . ucwords(strtolower($sales_name->name)) ?></option>
                                                                <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        <?php
                                                        } elseif ($level == 'admin') {
                                                        ?>
                                                            <select class="form-control" name="sales" required oninvalid="this.setCustomValidity('Select Sales')" oninput="setCustomValidity('')">
                                                                <option value="">Nothing Selected</option>
                                                                <?php
                                                                foreach ($sales as $sales_name) {
                                                                ?>
                                                                    <option value="<?= $sales_name->id ?>" <?php if ($sales_name->id == $odr->sales) {
                                                                                                                echo 'selected';
                                                                                                            } ?>><?= strtoupper($sales_name->level) . ' &rarr; ' . ucwords(strtolower($sales_name->name)) ?></option>
                                                                <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Category Product</label>
                                                        <select class="form-control" name="cat_product" required oninvalid="this.setCustomValidity('Select Category Product)" oninput="setCustomValidity('')">
                                                            <option value="">Nothing Selected</option>
                                                            <?php
                                                            foreach ($cat_product as $cat) {
                                                            ?>
                                                                <option value="<?= $cat->id ?>" <?php if ($cat->id == $odr->category_product) {
                                                                                                    echo 'selected';
                                                                                                } ?>><?= $cat->name ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Segment</label>
                                                        <!-- <input type="text" class="form-control" name="segment" placeholder="Segment" value="<?= $odr->segment ?>" required oninvalid="this.setCustomValidity('Input Segment')" oninput="setCustomValidity('')"> -->
                                                        <select class="form-control" name="segment" required oninvalid="this.setCustomValidity('Select Segment)" oninput="setCustomValidity('')">
                                                            <option value="">Nothing Selected</option>
                                                            <?php
                                                            foreach ($segment as $sgt) {
                                                            ?>
                                                                <option value="<?= $sgt->id ?>" <?php if ($sgt->id == $odr->segment) {
                                                                                                    echo 'selected';
                                                                                                } ?>><?= $sgt->name ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input type="email" class="form-control" name="email" placeholder="Email Address" value="<?= $odr->email ?>" required oninvalid="this.setCustomValidity('Input Email Address')" oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Address</label>
                                                        <!-- <input type="text" class="form-control" name="address" placeholder="Address" value="" required oninvalid="this.setCustomValidity('Input Address')" oninput="setCustomValidity('')"> -->
                                                        <textarea class="form-control" name="address" rows="3" required oninvalid="this.setCustomValidity('Input Address')" oninput="setCustomValidity('')"><?= $odr->address ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Spora</label>
                                                        <input type="text" class="form-control" name="press" placeholder="Spora" value="<?= $odr->press4u ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Price</label>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">RM</span>
                                                            </div>
                                                            <input type="text" name="harga_paket" class="form-control" placeholder="1" value="<?= number_format($odr->price, 2) ?>" required oninvalid="this.setCustomValidity('Input Harga Paket')" oninput="setCustomValidity('')">
                                                        </div>
                                                        <small class="form-text text-c-red">Use point (.)</small>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Type</label>
                                                    <div class="form-group">
                                                        <?php
                                                        $no = 1;
                                                        foreach ($type as $typ) {
                                                        ?>
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="customRadioInline<?= $no ?>" name="type_order" class="custom-control-input" value="<?= $typ->id ?>" <?php if ($typ->id == $odr->type) {
                                                                                                                                                                                                echo 'checked';
                                                                                                                                                                                            } ?>>
                                                                <label class="custom-control-label" for="customRadioInline<?= $no ?>"><?= $typ->value ?></label>
                                                            </div>
                                                        <?php
                                                            $no++;
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Google Form</label>
                                                    <div class="form-group">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="radioGF1" name="google_form" class="custom-control-input" value="Y" <?php if ($odr->google_form == 'Y') {
                                                                                                                                                            echo 'checked';
                                                                                                                                                        } ?>>
                                                            <label class="custom-control-label" for="radioGF1">Yes</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="radioGF2" name="google_form" class="custom-control-input" value="N" <?php if ($odr->google_form == 'N') {
                                                                                                                                                            echo 'checked';
                                                                                                                                                        } ?>>
                                                            <label class="custom-control-label" for="radioGF2">No</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>E-Form ID</label>
                                                        <input type="text" class="form-control" name="eform_id" value="<?= $odr->eform_id ?>" placeholder="E-Form ID">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Remarks</label>
                                                        <textarea cols="10" rows="3" id="remarks" name="remakrs" placeholder="Optional Remarks" class="form-control"><?= $odr->remarks ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="alert alert-info">This status order is <b><?= strtoupper($odr->approve) ?></b>.</div>
                                            <div class="row">
                                                <?php
                                                $chk = $this->mOrder->cek_edit_leader('sha', $uri2);
                                                if ($level == 'admin') {
                                                    $disp = 'style="display: block;"';
                                                    $back = 'view_order';
                                                } elseif ($level == 'leader') {
                                                    // $disp = $chk == '' ? 'style="display: block;"' : 'style="display: none;"';
                                                    $disp = 'style="display: block;"';
                                                    $back = 'view_order_leader';
                                                } else {
                                                    $disp = 'style="display: none;"';
                                                    $back = 'view_order_user';
                                                }
                                                ?>
                                                <style>
                                                    .me-btn {
                                                        padding: 7px 7px;
                                                        margin-right: 2px;
                                                    }
                                                </style>
                                                <div class="col-sm-12" <?= $disp ?>>
                                                    <div class="form-group">
                                                        <label>Status Button</label>
                                                        <div class="col-xs-12">
                                                            <input type="hidden" name="status" id="jenisStatus">
                                                            <!-- <a href="<?= base_url('pending/' . $back . '/' . $odr->id) ?>" class="btn btn-warning mb-2" title="Pending"><i class="feather icon-clock"></i>Pending</a> -->
                                                            <button type="button" class="btn me-btn btn-warning mb-2 btnPending" data-toggle="modal" data-target="#modalPending" data-order="<?= $odr->number_order ?>" title="Proses"><i class="feather icon-rotate-cw"></i>Pending</button>
                                                            <button type="button" class="btn me-btn btn-secondary mb-2 btnProses" data-toggle="modal" data-target="#modalProses" data-order="<?= $odr->number_order ?>" title="Proses"><i class="feather icon-rotate-cw"></i>Proses</button>
                                                            <button type="button" class="btn me-btn btn-light mb-2 btnPress4U" data-toggle="modal" data-target="#modalPress" data-order="<?= $odr->number_order ?>" title="Spora"><i class="feather icon-disc"></i>Spora</button>
                                                            <button type="button" class="btn me-btn btn-success mb-2 btnPaidUploadReceipt" data-toggle="modal" data-target="#modalPaidUploadReceipt" data-order="<?= $odr->number_order ?>" title="Paid"><i class="feather icon-check-circle"></i>Paid</button>
                                                            <button type="button" class="btn me-btn btn-danger mb-2 btnUnpaidUploadReceipt" data-toggle="modal" data-target="#modalUnpaidUploadReceipt" data-order="<?= $odr->number_order ?>" title="Unpaid"><i class="feather icon-x-circle"></i>Unpaid</button>
                                                            <button type="button" class="btn me-btn btn-danger mb-2 modalCancel" data-toggle="modal" data-target="#modalCancel" data-order="<?= $odr->number_order ?>" title="Cancel"><i class="feather icon-x"></i>Cancel</button>
                                                            <button type="button" class="btn me-btn btn-warning mb-2 modalPresales" data-toggle="modal" data-target="#modalPresales" data-order="<?= $odr->number_order ?>" title="Presales"><i class="feather icon-corner-down-left"></i>Presales</button>
                                                            <button type="button" class="btn me-btn btn-dark mb-2 modalOSSGFORM" data-toggle="modal" data-target="#modalOSSGFORM" data-order="<?= $odr->number_order ?>" title="OSS/GFORM"><i class="feather icon-trending-up"></i>OSS/GFORM</button>
                                                            <button type="button" class="btn me-btn btn-secondary mb-2 modalPortFull" data-toggle="modal" data-target="#modalPortFull" data-order="<?= $odr->number_order ?>" title="Port Full"><i class="feather icon-alert-triangle"></i>Port Full</button>
                                                            <button type="button" class="btn me-btn btn-dark mb-2 btnRejectUploadReceipt" data-toggle="modal" data-target="#modalRejectUploadReceipt" data-order="<?= $odr->number_order ?>" title="Reject"><i class="feather icon-corner-up-left"></i>Reject</button>
                                                            <a href="<?= base_url('complete/' . $back . '/' . $odr->id) ?>" class="btn me-btn btn-success mb-2" title="Complete" onclick="return msgComplete()"><i class="feather icon-file-text"></i>Complete</a>
                                                            <!-- <button type="button" class="btn btn-info mb-2" data-order="<?= $odr->number_order ?>" title="Complete"><i class="feather icon-file-text"></i>Complete</button> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="text-left">
                                                <label>Update Data</label>
                                                <br />
                                                <?php
                                                // switch ($level) {
                                                //     case 'admin':
                                                //         $url = 'view_order';
                                                //         break;

                                                //     case 'leader':
                                                //         $url = 'view_order_leader';
                                                //         break;

                                                //     case 'user':
                                                //         $url = 'view_order_user';
                                                //         break;
                                                // }
                                                $url = $_SERVER['HTTP_REFERER'];
                                                ?>
                                                <a href="<?= $url ?>" class="btn btn-secondary"><i class="feather icon-arrow-left"></i>Back</a>
                                                <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Update</button>
                                            </div>
                                            <hr>
                                        <?php
                                        }
                                        echo form_close();
                                        ?>

                                        <!-- File Upload -->
                                        <div class="row mb-3">
                                            <!-- [ tabs ] start -->
                                            <div class="col-sm-12">
                                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" id="file1-tab" data-toggle="tab" href="#file1" role="tab" aria-controls="home" aria-selected="true"><i class="feather icon-paperclip m-r-10"></i>File 1</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="file2-tab" data-toggle="tab" href="#file2" role="tab" aria-controls="profile" aria-selected="false"><i class="feather icon-paperclip m-r-10"></i>File 2</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="file3-tab" data-toggle="tab" href="#file3" role="tab" aria-controls="contact" aria-selected="false"><i class="feather icon-paperclip m-r-10"></i>File 3</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="file4-tab" data-toggle="tab" href="#file4" role="tab" aria-controls="contact" aria-selected="false"><i class="feather icon-paperclip m-r-10"></i>File 4</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="file5-tab" data-toggle="tab" href="#file5" role="tab" aria-controls="contact" aria-selected="false"><i class="feather icon-paperclip m-r-10"></i>File 5</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content" id="myTabContent">
                                                    <div class="tab-pane fade show active" id="file1" role="tabpanel" aria-labelledby="file1-tab">
                                                        <?php
                                                        if ($odr->image1 == '') {
                                                            echo '<div class="alert alert-danger"><i class="feather icon-alert-circle m-r-10"></i>No File Upload.</div>';
                                                        } else {
                                                            echo '<iframe src="' . base_url() . 'assets/images/order/' . $odr->image1 . '" class="img-responsive file-upload" alt="attachment-file-1" height="450px"></iframe>';
                                                        }
                                                        ?>
                                                        <div class="m-t-10">
                                                            <label>Reupload File 1</label>
                                                            <?= form_open_multipart('save_reupload/file1', 'method="post"') ?>
                                                            <div class="row">
                                                                <input type="hidden" class="form-control" name="id" value="<?= $odr->id ?>">
                                                                <div class="col-md-5">
                                                                    <div class="form-group mb-2">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input uploadEdit1" name="file1" required oninvalid="this.setCustomValidity('Select File')" oninput="setCustomValidity('')">
                                                                            <label class="custom-file-label">Choose file..</label>
                                                                        </div>
                                                                        <small style="color: red; font-size: 11px;">Max size file 5 megabyte. File <strong>jpeg, jpg, png, pdf</strong></small>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <button type="submit" class="btn btn-primary"><i class="feather icon-share"></i>Upload File</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="uploadEdit1"></div>
                                                            <?= form_close() ?>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="file2" role="tabpanel" aria-labelledby="file2-tab">
                                                        <?php
                                                        if ($odr->image2 == '') {
                                                            echo '<div class="alert alert-danger"><i class="feather icon-alert-circle m-r-10"></i>No File Upload.</div>';
                                                        } else {
                                                            echo '<iframe src="' . base_url() . 'assets/images/order/' . $odr->image2 . '" class="img-responsive file-upload" alt="attachment-file-2" height="450px"></iframe>';
                                                        }
                                                        ?>
                                                        <div class="m-t-10">
                                                            <label>Reupload File 2</label>
                                                            <?= form_open_multipart('save_reupload/file2', 'method="post"') ?>
                                                            <div class="row">
                                                                <input type="hidden" class="form-control" name="id" value="<?= $odr->id ?>">
                                                                <div class="col-md-5">
                                                                    <div class="form-group mb-2">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input uploadEdit2" name="file2" required oninvalid="this.setCustomValidity('Select File')" oninput="setCustomValidity('')">
                                                                            <label class="custom-file-label">Choose file..</label>
                                                                        </div>
                                                                        <small style="color: red; font-size: 11px;">Max size file 5 megabyte. File <strong>jpeg, jpg, png, pdf</strong></small>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <button type="submit" class="btn btn-primary"><i class="feather icon-share"></i>Upload File</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="uploadEdit2"></div>
                                                            <?= form_close() ?>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="file3" role="tabpanel" aria-labelledby="file3-tab">
                                                        <?php
                                                        if ($odr->image3 == '') {
                                                            echo '<div class="alert alert-danger"><i class="feather icon-alert-circle m-r-10"></i>No File Upload.</div>';
                                                        } else {
                                                            echo '<iframe src="' . base_url() . 'assets/images/order/' . $odr->image3 . '" class="img-responsive file-upload" alt="attachment-file-3" height="450px"></iframe>';
                                                        }
                                                        ?>
                                                        <div class="m-t-10">
                                                            <label>Reupload File 3</label>
                                                            <?= form_open_multipart('save_reupload/file3', 'method="post"') ?>
                                                            <div class="row">
                                                                <input type="hidden" class="form-control" name="id" value="<?= $odr->id ?>">
                                                                <div class="col-md-5">
                                                                    <div class="form-group mb-2">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input uploadEdit3" name="file3" required oninvalid="this.setCustomValidity('Select File')" oninput="setCustomValidity('')">
                                                                            <label class="custom-file-label">Choose file..</label>
                                                                        </div>
                                                                        <small style="color: red; font-size: 11px;">Max size file 5 megabyte. File <strong>jpeg, jpg, png, pdf</strong></small>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <button type="submit" class="btn btn-primary"><i class="feather icon-share"></i>Upload File</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="uploadEdit3"></div>
                                                            <?= form_close() ?>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="file4" role="tabpanel" aria-labelledby="file4-tab">
                                                        <?php
                                                        if ($odr->image4 == '') {
                                                            echo '<div class="alert alert-danger"><i class="feather icon-alert-circle m-r-10"></i>No File Upload.</div>';
                                                        } else {
                                                            echo '<iframe src="' . base_url() . 'assets/images/order/' . $odr->image4 . '" class="img-responsive file-upload" alt="attachment-file-4" height="450px"></iframe>';
                                                        }
                                                        ?>
                                                        <div class="m-t-10">
                                                            <label>Reupload File 4</label>
                                                            <?= form_open_multipart('save_reupload/file4', 'method="post"') ?>
                                                            <div class="row">
                                                                <input type="hidden" class="form-control" name="id" value="<?= $odr->id ?>">
                                                                <div class="col-md-5">
                                                                    <div class="form-group mb-2">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input uploadEdit4" name="file4" required oninvalid="this.setCustomValidity('Select File')" oninput="setCustomValidity('')">
                                                                            <label class="custom-file-label">Choose file..</label>
                                                                        </div>
                                                                        <small style="color: red; font-size: 11px;">Max size file 5 megabyte. File <strong>jpeg, jpg, png, pdf</strong></small>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <button type="submit" class="btn btn-primary"><i class="feather icon-share"></i>Upload File</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="uploadEdit4"></div>
                                                            <?= form_close() ?>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="file5" role="tabpanel" aria-labelledby="file5-tab">
                                                        <?php
                                                        if ($odr->image5 == '') {
                                                            echo '<div class="alert alert-danger"><i class="feather icon-alert-circle m-r-10"></i>No File Upload.</div>';
                                                        } else {
                                                            echo '<iframe src="' . base_url() . 'assets/images/order/' . $odr->image5 . '" class="img-responsive file-upload" alt="attachment-file-5" height="450px"></iframe>';
                                                        }
                                                        ?>
                                                        <div class="m-t-10">
                                                            <label>Reupload File 5</label>
                                                            <?= form_open_multipart('save_reupload/file5', 'method="post"') ?>
                                                            <div class="row">
                                                                <input type="hidden" class="form-control" name="id" value="<?= $odr->id ?>">
                                                                <div class="col-md-5">
                                                                    <div class="form-group mb-2">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input uploadEdit5" name="file5" required oninvalid="this.setCustomValidity('Select File')" oninput="setCustomValidity('')">
                                                                            <label class="custom-file-label">Choose file..</label>
                                                                        </div>
                                                                        <small style="color: red; font-size: 11px;">Max size file 5 megabyte. File <strong>jpeg, jpg, png, pdf</strong></small>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <button type="submit" class="btn btn-primary"><i class="feather icon-share"></i>Upload File</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="uploadEdit5"></div>
                                                            <?= form_close() ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <input type="hidden" name="reupload" id="reuploadText">
                                                    <div class="custom-control custom-checkbox mt-3 mb-1">
                                                        <input type="checkbox" class="custom-control-input" onclick="showAttachment()" id="reuploadQuest">
                                                        <label class="custom-control-label" for="reuploadQuest"><b>Checked to re-upload attachment file</b></label>
                                                    </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <button type="button" onclick="return msgComplete()">Click</button> -->
                        <!-- [ Main Content ] end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- [ Main Content ] end -->

<!-- Modal Complete -->
<div class="modal fade" id="modalComplete" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order Complete</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> -->
            </div>
            <div class="modal-body">
                <div id="msgcomplete"></div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Pending -->
<div class="modal fade" id="modalPending" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order Pending</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open_multipart('save_remarks/pending', 'method="post"') ?>
            <div class="modal-body">
                <div id="msgpending"></div>
                <div class="form-group">
                    <label>Order Number</label>
                    <br>
                    <input type="hidden" name="id" value="<?= $odr->id ?>">
                    <input type="text" class="form-control" name="order_num" value="<?= $odr->number_order ?>" placeholder="Input Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                    <!-- <span id="uploadRemarksNum" style="font-size: 18px;"></span> -->
                </div>
                <div class="form-group">
                    <label>Remarks</label>
                    <textarea class="form-control" rows="3" name="remarks" placeholder="Input Remarks" required oninvalid="this.setCustomValidity('Input Remarks')" oninput="setCustomValidity('')"><?= $odr->remarks ?></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="return msgSave('pending')"><i class="feather icon-check-circle"></i>Save</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- Modal modalPaidUploadReceipt -->
<div class="modal fade" id="modalPaidUploadReceipt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order Paid</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open_multipart('save_remarks/paid', 'method="post"') ?>
            <div class="modal-body">
                <div id="msgpaid"></div>
                <div class="form-group">
                    <label>Order Number</label>
                    <br>
                    <input type="hidden" name="id" value="<?= $odr->id ?>">
                    <input type="text" class="form-control" name="order_num" placeholder="Input Order Number" value="<?= $odr->number_order ?>" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                    <!-- <span id="uploadReceiptNum" style="font-size: 18px;"></span> -->
                </div>
                <div class="form-group">
                    <label>Receipt</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input uploadReceipt" name="receipt" required oninvalid="this.setCustomValidity('Select File')" oninput="setCustomValidity('')">
                        <label class="custom-file-label">Choose file..</label>
                    </div>
                    <small style="color: red; font-size: 11px;">Max size file 1 megabyte. File <strong>jpeg, jpg, png, pdf</strong></small>
                    <div class="text-center">
                        <img src="#" class="img-responsive mt-2" id="uploadReceipt" alt="image-receipt" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="return msgSave('paid')"><i class="feather icon-check-circle"></i>Save</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- Modal modalUnpaidUploadReceipt -->
<div class="modal fade" id="modalUnpaidUploadReceipt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order Unpaid</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open_multipart('save_remarks/unpaid', 'method="post"') ?>
            <div class="modal-body">
                <div id="msgunpaid"></div>
                <div class="form-group">
                    <label>Order Number</label>
                    <br>
                    <input type="hidden" name="id" value="<?= $odr->id ?>">
                    <input type="text" class="form-control" name="order_num" value="<?= $odr->number_order ?>" placeholder="Input Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                    <!-- <span id="uploadRemarksNum" style="font-size: 18px;"></span> -->
                </div>
                <div class="form-group">
                    <label>Remarks</label>
                    <textarea class="form-control" rows="3" name="remarks" placeholder="Input Remarks" required oninvalid="this.setCustomValidity('Input Remarks')" oninput="setCustomValidity('')"><?= $odr->remarks ?></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="return msgSave('unpaid')"><i class="feather icon-check-circle"></i>Save</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- Modal modalCancel -->
<div class="modal fade" id="modalCancel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order Cancel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open_multipart('save_remarks/cancel', 'method="post"') ?>
            <div class="modal-body">
                <div id="msgcancel"></div>
                <div class="form-group">
                    <label>Order Number</label>
                    <br>
                    <input type="hidden" name="id" value="<?= $odr->id ?>">
                    <input type="text" class="form-control" name="order_num" value="<?= $odr->number_order ?>" placeholder="Input Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                    <!-- <span id="uploadRemarksNum" style="font-size: 18px;"></span> -->
                </div>
                <div class="form-group">
                    <label>Remarks</label>
                    <textarea class="form-control" rows="3" name="remarks" placeholder="Input Remarks" required oninvalid="this.setCustomValidity('Input Remarks')" oninput="setCustomValidity('')"><?= $odr->remarks ?></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="return msgSave('cancel')"><i class="feather icon-check-circle"></i>Save</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- Modal modalPresales -->
<div class="modal fade" id="modalPresales" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order Presales</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open_multipart('save_remarks/presales', 'method="post"') ?>
            <div class="modal-body">
                <div id="msgpresales"></div>
                <div class="form-group">
                    <label>Order Number</label>
                    <br>
                    <input type="hidden" name="id" value="<?= $odr->id ?>">
                    <input type="text" class="form-control" name="order_num" value="<?= $odr->number_order ?>" placeholder="Input Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                    <!-- <span id="uploadRemarksNum" style="font-size: 18px;"></span> -->
                </div>
                <div class="form-group">
                    <label>Remarks</label>
                    <textarea class="form-control" rows="3" name="remarks" placeholder="Input Remarks" required oninvalid="this.setCustomValidity('Input Remarks')" oninput="setCustomValidity('')"><?= $odr->remarks ?></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="return msgSave('presales')"><i class="feather icon-check-circle"></i>Save</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- Modal modalOSSGFORM -->
<div class="modal fade" id="modalOSSGFORM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order OSS/GFORM</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open_multipart('save_remarks/ossgform', 'method="post"') ?>
            <div class="modal-body">
                <div id="msgossgform"></div>
                <div class="form-group">
                    <label>Order Number</label>
                    <br>
                    <input type="hidden" name="id" value="<?= $odr->id ?>">
                    <input type="text" class="form-control" name="order_num" value="<?= $odr->number_order ?>" placeholder="Input Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                    <!-- <span id="uploadRemarksNum" style="font-size: 18px;"></span> -->
                </div>
                <div class="form-group">
                    <label>Remarks</label>
                    <textarea class="form-control" rows="3" name="remarks" placeholder="Input Remarks" required oninvalid="this.setCustomValidity('Input Remarks')" oninput="setCustomValidity('')"><?= $odr->remarks ?></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="return msgSave('ossgform')"><i class="feather icon-check-circle"></i>Save</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- Modal modalPortFull -->
<div class="modal fade" id="modalPortFull" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order Port Full</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open_multipart('save_remarks/portfull', 'method="post"') ?>
            <div class="modal-body">
                <div id="msgportfull"></div>
                <div class="form-group">
                    <label>Order Number</label>
                    <br>
                    <input type="hidden" name="id" value="<?= $odr->id ?>">
                    <input type="text" class="form-control" name="order_num" value="<?= $odr->number_order ?>" placeholder="Input Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                    <!-- <span id="uploadRemarksNum" style="font-size: 18px;"></span> -->
                </div>
                <div class="form-group">
                    <label>Remarks</label>
                    <textarea class="form-control" rows="3" name="remarks" placeholder="Input Remarks" required oninvalid="this.setCustomValidity('Input Remarks')" oninput="setCustomValidity('')"><?= $odr->remarks ?></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="return msgSave('portfull')"><i class="feather icon-check-circle"></i>Save</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- Modal modalProses -->
<div class="modal fade" id="modalProses" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order Proses</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open_multipart('save_remarks/proses', 'method="post"') ?>
            <div class="modal-body">
                <div class="form-group">
                    <div id="msgproses"></div>
                    <label>Order Number</label>
                    <br>
                    <input type="hidden" name="id" value="<?= $odr->id ?>">
                    <input type="text" class="form-control" name="order_num" value="<?= $odr->number_order ?>" placeholder="Input Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                    <!-- <span id="uploadRemarksNum" style="font-size: 18px;"></span> -->
                </div>
                <div class="form-group">
                    <label>Remarks</label>
                    <textarea class="form-control" rows="3" name="remarks" placeholder="Input Remarks" required oninvalid="this.setCustomValidity('Input Remarks')" oninput="setCustomValidity('')"><?= $odr->remarks ?></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="return msgSave('proses')"><i class="feather icon-check-circle"></i>Save</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- Modal modalPress -->
<div class="modal fade" id="modalPress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order Spora</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open_multipart('save_remarks/spora', 'method="post"') ?>
            <div class="modal-body">
                <div id="msgspora"></div>
                <div class="form-group">
                    <label>Order Number</label>
                    <br>
                    <input type="hidden" name="id" value="<?= $odr->id ?>">
                    <input type="text" class="form-control" name="order_num" value="<?= $odr->number_order ?>" placeholder="Input Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                    <!-- <span id="uploadRemarksNum" style="font-size: 18px;"></span> -->
                </div>
                <div class="form-group">
                    <label>Remarks</label>
                    <textarea class="form-control" rows="3" name="remarks" id="remarksModal" placeholder="Input Remarks" required><?= $odr->remarks ?></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="return msgSave('spora')"><i class="feather icon-check-circle"></i>Save</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- Modal modalRejectUploadReceipt -->
<div class="modal fade" id="modalRejectUploadReceipt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order Reject</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open_multipart('save_remarks/reject', 'method="post"') ?>
            <div class="modal-body">
                <div id="msgreject"></div>
                <div class="form-group">
                    <label>Order Number</label>
                    <br>
                    <input type="hidden" name="id" value="<?= $odr->id ?>">
                    <input type="text" class="form-control" name="order_num" value="<?= $odr->number_order ?>" placeholder="Input Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                    <!-- <span id="uploadRemarksNum" style="font-size: 18px;"></span> -->
                </div>
                <div class="form-group">
                    <label>Remarks</label>
                    <textarea class="form-control" rows="3" name="remarks" placeholder="Input Remarks"><?= $odr->remarks ?></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="return msgSave('reject')"><i class="feather icon-check-circle"></i>Save</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<script>
    function msgSave(val) {
        document.querySelector('#msg' + val).innerHTML = '<div class="alert alert-info"><img src="<?= base_url() ?>assets/images/setting/loading.gif" class="mr-2" width="30">Please wait, process <b>save data</b> and <b>send status to email</b> !!</div>';
    }

    function msgComplete() {
        $('#modalComplete').modal('show');
        $('#msgcomplete').html('<div class="alert alert-info" style="margin-bottom: 0;"><img src="<?= base_url() ?>assets/images/setting/loading.gif" class="mr-2" width="30">Please wait, process <b>save data</b> and <b>send status to email</b> !!</div>');
    }
</script>