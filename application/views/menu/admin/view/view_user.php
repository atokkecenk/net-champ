<style>
    table>tbody>tr>td {
        white-space: nowrap !important;
    }

    .eye-btn {
        background-color: #1de9b6;
        color: #fff;
        padding: 2px 3px;
        border-radius: 3px;
        cursor: pointer;
    }
</style>

<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">View User</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Data User</h5>
                                        <!-- <a href="<?= base_url('add_user') ?>" class="btn btn-primary"><i class="feather icon-plus"></i>Add New</a> -->
                                        <a href="<?= base_url('set-credentials') ?>" class="btn btn-primary"><i class="feather icon-plus"></i>Add New</a>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover" id="tbl-view-user">
                                                <thead>
                                                    <tr>
                                                        <th>NO.</th>
                                                        <th>NAMA PENUH</th>
                                                        <th width="12%">CODE</th>
                                                        <th>USERNAME</th>
                                                        <th>CREDENTIAL</th>
                                                        <th>SUBMITTED</th>
                                                        <th>SUBMITTED DATE</th>
                                                        <th width="12%"><i class="feather icon-settings"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modalDetailUser" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail <span id="headerDetailUser"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="showDetailUser"></div>
                <?php
                // $id = base64_decode($_GET['h']);
                // $sql = $this->db->query("SELECT
                //                             * 
                //                         FROM
                //                             (
                //                             SELECT
                //                                 0 urut,
                //                                 a.`level`,
                //                                 '|--------- ' hirarki,
                //                                 a.`name`,
                //                                 a.email 
                //                             FROM
                //                                 tb_user a
                //                                 LEFT JOIN tb_leader b ON a.uid_leader = b.uid
                //                                 LEFT JOIN tb_user c ON b.uid = c.uid 
                //                             WHERE
                //                                 a.`hash` = '$id' 
                //                                 AND a.active = 'Y' UNION ALL
                //                             SELECT
                //                                 1 urut,
                //                                 c.`level`,
                //                                 '|--- ' hirarki,
                //                                 c.`name`,
                //                                 c.email 
                //                             FROM
                //                                 tb_user a
                //                                 LEFT JOIN tb_leader b ON a.uid_leader = b.uid
                //                                 LEFT JOIN tb_user c ON b.uid = c.uid 
                //                             WHERE
                //                                 a.`hash` = '$id' 
                //                                 AND a.active = 'Y' UNION ALL
                //                             SELECT
                //                                 2 urut,
                //                                 e.`level`,
                //                                 '' hirarki,
                //                                 e.`name`,
                //                                 e.email 
                //                             FROM
                //                                 tb_user a
                //                                 LEFT JOIN tb_leader b ON a.uid_leader = b.uid
                //                                 LEFT JOIN tb_user c ON b.uid = c.uid
                //                                 LEFT JOIN tb_main_leader d ON b.uid_main_leader = d.uid
                //                                 LEFT JOIN tb_user e ON d.uid = e.uid 
                //                             WHERE
                //                                 a.`hash` = '$id' 
                //                                 AND a.active = 'Y' 
                //                             ) us 
                //                         ORDER BY
                //                             us.urut DESC")->result();
                // foreach ($sql as $s) {
                //     switch ($s->level) {
                //         case 'mainleader':
                //             $lv = 'ML';
                //             break;
                //         case 'leader':
                //             $lv = 'LD';
                //             break;
                //         case 'user':
                //             $lv = 'US';
                //             break;
                //     }
                //     $lev = ($s->level == 'mainleader') ? 'MAIN LEADER' : strtoupper($s->level);
                //     echo '<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;">' . $lv . '</span> ' . $s->hirarki . ' <b>' . $s->name . '</b><br/>';
                // }
                ?>
            </div>
            <!-- <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div> -->
        </div>
    </div>
</div>

<!-- Modal Hapus -->
<div class="modal fade" id="modalDeleteUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
            </div>
            <div class="modal-body text-center">
                <?= form_open('delete_user', 'method="post"') ?>
                <input type="hidden" name="id" id="idUser">
                Are you sure delete this data ?
                <br>
                <!-- </div>
          <div class="modal-footer"> -->
                <button type="button" class="btn btn-danger my-2" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary">Yes</button>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Reset -->
<div class="modal fade" id="modalResetUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
            </div>
            <div class="modal-body text-center">
                <?= form_open('reset-password', 'method="post"') ?>
                <input type="hidden" name="id" id="idUserReset">
                <input type="hidden" name="idno_hash" id="idUserReset2">
                Are you sure Reset Password ?
                <br>
                <!-- </div>
          <div class="modal-footer"> -->
                <button type="button" class="btn btn-danger my-2" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary">Yes</button>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit Password -->
<div class="modal fade" id="modalEditPwd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open('change-password', 'method="post"') ?>
            <input type="hidden" name="id" id="idUserChg">
            <input type="hidden" name="hash" id="idHash">
            <div class="modal-body">
                <div class="form-group">
                    <label>New Password</label>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" name="pwd" placeholder="New password" id="input1" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" onclick="return show()" type="button"><span id="t_pwd">Show</span></button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Confirm New Password</label>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" name="cf_pwd" placeholder="Confirm new password" id="input2" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" onclick="return show2()" type="button"><span id="t_pwd2">Show</span></button>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger my-2" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>