<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">View Detail Order</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Data Detail Order</h5>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" colspan="6"><i class="feather icon-file-text m-r-5" style="color: #04a9f5;"></i>DATA DETAIL ORDER</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>ORDER NUMBER</td>
                                                        <td>:</td>
                                                        <td><?= $order->number_order ?></td>
                                                        <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>SALES NAME</td>
                                                        <td>:</td>
                                                        <td><?= strtoupper($order->name_sales) . '<span class="badge badge-pill m-l-5" style="color: #fff; background-color: #04a9f5;">' . $order->sales_id . '</span>' ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>IC NUMBER</td>
                                                        <td>:</td>
                                                        <td><?= $order->number_ic ?></td>
                                                        <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>LEADER NAME</td>
                                                        <td>:</td>
                                                        <td><?= strtoupper($order->name_leader) . '<span class="badge badge-pill m-l-5" style="color: #fff; background-color: #04a9f5;">' . $order->leader_id . '</span>' ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>PASSPORT</td>
                                                        <td>:</td>
                                                        <td><?= $order->number_passport ?></td>
                                                        <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>SUBMITTED</td>
                                                        <td>:</td>
                                                        <td><?= strtoupper($order->name_user_input) . '<span class="badge badge-pill m-l-5" style="color: #fff; background-color: #04a9f5;">' . strtoupper($order->level_user_input) . '</span>' ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>INSTALLATION DATE</td>
                                                        <td>:</td>
                                                        <td><?= strtoupper($order->new_date) ?></td>
                                                        <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>SUBMITTED DATE</td>
                                                        <td>:</td>
                                                        <td><?= strtoupper($order->new_date_input) . ', ' . $order->time ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>CUSTOMER NAME</td>
                                                        <td>:</td>
                                                        <td><?= strtoupper($order->name_customer) ?></td>
                                                        <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>STATUS</td>
                                                        <td>:</td>
                                                        <td><?php
                                                            switch ($order->approve) {

                                                                case 'pending':
                                                                    $badge = '<i class="fas fa-circle f-10 m-r-10" style="color: #ffeb00fa;"></i>';
                                                                    $txt = null;
                                                                    $txt2 = null;
                                                                    $rmks = null;
                                                                    break;

                                                                case 'spora':
                                                                    $badge = '<i class="fas fa-circle f-10 m-r-10" style="color: #ff98b4;"></i>';
                                                                    $txt = null;
                                                                    $txt2 = null;
                                                                    $rmks = null;
                                                                    break;

                                                                case 'proses':
                                                                    $badge = '<i class="fas fa-circle f-10 m-r-10" style="color: #748892;"></i>';
                                                                    $txt = 'PROSES BY';
                                                                    $txt2 = 'PROSES DATE';
                                                                    $rmks = $order->remarks;
                                                                    break;

                                                                case 'paid':
                                                                    $badge = '<i class="fas fa-circle text-c-green f-10 m-r-10"></i>';
                                                                    $txt = 'APPROVE BY';
                                                                    $txt2 = 'APPROVE DATE';
                                                                    $rmks = null;
                                                                    break;

                                                                case 'unpaid':
                                                                    $badge = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>';
                                                                    $txt = 'REJECT BY';
                                                                    $txt2 = 'REJECT DATE';
                                                                    $rmks = $order->remarks;
                                                                    break;

                                                                case 'presales':
                                                                    $badge = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>';
                                                                    $txt = null;
                                                                    $txt2 = null;
                                                                    $rmks = null;
                                                                    break;

                                                                case 'portfull':
                                                                    $badge = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>';
                                                                    $txt = null;
                                                                    $txt2 = null;
                                                                    $rmks = null;
                                                                    break;
                                                                
                                                                case 'ossgform':
                                                                    $badge = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>';
                                                                    $txt = null;
                                                                    $txt2 = null;
                                                                    $rmks = null;
                                                                    break;

                                                                case 'cancel':
                                                                    $badge = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>';
                                                                    $txt = 'CANCEL BY';
                                                                    $txt2 = 'CANCEL DATE';
                                                                    $rmks = $order->remarks;
                                                                    break;

                                                                case 'reject':
                                                                    $badge = '<i class="fas fa-circle f-10 m-r-10" style="color: #37474f;"></i>';
                                                                    $txt = 'REJECT BY';
                                                                    $txt2 = 'REJECT DATE';
                                                                    $rmks = $order->remarks;
                                                                    break;

                                                                case 'complete':
                                                                    $badge = '<i class="fas fa-circle f-10 m-r-10" style="color: #04a9f5;"></i>';
                                                                    $txt = 'APPROVE BY';
                                                                    $txt2 = 'APPROVE DATE';
                                                                    $rmks = null;
                                                                    break;
                                                            }
                                                            echo $badge . strtoupper($order->approve);
                                                            ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>ZONE</td>
                                                        <td>:</td>
                                                        <td><?= strtoupper($order->name_zone) ?></td>
                                                        <?php
                                                        $arr = ['paid', 'unpaid', 'complete', 'reject'];
                                                        if (in_array($order->approve, $arr)) {
                                                        ?>
                                                            <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i><?= $txt ?></td>
                                                            <td>:</td>
                                                            <td><?= strtoupper($order->name_approve_by) . '<span class="badge badge-pill m-l-5" style="color: #fff; background-color: #04a9f5;">' . strtoupper($order->level_approve) . '</span>' ?></td>
                                                        <?php } elseif ($order->approve == 'proses') { ?>
                                                            <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i><?= $txt ?></td>
                                                            <td>:</td>
                                                            <td><?= $order->remarks ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>CATEGORY PRODUCT</td>
                                                        <td>:</td>
                                                        <td><?= strtoupper($order->name_cat_product) ?></td>
                                                        <?php
                                                        $arr = ['paid', 'unpaid', 'complete', 'reject'];
                                                        if (in_array($order->approve, $arr)) {
                                                        ?>
                                                            <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i><?= $txt2 ?></td>
                                                            <td>:</td>
                                                            <td><?= strtoupper($order->acc_date) ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>SEGMENT</td>
                                                        <td>:</td>
                                                        <td><?= strtoupper($order->name_segment) ?></td>
                                                        <?php
                                                        if ($order->approve == 'unpaid' || $order->approve == 'proses' || $order->approve == 'reject') {
                                                        ?>
                                                            <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>REMARKS</td>
                                                            <td>:</td>
                                                            <td><?= $rmks ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>TYPE</td>
                                                        <td>:</td>
                                                        <td><?= strtoupper($order->new_type) ?></td>
                                                        <!-- <td style="font-weight: bold;"></td>
                                                        <td></td>
                                                        <td></td> -->
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>PRICE</td>
                                                        <td>:</td>
                                                        <td><?= 'RM ' . number_format($order->price, 2) ?></td>
                                                        <!-- <td style="font-weight: bold;"></td>
                                                        <td></td>
                                                        <td></td> -->
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" colspan="6"><i class="feather icon-edit-1 m-r-5" style="color: #04a9f5;"></i>SIGNATURE</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <?php 
                                                            if ($order->signature == '') {
                                                                echo '<center><b>No Signature Here !!</b></center>';
                                                            } else { 
                                                            ?>                                                            
                                                            <img src="data:image/jpeg;charset=utf-8;base64, <?= $order->signature ?>" style="width: 100%;"></td>
                                                            <?php 
                                                                }
                                                            ?>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- <img src="data:image/jpeg;charset=utf-8;base64, <?= $order->signature ?>" width="500px"> -->

                                        <!-- File Receipt -->
                                        <div class="row mb-3">
                                            <!-- [ tabs ] start -->
                                            <div class="col-sm-12">
                                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" id="file1-tab" data-toggle="tab" href="#file1" role="tab" aria-controls="home" aria-selected="true"><i class="feather icon-paperclip m-r-10"></i>File Receipt</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content" id="myTabContent">
                                                    <div class="tab-pane fade show active" id="file1" role="tabpanel" aria-labelledby="file1-tab">
                                                        <?php
                                                        if ($order->image_receipt == '') {
                                                            echo '<div class="alert alert-danger"><i class="feather icon-alert-circle m-r-10"></i>No File Receipt Upload.</div>';
                                                        } else {
                                                            echo '<iframe src="' . base_url() . 'assets/images/order/receipt/' . $order->image_receipt . '" class="img-responsive file-upload" alt="file-receipt" height="450px"></iframe>';
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- File Upload -->
                                        <div class="row mb-3">
                                            <!-- [ tabs ] start -->
                                            <div class="col-sm-12">
                                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" id="file1-tab" data-toggle="tab" href="#file1" role="tab" aria-controls="home" aria-selected="true"><i class="feather icon-paperclip m-r-10"></i>File 1</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="file2-tab" data-toggle="tab" href="#file2" role="tab" aria-controls="profile" aria-selected="false"><i class="feather icon-paperclip m-r-10"></i>File 2</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="file3-tab" data-toggle="tab" href="#file3" role="tab" aria-controls="contact" aria-selected="false"><i class="feather icon-paperclip m-r-10"></i>File 3</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="file4-tab" data-toggle="tab" href="#file4" role="tab" aria-controls="contact" aria-selected="false"><i class="feather icon-paperclip m-r-10"></i>File 4</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="file5-tab" data-toggle="tab" href="#file5" role="tab" aria-controls="contact" aria-selected="false"><i class="feather icon-paperclip m-r-10"></i>File 5</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content" id="myTabContent">
                                                    <div class="tab-pane fade show active" id="file1" role="tabpanel" aria-labelledby="file1-tab">
                                                        <?php
                                                        if ($order->image1 == '') {
                                                            echo '<div class="alert alert-danger"><i class="feather icon-alert-circle m-r-10"></i>No File Upload.</div>';
                                                        } else {
                                                            echo '<iframe src="' . base_url() . 'assets/images/order/' . $order->image1 . '" class="img-responsive file-upload" alt="attachment-file-1" height="450px"></iframe>';
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="tab-pane fade" id="file2" role="tabpanel" aria-labelledby="file2-tab">
                                                        <?php
                                                        if ($order->image2 == '') {
                                                            echo '<div class="alert alert-danger"><i class="feather icon-alert-circle m-r-10"></i>No File Upload.</div>';
                                                        } else {
                                                            echo '<iframe src="' . base_url() . 'assets/images/order/' . $order->image2 . '" class="img-responsive file-upload" alt="attachment-file-2" height="450px"></iframe>';
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="tab-pane fade" id="file3" role="tabpanel" aria-labelledby="file3-tab">
                                                        <?php
                                                        if ($order->image3 == '') {
                                                            echo '<div class="alert alert-danger"><i class="feather icon-alert-circle m-r-10"></i>No File Upload.</div>';
                                                        } else {
                                                            echo '<iframe src="' . base_url() . 'assets/images/order/' . $order->image3 . '" class="img-responsive file-upload" alt="attachment-file-3" height="450px"></iframe>';
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="tab-pane fade" id="file4" role="tabpanel" aria-labelledby="file4-tab">
                                                        <?php
                                                        if ($order->image4 == '') {
                                                            echo '<div class="alert alert-danger"><i class="feather icon-alert-circle m-r-10"></i>No File Upload.</div>';
                                                        } else {
                                                            echo '<iframe src="' . base_url() . 'assets/images/order/' . $order->image4 . '" class="img-responsive file-upload" alt="attachment-file-4" height="450px"></iframe>';
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="tab-pane fade" id="file5" role="tabpanel" aria-labelledby="file5-tab">
                                                        <?php
                                                        if ($order->image5 == '') {
                                                            echo '<div class="alert alert-danger"><i class="feather icon-alert-circle m-r-10"></i>No File Upload.</div>';
                                                        } else {
                                                            echo '<iframe src="' . base_url() . 'assets/images/order/' . $order->image5 . '" class="img-responsive file-upload" alt="attachment-file-5" height="450px"></iframe>';
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>