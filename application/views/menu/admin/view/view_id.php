<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">View ID</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Data ID</h5>
                                        <a href="<?= base_url('add_id') ?>" class="btn btn-primary"><i class="feather icon-plus"></i>Add New</a>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover" id="tbl-view-id">
                                                <thead>
                                                    <tr>
                                                        <th>NO.</th>
                                                        <th>DEPARTMENT</th>
                                                        <th>CODE</th>
                                                        <th>SUBMITTED</th>
                                                        <th>SUBMITTED DATE</th>
                                                        <th><i class="feather icon-settings"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Paid -->
<!-- <div class="modal fade" id="modalPaid" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Paid Receipt</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open_multipart('save_remarks/paid', 'method="post"') ?>
            <div class="modal-body">
                <div class="form-group">
                    <label>Order Number</label>
                    <p style="font-weight: bold; font-size: 16px;" id="paid1"></p>
                    <input type="hidden" name="id_order" id="paid2">
                </div>
                <div class="form-group">
                    <label>Upload Receipt</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input uploadReceiptImg" name="receipt" required oninvalid="this.setCustomValidity('Upload image receipt')" oninput="setCustomValidity('')">
                        <label class="custom-file-label">Choose file..</label>
                    </div>
                    <small style="color: red; font-size: 11px;">Max size file 1 megabyte. File <strong>jpeg, jpg, png</strong></small>
                    <div class="text-center">
                        <img src="#" class="img-responsive m-t-10" id="uploadReceipt" alt="image-receipt" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary"><i class="feather icon-check-circle"></i>Save</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div> -->

<!-- Modal Unpaid -->
<!-- <div class="modal fade" id="modalUnpaid" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Unpaid Remarks</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open('save_remarks/unpaid', 'method="post"') ?>
            <div class="modal-body">
                <div class="form-group">
                    <label>Order Number</label>
                    <p style="font-weight: bold; font-size: 16px;" id="unpaid1"></p>
                    <input type="hidden" name="id_order" id="unpaid2">
                </div>
                <div class="form-group">
                    <label>Remarks</label>
                    <textarea class="form-control" name="remarks" rows="4" placeholder="Input Remarks"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary"><i class="feather icon-check-circle"></i>Save</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div> -->

<!-- Modal Hapus -->
<div class="modal fade" id="modalDeleteId" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
            </div>
            <div class="modal-body text-center">
                <?= form_open('delete_id', 'method="post"') ?>
                <input type="hidden" name="id" id="idCode">
                Are you sure delete this data ?
                <br>
                <!-- </div>
          <div class="modal-footer"> -->
                <button type="button" class="btn btn-danger my-2" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary">Yes</button>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>