<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">View Package</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Data Package</h5>
                                        <a href="<?= base_url('add-package') ?>" class="btn btn-primary"><i class="feather icon-plus"></i>Add New</a>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover" id="tbl-package">
                                                <thead>
                                                    <tr>
                                                        <th class="to-center">NO.</th>
                                                        <th>PACKAGE</th>
                                                        <th>PACKAGE NAME</th>
                                                        <!-- <th>PRICE</th> -->
                                                        <th>SUBMITTED</th>
                                                        <th>SUBMITTED DATE</th>
                                                        <th class="text-center"><i class="feather icon-settings"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $no = 1;
                                                    foreach ($package_list as $val) {
                                                        $typ = ($val->type == 'S') ? 'SME' : 'CONSUMER';
                                                        $submitted = strtoupper($val->name) . '&nbsp;&nbsp;<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;">' . strtoupper($val->level) . '</span>';
                                                    ?>
                                                        <tr>
                                                            <td align="center"><?= $no . '.' ?></td>
                                                            <td><?= $typ ?></td>
                                                            <td>
                                                                <?= $val->package ?>
                                                                <?php
                                                                echo '<br>
                                                                <p class="text-danger mb-1" style="font-size: 11px;">Sub Package Item:</p>';
                                                                $get_sub = $this->db->get_where('tb_sub_package', ['id' => $val->id_pkg]);
                                                                if ($get_sub->num_rows() == 0) {
                                                                    echo '<mark class="f-11">No Sub Data.</mark>';
                                                                } else {
                                                                    $nomor = 1;
                                                                    foreach ($get_sub->result() as $sub) {
                                                                        echo '<span class="badge badge-warning f-11" style="font-size: 12px !important; padding: 5px; margin: 1.5px;">' .$nomor.') '. $sub->name .' - RM'.number_format($sub->price, 2). '</span><i class="fas fa-times-circle red-them del-sub" data-id="' . $sub->id_sub . '" data-toggle="tooltip" data-placement="top" title="Delete without confirmation !!" onclick="delSub('. $sub->id_sub.'); return false;"></i><br>';
                                                                    $nomor++;
                                                                    }
                                                                }
                                                                ?>
                                                            </td>
                                                            <!-- <td align="right"><?= 'RM' . $val->price ?></td> -->
                                                            <td><?= $submitted ?></td>
                                                            <td><?= '<i class="fas fa-circle text-c-green f-10 m-r-15"></i>' . $val->date . ', ' . $val->time ?></td>
                                                            <td align="center">
                                                                <button type="button" class="btn btn-primary btn-delete-modif addSubPackage" data-toggle="modal" data-target="#modalAddPackage" data-id="<?= $val->id_pkg ?>" data-package="<?= $val->package ?>" title="Add Detail Package"><i class="feather icon-plus" style="margin: 0 !important;"></i></button>
                                                                <button type="button" class="btn btn-danger btn-delete-modif btnDeletePackage" data-toggle="modal" data-target="#modalDeletePackage" data-id="<?= $val->id_pkg ?>" title="Delete"><i class="feather icon-trash" style="margin: 0 !important;"></i></button>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                        $no++;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Add Detail Package -->
<div class="modal fade" id="modalAddPackage" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add Sub Package <span id="ModaltxtPackage"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open('save-sub-package', 'method="post"') ?>
            <div class="modal-body">
                <input id="idPkg" name="id_pkg" type="hidden" />
                <input id="id" value="1" type="hidden" />
                <div class="form-group">
                    <button type="button" class="btn btn-dark" onclick="addSubPackage(); return false;"><i class="fas fa-plus"></i>Add New</button>
                </div>
                <!-- <div class="form-group"> -->
                    <div id="pemain"></div>
                <!-- </div> -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger my-2" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- Modal Hapus -->
<div class="modal fade" id="modalDeletePackage" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
            </div>
            <div class="modal-body text-center">
                <?= form_open('delete/package', 'method="post"') ?>
                <input type="hidden" name="id" id="idPackage">
                Are you sure delete this data ?
                <br>
                <!-- </div>
          <div class="modal-footer"> -->
                <button type="button" class="btn btn-danger my-2" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary">Yes</button>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>