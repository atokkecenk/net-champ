<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Setting Landing Page</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <div class="row">
                            <!-- Setting Landing Page -->
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Setting Text</h5>
                                    </div>
                                    <div class="card-body">
                                        <?php foreach ($setting as $sett) { ?>
                                            <?= form_open('setting-landing-text', 'method="post"') ?>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Title Tab</label>
                                                        <input class="form-control" name="title" value="<?= $sett->lp_title ?>" placeholder="Title Landing Page" required oninvalid="this.setCustomValidity('Input Title Landing Page')" oninput="this.setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Text</label>
                                                        <textarea class="form-control" rows="3" name="text" placeholder="Text Landing Page" required oninvalid="this.setCustomValidity('Input Text Landing Page')" oninput="this.setCustomValidity('')"><?= $sett->lp_text ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Update</button>
                                            </div>
                                            <?= form_close() ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Setting Background</h5>
                                    </div>
                                    <div class="card-body">
                                        <?php foreach ($setting as $sett) { ?>
                                            <?= form_open_multipart('setting-landing-background', 'method="post"') ?>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group card-upload">
                                                        <label>Background Image</label>
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input landing" name="background" required oninvalid="this.setCustomValidity('Please Select File')" oninput="setCustomValidity('')">
                                                            <label class="custom-file-label">Choose file..</label>
                                                        </div>
                                                        <small style="color: red; font-size: 11px;">Max size file 5 megabyte. File <strong>jpeg, jpg, png</strong></small>
                                                        <div class="table-responsive">
                                                            <div class="text-center">
                                                                <img src="<?= base_url() . 'assets/landing-page/img/' . $sett->lp_background ?>" class="img-responsive" id="landing" alt="favicon" width="510px" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Update</button>
                                            </div>
                                            <?= form_close() ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>