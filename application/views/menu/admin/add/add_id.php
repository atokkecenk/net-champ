<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Add ID</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Add Data ID</h5>
                                    </div>
                                    <div class="card-body">
                                        <?= form_open('save_id', 'method="post"') ?>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Department</label>
                                                    <input type="text" class="form-control" name="department" placeholder="Department Name" required oninvalid="this.setCustomValidity('Input Department Name')" oninput="setCustomValidity('')">
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="row">
                                                    <div class="form-group col-sm-3">
                                                        <label>Company Name</label>
                                                        <input type="text" class="form-control" name="company" placeholder="MT" required oninvalid="this.setCustomValidity('Input Company Name')" oninput="setCustomValidity('')">
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label>Department Code</label>
                                                        <input type="number" min="0" class="form-control" name="dept_code" placeholder="1000" required oninvalid="this.setCustomValidity('Input Departement Code')" oninput="setCustomValidity('')">
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label>Business Type</label>
                                                        <input type="text" class="form-control" name="type" placeholder="U" required oninvalid="this.setCustomValidity('Input Business Type')" oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <a href="<?= base_url('view_id') ?>" class="btn btn-secondary"><i class="feather icon-arrow-left"></i>Back</a>
                                            <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                        <?= form_close() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>