<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Add Package</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Add Package</h5>
                                    </div>
                                    <div class="card-body">
                                        <?= form_open('save-package', 'method="post"') ?>
                                        <div class="row">
                                            <!-- Kolom Kiri -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Package</label>
                                                    <select class="form-control" name="type" required oninvalid="this.setCustomValidity('Select One')" oninput="setCustomValidity('')">
                                                        <option value="">Nothing Selected</option>
                                                        <option value="S">SME</option>
                                                        <option value="C">CONSUMER</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label>Package Name</label>
                                                    <input type="text" class="form-control" name="package" placeholder="Package Name" required oninvalid="this.setCustomValidity('Input Package Name')" oninput="setCustomValidity('')">
                                                </div>

                                                <!-- <div class="form-group">
                                                    <label>Price</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">RM</span>
                                                        </div>
                                                        <input type="text" name="price" class="form-control" placeholder="20.55" required oninvalid="this.setCustomValidity('Input Harga Paket')" oninput="setCustomValidity('')">
                                                    </div>
                                                    <small class="form-text text-c-red">Use point (.)</small>
                                                </div> -->
                                            </div>
                                            <!-- Kolom Kanan -->
                                            <!-- <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Add Detail Package</label>
                                                    <div id="pemain"></div>
                                                </div>
                                            </div> -->
                                        </div>
                                        <div>
                                            <a href="<?= base_url('view-package') ?>" class="btn btn-secondary"><i class="feather icon-arrow-left"></i>Back</a>
                                            <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                        <?= form_close() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>