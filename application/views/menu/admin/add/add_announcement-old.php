<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Add Announcement</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Add Data Announcement</h5>
                                    </div>
                                    <div class="card-body">
                                        <?= form_open_multipart('save-announcement-img', 'method="post"') ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p style="background-color: #f44236; color: #fff; padding: 15px; border-radius: 5px;">
                                                    <i class="feather icon-alert-triangle m-r-5"></i>
                                                    <b>ANNOUNCEMENT WITH IMAGE</b>
                                                    <br><br>
                                                    1. Upload or Select image<br>
                                                    2. Copy link image<br>
                                                    3. Paste link image on Coloumn Text &rarr; Image &rarr; URL<br>
                                                    4. Setting width, height, border, etc (optional)<br>
                                                    5. Save
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Upload File</label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input uploadAnncImg" name="uploadAnncImg" required oninvalid="this.setCustomValidity('Please Select Image File')" oninput="this.setCustomValidity('')">
                                                        <label class="custom-file-label">Choose file..</label>
                                                    </div>
                                                    <small style="color: red; font-size: 11px;">Max size file 5 megabyte. File <strong>jpeg, jpg, png</strong></small>
                                                </div>
                                                <button type="submit" class="btn btn-primary"><i class="feather icon-share"></i>Upload File</button>
                                                <div class="m-t-5" id="uploadAnncImg"></div>
                                                <!-- <img src="#" class="img-responsive" id="uploadAnnc" alt="file-announcment" /> -->
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12" style="margin-bottom: 5px;">
                                                <label>Source URL</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="source" id="source" value="<?= $this->session->annc_source ?>"  placeholder="Source URL">
                                                    <div class="input-group-append">
                                                        <button type="button" class="btn btn-primary" data-toggle="tooltip" title="Copy Link" id="btnCopyURL">Copy</button>
                                                    </div>
                                                </div>
                                                <small class="text-c-red">Copy this URL to add image announcement</small>
                                            </div>
                                        </div>
                                        <?= form_close() ?>
                                        <hr>
                                        <?= form_open('save-announcement', 'method="post"') ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Subject</label>
                                                    <input type="text" class="form-control" name="subject" placeholder="Subject Announcement" required oninvalid="this.setCustomValidity('Input Subject')" oninput="setCustomValidity('')">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Publish</label>
                                                    <select class="form-control" name="publish">
                                                        <option value="">Nothing Selected</option>
                                                        <option value="publish">Publish</option>
                                                        <option value="draft">Draft</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Masa</label>
                                                    <select class="form-control" name="masa" id="masa">
                                                        <option value="">Nothing Selected</option>
                                                        <option value="U">Unlimited</option>
                                                        <option value="D">Set By Date</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group card-upload">
                                                    <label>Upload File</label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input upload1" name="uploadAnnc">
                                                        <label class="custom-file-label">Choose file..</label>
                                                    </div>
                                                    <small style="color: red; font-size: 11px;">Max size file 5 megabyte. File <strong>jpeg, jpg, png, pdf</strong></small>
                                                    <div class="text-center">
                                                        <img src="#" class="img-responsive" id="upload1" alt="file-announcment" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div id="setByDate2" style="display: none;">
                                                    <div class="form-group">
                                                        <label>Start Date</label>
                                                        <div class="input-group mb-3">
                                                            <input type="text" class="form-control" id="startDate" name="start_date" placeholder="dd/mm/yyyy">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="feather icon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div id="setByDate2" style="display: none;">
                                                    <div class="form-group">
                                                        <label>End Date</label>
                                                        <div class="input-group mb-3">
                                                            <input type="text" class="form-control" id="endDate" name="end_date" placeholder="dd/mm/yyyy">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="feather icon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Text</label>
                                                    <textarea class="form-control" name="text" id="textAnnoucement" rows="6" placeholder="Text Announcement" required oninvalid="this.setCustomValidity('Input Text Announcement')" oninput="this.setCustomValidity('')"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div id="setByDate2" style="display: none;">
                                                    <div class="form-group">
                                                        <label>Start Date</label>
                                                        <div class="input-group mb-3">
                                                            <input type="text" class="form-control" id="startDate" name="start_date" placeholder="dd/mm/yyyy">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="feather icon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div id="setByDate2" style="display: none;">
                                                    <div class="form-group">
                                                        <label>End Date</label>
                                                        <div class="input-group mb-3">
                                                            <input type="text" class="form-control" id="endDate" name="end_date" placeholder="dd/mm/yyyy">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="feather icon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <a href="<?= base_url('view-announcement') ?>" class="btn btn-secondary"><i class="feather icon-arrow-left"></i>Back</a>
                                            <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                        <?= form_close() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>