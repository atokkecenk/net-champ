<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Add Announcement</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Add Data Announcement</h5>
                                    </div>
                                    <div class="card-body">
                                        <?= form_open_multipart('save-announcement', 'method="post"') ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Subject</label>
                                                    <input type="text" class="form-control" name="subject" placeholder="Subject Announcement" required oninvalid="this.setCustomValidity('Input Subject')" oninput="setCustomValidity('')">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Upload Image</label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input imageAnnc" onchange="return checkimg()" name="image">
                                                        <label class="custom-file-label">Choose file..</label>
                                                    </div>
                                                    <small class="text-c-red">Max file size 5 mb. Extension jpg, jpeg, png</small>
                                                </div>
                                                <div id="messageImageAnnouncement" style="margin-bottom: 10px;"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Publish</label>
                                                    <select class="form-control" name="publish">
                                                        <option value="">Nothing Selected</option>
                                                        <option value="publish">Publish</option>
                                                        <option value="draft">Draft</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Masa</label>
                                                    <select class="form-control" name="masa" id="masa">
                                                        <option value="">Nothing Selected</option>
                                                        <option value="U">Unlimited</option>
                                                        <option value="D">Set By Date</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div id="setByDate2" style="display: none;">
                                                    <div class="form-group">
                                                        <label>Start Date</label>
                                                        <div class="input-group mb-3">
                                                            <input type="text" class="form-control" id="startDate" name="start_date" placeholder="dd/mm/yyyy">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="feather icon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div id="setByDate2" style="display: none;">
                                                    <div class="form-group">
                                                        <label>End Date</label>
                                                        <div class="input-group mb-3">
                                                            <input type="text" class="form-control" id="endDate" name="end_date" placeholder="dd/mm/yyyy">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="feather icon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Text</label>
                                                    <textarea class="form-control" name="text" id="textAnnoucement" rows="6" placeholder="Text Announcement" required oninvalid="this.setCustomValidity('Input Text Announcement')" oninput="this.setCustomValidity('')"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <a href="<?= base_url('view-announcement') ?>" class="btn btn-secondary"><i class="feather icon-arrow-left"></i>Back</a>
                                            <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                        <?= form_close() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function checkimg() {
        var msg = document.querySelector('#messageImageAnnouncement');
        msg.innerHTML = '<span class="text-success"><i class="feather icon-upload"></i> <b>File ready to upload.</b></span>';
    }
</script>