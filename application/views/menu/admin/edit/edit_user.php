<?php
$myid = $user->awal . $user->tengah . $user->akhir;
?>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Edit User</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        Edit Data User <span class="badge" style="font-size: 15px; color: #fff; background-color: #f44236;">ID: <?= $myid ?></span>
                                    </div>
                                    <div class="card-body">
                                        <?= form_open('save-edit-user', 'method="post"') ?>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <input type="hidden" name="id" value="<?= $user->id ?>">
                                                <div class="form-group">
                                                    <label>Credential Pilihan</label>
                                                    <select class="form-control" name="level" id="level" required oninvalid="this.setCustomValidity('Select Credential')" oninput="setCustomValidity('')">
                                                        <option value="">Nothing Selected</option>
                                                        <option value="admin" <?php if ($user->level == 'admin') {
                                                                                    echo 'selected';
                                                                                } ?>>ADMIN</option>
                                                        <option value="leader" <?php if ($user->level == 'leader') {
                                                                                    echo 'selected';
                                                                                } ?>>LEADER</option>
                                                        <option value="user" <?php if ($user->level == 'user') {
                                                                                    echo 'selected';
                                                                                } ?>>USER</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Nama Penuh</label>
                                                    <input type="text" class="form-control" name="name" placeholder="Nama Penuh" value="<?= $user->name ?>" required oninvalid="this.setCustomValidity('Input Nama Penuh')" oninput="setCustomValidity('')">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5" id="divLeader" <?php if ($user->level == 'user') {
                                                                                        echo 'style="display: block;"';
                                                                                    } else {
                                                                                        echo 'style="display: none;"';
                                                                                    } ?>>
                                                <div class="form-group">
                                                    <label>Leader</label>
                                                    <select class="form-control" name="leader">
                                                        <option value="">Nothing Selected</option>
                                                        <?php
                                                        $idleader = $this->db->query("SELECT leader AS id_leader FROM tb_user where id='$user->id'")->row()->id_leader;
                                                        foreach ($leader as $lead) {
                                                        ?>
                                                            <option value="<?= $lead->id ?>" <?php if ($idleader == $lead->id) {
                                                                                                    echo 'selected';
                                                                                                } ?>><?= strtoupper($lead->level) . ' &rarr; ' . ucwords(strtolower($lead->name)) ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Username</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="username" id="username" oninput="cekUsername()" value="<?= $user->username ?>" placeholder="Username" required oninvalid="this.setCustomValidity('Input Username')" oninput="setCustomValidity('')">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="feather icon-user"></i></span>
                                                        </div>
                                                    </div>
                                                    <p id="usernameCek"></p>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>IC Number</label>
                                                    <input type="text" class="form-control" name="ic_num" placeholder="IC Number" value="<?= str_replace('-', '', $user->number_ic) ?>" maxlength="12">
                                                    <small class="form-text text-c-red">Input without strip (-)</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <div class="input-group mb-3">
                                                        <input type="password" class="form-control" name="password" placeholder="Password" id="password">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="passIcon"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="showPassword" onclick="showPass();">
                                                    <label class="custom-control-label" for="showPassword">Show Password</label>
                                                </div>
                                            </div> -->

                                            <div class="col-md-5">
                                                <label>Staff ID</label>
                                                <div class="form-group">
                                                    <?php
                                                    $no = 1;
                                                    foreach ($data_id as $da_id) {
                                                        $chk = $da_id->id == $user->id_code ? 'checked' : null;
                                                    ?>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="radioTypeId<?= $no ?>" name="type_id" class="custom-control-input" <?= $chk ?> value="<?= $da_id->id ?>">
                                                            <label class="custom-control-label" for="radioTypeId<?= $no ?>"><?= $da_id->new_id ?></label>
                                                        </div>
                                                    <?php
                                                        $no++;
                                                    }
                                                    ?>
                                                    <br>
                                                    <small class="text-c-red">NOTE : IF YOU WANT TO CHANGE STAFF ID, CHOOSE ONE. IF NOT, IGNORE IT.</small>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Passport</label>
                                                    <input type="text" class="form-control" name="passport" value="<?= $user->passport ?>" placeholder="Passport">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>No HP</label>
                                                    <div class="input-group mb-3">
                                                        <input type="text" class="form-control" name="no_hp" placeholder="No HP" value="<?= $user->number_hp ?>" required oninvalid="this.setCustomValidity('Input No HP')" oninput="setCustomValidity('')">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="feather icon-smartphone"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Alamat Tempat Tinggal</label>
                                                    <textarea class="form-control" name="alamat" rows="2" placeholder="Alamat Tempat Tinggal" required oninvalid="this.setCustomValidity('Input Alamat Tempat Tinggal')" oninput="setCustomValidity('')"><?= $user->alamat ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">

                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <div class="input-group mb-3">
                                                        <input type="email" class="form-control" name="email" placeholder="Email Address" value="<?= $user->email ?>" required oninvalid="this.setCustomValidity('Input Email Address')" oninput="setCustomValidity('')">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="feather icon-mail"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <a href="<?= base_url('view_user_admin') ?>" class="btn btn-secondary"><i class="feather icon-arrow-left"></i>Back</a>
                                            <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                        <?= form_close() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>