<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">View Order</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Data Order</h5>
                                        <a href="<?= base_url('input_order') ?>" class="btn btn-primary"><i class="feather icon-plus"></i>Add New</a>
                                    </div>
                                    <div class="card-body">
                                        <input type="hidden" id="modeViewOrder" value="<?= $mode ?>">
                                        <div class="table-responsive">
                                            <table class="table table-hover" id="tbl-view-order-leader">
                                                <thead>
                                                    <tr>
                                                        <th>NO.</th>
                                                        <th>ORDER NUMBER</th>
                                                        <th>IC NUMBER</th>
                                                        <th>CUSTOMER NAME</th>
                                                        <th>SALES NAME</th>
                                                        <th>ZONE</th>
                                                        <!-- <th>SEGMENT</th> -->
                                                        <th>TYPE</th>
                                                        <th>PRICE</th>
                                                        <th>DATE TIME</th>
                                                        <th>STATUS</th>
                                                        <!-- <th>REMARKS</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Pending User -->
<div class="modal fade" id="modalPendingUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Status Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open('save_order_number', 'method="post"') ?>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="3">DETAIL ORDER</th>
                            </tr>
                            <tr>
                                <?= form_open('save_order_number', 'method="post"') ?>
                                <th width="27%">Order Number</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;">
                                    <input type="hidden" name="id" id="pendingDetailOrderId">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="order_num" id="pendingDetailOrderNum" placeholder="Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary" title="Save or Update Order Number"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                    </div>
                                </th>
                                <?= form_close() ?>
                            </tr>
                            <tr>
                                <th width="27%">Number IC</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;"><span id="pendingDetailUserNumIc"></span></th>
                            </tr>
                            <tr>
                                <th width="27%">Submitted</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;"><span id="pendingDetailUserSubmit"></span></th>
                            </tr>
                            <tr>
                                <th width="27%">Submitted Date</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;"><span id="pendingDetailUserDate"></span></th>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="pendingDetailUserSts"></span></th>
                            </tr>
                            <tr>
                                <th>Remarks</th>
                                <th>:</th>
                                <th><span class="text-danger" style="font-weight: normal;" id="pendingDetailUserRemarks"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <span id="editPendingOrderBtn"></span>
                <!-- <?= form_close() ?> -->
            </div>
        </div>
    </div>
</div>

<!-- Modal Paid User -->
<div class="modal fade" id="modalPaidUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Status Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="3">DETAIL ORDER</th>
                            </tr>
                            <tr>
                                <?= form_open('save_order_number', 'method="post"') ?>
                                <th width="27%">Order Number</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;">
                                    <input type="hidden" name="id" id="paidDetailOrderId">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="order_num" id="paidDetailOrderNum" placeholder="Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary" title="Save or Update Order Number"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                    </div>
                                </th>
                                <?= form_close() ?>
                            </tr>
                            <tr>
                                <th>Number IC</th>
                                <th>:</th>
                                <th style="font-weight: normal;"><span id="paidDetailUserNumIc"></span></th>
                            </tr>
                            <tr>
                                <th>Approve By</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="paidDetailUserAcc"></span></th>
                            </tr>
                            <tr>
                                <th>Approve Date</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="paidDetailUserAccDate"></span></th>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="paidDetailUserSts"></span></th>
                            </tr>
                            <tr>
                                <th class="text-center" colspan="3">Receipt</th>
                            </tr>
                            <tr>
                                <th class="text-center" colspan="3">
                                    <iframe src="#" id="paidDetailUserRcp" width="100%" height="450px"></iframe>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <span id="editPaidOrderBtn"></span>
            </div>
        </div>
    </div>
</div>

<!-- Modal Unpaid User -->
<div class="modal fade" id="modalUnpaidUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Status Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="3">DETAIL ORDER</th>
                            </tr>
                            <tr>
                                <?= form_open('save_order_number', 'method="post"') ?>
                                <th width="27%">Order Number</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;">
                                    <input type="hidden" name="id" id="unpaidDetailOrderId">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="order_num" id="unpaidDetailOrderNum" placeholder="Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary" title="Save or Update Order Number"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                    </div>
                                </th>
                                <?= form_close() ?>
                            </tr>
                            <tr>
                                <th>Number IC</th>
                                <th>:</th>
                                <th style="font-weight: normal;"><span id="unpaidDetailUserNumIc"></span></th>
                            </tr>
                            <tr>
                                <th>Reject By</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="unpaidDetailUserAcc"></span></th>
                            </tr>
                            <tr>
                                <th>Reject Date</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="unpaidDetailUserAccDate"></span></th>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="unpaidDetailUserSts"></span></th>
                            </tr>
                            <tr>
                                <th>Remarks</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="unpaidDetailUserRmk"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <span id="editUnpaidOrderBtn"></span>
            </div>
        </div>
    </div>
</div>

<!-- Modal Cancel User -->
<div class="modal fade" id="modalCancelUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Status Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="3">DETAIL ORDER</th>
                            </tr>
                            <tr>
                                <?= form_open('save_order_number', 'method="post"') ?>
                                <th width="27%">Order Number</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;">
                                    <input type="hidden" name="id" id="cancelDetailOrderId">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="order_num" id="cancelDetailOrderNum" placeholder="Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary" title="Save or Update Order Number"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                    </div>
                                </th>
                                <?= form_close() ?>
                            </tr>
                            <tr>
                                <th>Number IC</th>
                                <th>:</th>
                                <th style="font-weight: normal;"><span id="cancelDetailUserNumIc"></span></th>
                            </tr>
                            <tr>
                                <th>Reject By</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="cancelDetailUserAcc"></span></th>
                            </tr>
                            <tr>
                                <th>Reject Date</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="cancelDetailUserAccDate"></span></th>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="cancelDetailUserSts"></span></th>
                            </tr>
                            <tr>
                                <th>Remarks</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="cancelDetailUserRmk"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <span id="editCancelOrderBtn"></span>
            </div>
        </div>
    </div>
</div>

<!-- Modal Pending User -->
<div class="modal fade" id="modalPendingUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Status Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open('save_order_number', 'method="post"') ?>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="3">DETAIL ORDER</th>
                            </tr>
                            <tr>
                                <?= form_open('save_order_number', 'method="post"') ?>
                                <th width="27%">Order Number</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;">
                                    <input type="hidden" name="id" id="pendingDetailOrderId">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="order_num" id="pendingDetailOrderNum" placeholder="Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary" title="Save or Update Order Number"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                    </div>
                                </th>
                                <?= form_close() ?>
                            </tr>
                            <tr>
                                <th width="27%">Number IC</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;"><span id="pendingDetailUserNumIc"></span></th>
                            </tr>
                            <tr>
                                <th width="27%">Submitted</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;"><span id="pendingDetailUserSubmit"></span></th>
                            </tr>
                            <tr>
                                <th width="27%">Submitted Date</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;"><span id="pendingDetailUserDate"></span></th>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="pendingDetailUserSts"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <span id="editPendingOrderBtn"></span>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Reject User -->
<div class="modal fade" id="modalRejectUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Status Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="3">DETAIL ORDER</th>
                            </tr>
                            <tr>
                                <?= form_open('save_order_number', 'method="post"') ?>
                                <th width="27%">Order Number</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;">
                                    <input type="hidden" name="id" id="rejectDetailOrderId">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="order_num" id="rejectDetailOrderNum" placeholder="Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary" title="Save or Update Order Number"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                    </div>
                                </th>
                                <?= form_close() ?>
                            </tr>
                            <tr>
                                <th>Number IC</th>
                                <th>:</th>
                                <th style="font-weight: normal;"><span id="rejectDetailUserNumIc"></span></th>
                            </tr>
                            <tr>
                                <th>Reject By</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="rejectDetailUserAcc"></span></th>
                            </tr>
                            <tr>
                                <th>Reject Date</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="rejectDetailUserAccDate"></span></th>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="rejectDetailUserSts"></span></th>
                            </tr>
                            <tr>
                                <th>Remarks</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="rejectDetailUserRmk"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <span id="editRejectOrderBtn"></span>
            </div>
        </div>
    </div>
</div>

<!-- Modal Press User -->
<div class="modal fade" id="modalPressUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Status Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open('save_order_number', 'method="post"') ?>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="3">DETAIL ORDER</th>
                            </tr>
                            <tr>
                                <?= form_open('save_order_number', 'method="post"') ?>
                                <th width="27%">Order Number</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;">
                                    <input type="hidden" name="id" id="pressDetailOrderId">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="order_num" id="pressDetailOrderNum" placeholder="Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary" title="Save or Update Order Number"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                    </div>
                                </th>
                                <?= form_close() ?>
                            </tr>
                            <tr>
                                <th width="27%">Number IC</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;"><span id="pressDetailUserNumIc"></span></th>
                            </tr>
                            <tr>
                                <th width="27%">Submitted</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;"><span id="pressDetailUserSubmit"></span></th>
                            </tr>
                            <tr>
                                <th width="27%">Submitted Date</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;"><span id="pressDetailUserDate"></span></th>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="pressDetailUserSts"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <span id="editPressOrderBtn"></span>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Proses User -->
<div class="modal fade" id="modalProsesUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Status Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open('save_order_number', 'method="post"') ?>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="3">DETAIL ORDER</th>
                            </tr>
                            <tr>
                                <?= form_open('save_order_number', 'method="post"') ?>
                                <th width="27%">Order Number</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;">
                                    <input type="hidden" name="id" id="prosesDetailOrderId">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="order_num" id="prosesDetailOrderNum" placeholder="Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary" title="Save or Update Order Number"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                    </div>
                                </th>
                                <?= form_close() ?>
                            </tr>
                            <tr>
                                <th width="27%">Number IC</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;"><span id="prosesDetailUserNumIc"></span></th>
                            </tr>
                            <tr>
                                <th width="27%">Submitted</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;"><span id="prosesDetailUserSubmit"></span></th>
                            </tr>
                            <tr>
                                <th width="27%">Submitted Date</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;"><span id="prosesDetailUserDate"></span></th>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="prosesDetailUserSts"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <span id="editProsesOrderBtn"></span>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Complete User -->
<div class="modal fade" id="modalCompleteUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Status Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open('save_order_number', 'method="post"') ?>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="3">DETAIL ORDER</th>
                            </tr>
                            <tr>
                                <?= form_open('save_order_number', 'method="post"') ?>
                                <th width="27%">Order Number</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;">
                                    <input type="hidden" name="id" id="completeDetailOrderId">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="order_num" id="completeDetailOrderNum" placeholder="Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary" title="Save or Update Order Number"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                    </div>
                                </th>
                                <?= form_close() ?>
                            </tr>
                            <tr>
                                <th width="27%">Number IC</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;"><span id="completeDetailUserNumIc"></span></th>
                            </tr>
                            <tr>
                                <th width="27%">Submitted</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;"><span id="completeDetailUserSubmit"></span></th>
                            </tr>
                            <tr>
                                <th width="27%">Submitted Date</th>
                                <th width="2%">:</th>
                                <th width="71%" style="font-weight: normal;"><span id="completeDetailUserDate"></span></th>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <th>:</th>
                                <th><span style="font-weight: normal;" id="completeDetailUserSts"></span></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <span id="editCompleteOrderBtn"></span>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>