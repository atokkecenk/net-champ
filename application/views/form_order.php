﻿<?php
$sett = $this->db->get_where('tb_setting', array('id' => '1'))->row();
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Register - <?= $sett->title ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="icon" href="<?= base_url() ?>assets/images/setting/<?= $sett->favicon ?>" type="image/x-icon">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/vendor/bootstrap/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">

	<!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/vendor/animate/animate.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/vendor/css-hamburgers/hamburgers.min.css"> -->

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css">

	<!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/vendor/select2/select2.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/vendor/daterangepicker/daterangepicker.css"> -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/sweetalert/sweetalert.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/css/main.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/myscript/mystyle.css">

	<!-- Signature -->
	<!-- [if lt IE 9]>
	<script type="text/javascript" src="<?= base_url() ?>assets/libs/flashcanvas.js"></script>
	<![endif] -->

	<style>
		#signature {
			width: 100%;
			height: 200px;
			border-radius: 6px;
			border: 1px solid black;
			color: red;
			margin-bottom: .6em;
		}
	</style>
</head>

<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<?php
				if ($this->session->flashdata('error')) {
					echo '<div class="col-sm-12 mt-3">
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						' . $this->session->flashdata('error') . '
						<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="right: -0.5rem; padding: .75rem .2rem;">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>';
				} elseif ($this->session->flashdata('success')) {
					echo '<div class="col-sm-12 mt-3">
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						' . $this->session->flashdata('success') . '
						<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="right: -0.5rem; padding: .75rem .2rem;">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>';
				}
				?>
				<div class="title-form">
					FORM ORDER - MTECH
				</div>
				<?= form_open_multipart('save-order-customer', 'class="login100-form validate-form" method="post" style="padding: 50px;" target="_blank"') ?>
				<div class="row col-md-12">
					<div class="form-group">
						<label>Segment <span style="color: red;">*</span></label>
						<div class="radio mt-1">
							<?php
							$i = 1;
							foreach ($segmt as $sgt) {
							?>
								<div class="custom-control custom-radio">
									<label>
										<input type="radio" class="segment" name="segment" value="<?= $sgt->id ?>">
										<?= $sgt->name ?></label>
								</div>
							<?php
								$i++;
							}
							?>
						</div>
					</div>
				</div>
				<div class="wrap-input100 m-b-26">
					<label>Customer Name <span style="color: red;">*</span></label>
					<input class="input100" type="text" name="cust_name" placeholder="Abu Yahya Bin Ali" required oninvalid="this.setCustomValidity('Input Customer Name')" oninput="this.setCustomValidity('')">
				</div>
				<div class="wrap-input100 m-b-26">
					<label>IC Number <span style="color: red;">*</span></label>
					<input class="input100 numberIc" type="text" name="number_ic" placeholder="123456-00-1122" maxlength="14" required oninvalid="this.setCustomValidity('Input IC Number')" oninput="this.setCustomValidity('')">
				</div>
				<div class="wrap-input100 m-b-26">
					<label>Passport <span style="color: red;">*</span></label>
					<input class="input100" type="text" name="passport" placeholder="Passport Number" maxlength="12" required oninvalid="this.setCustomValidity('Input Passport Number')" oninput="this.setCustomValidity('')">
				</div>
				<div class="col-md-12" id="loadTop"></div>
				<div class="row col-md-12 mt-2">
					<div class="form-group">
						<label>Type <span style="color: red;">*</span></label>
						<div class="radio mt-1">
							<?php
							$i = 1;
							foreach ($type as $typ) {
							?>
								<div class="custom-control custom-radio">
									<label>
										<input type="radio" name="type" value="<?= $typ->id ?>">
										<?= $typ->value ?></label>
								</div>
							<?php
								$i++;
							}
							?>
						</div>
					</div>
				</div>
				<div class="form-group col-md-12 mt-2 mb-4">
					<div class="row">
						<label class="mb-2">Category Product <span style="color: red;">*</span></label>
						<select class="form-control" name="cat_product" required oninvalid="this.setCustomValidity('Select Category Product')" oninput="setCustomValidity('')">
							<option value="">Nothing Selected</option>
							<?php
							foreach ($cat as $ct) {
							?>
								<option value="<?= $ct->id ?>"><?= $ct->name ?></option>
							<?php
							}
							?>
						</select>
					</div>
				</div>
				<div class="wrap-input100 m-b-26">
					<label>Installation Date <span style="color: red;">*</span></label>
					<input class="input100 datepicker" type="text" name="date" placeholder="dd/mm/yyyy">
				</div>
				<!-- <div class="form-group col-md-12 m-b-26">
					<div class="row">
						<label>Zone <span style="color: red;">*</span></label>
						<select class="form-control" name="zone" required oninvalid="this.setCustomValidity('Select Zone')" oninput="setCustomValidity('')">
							<option value="">Nothing Selected</option>
							<?php
							foreach ($zone as $zn) {
							?>
								<option value="<?= $zn->id ?>"><?= $zn->name ?></option>
							<?php
							}
							?>
						</select>
					</div>
				</div>
				<div class="form-group col-md-12 m-b-26">
					<div class="row">
						<label>Sales <span style="color: red;">*</span></label>
						<select class="form-control" name="sales" required oninvalid="this.setCustomValidity('Select Sales')" oninput="setCustomValidity('')">
							<option value="">Nothing Selected</option>
							<?php
							foreach ($sales as $sls) {
							?>
								<option value="<?= $sls->id ?>"><?= strtolower($sls->level) . ' &rarr; ' . ucwords(strtolower($sls->name)) ?></option>
							<?php
							}
							?>
						</select>
					</div>
				</div> -->

				<!-- <div class="form-group col-md-12 mb-4">
					<div class="row">
						<label class="mb-2">Segment <span style="color: red;">*</span></label>
						<select class="form-control" name="segment" required oninvalid="this.setCustomValidity('Select Segment')" oninput="setCustomValidity('')">
							<option value="">Nothing Selected</option>
							<?php
							foreach ($segmt as $sgt) {
							?>
								<option value="<?= $sgt->id ?>"><?= $sgt->name ?></option>
							<?php
							}
							?>
						</select>
					</div>
				</div>
				<div class="form-group col-md-12 mb-4">
					<div class="row">
						<label class="mb-2">Type <span style="color: red;">*</span></label>
						<select class="form-control" name="segment" required oninvalid="this.setCustomValidity('Select Type')" oninput="setCustomValidity('')">
							<option value="">Nothing Selected</option>
							<?php
							foreach ($type as $typ) {
							?>
								<option value="<?= $typ->id ?>"><?= ucwords(strtolower($typ->value)) ?></option>
							<?php
							}
							?>
						</select>
					</div>
				</div> -->
				<div class="wrap-input100 m-b-26">
					<label>Address <span style="color: red;">*</span></label>
					<input class="input100" type="text" name="address" placeholder="Your Address" required oninvalid="this.setCustomValidity('Input Address')" oninput="setCustomValidity('')">
				</div>
				<div class="wrap-input100 m-b-26">
					<label>Mobile Phone <span style="color: red;">*</span></label>
					<input class="input100" type="text" name="mobile_phone" placeholder="0123456789" required oninvalid="this.setCustomValidity('Input Mobile Phone Number')" oninput="setCustomValidity('')">
				</div>
				<div class="wrap-input100 m-b-26">
					<label>Email <span style="color: red;">*</span></label>
					<input class="input100" type="email" name="email" placeholder="farid@gmail.com" required oninvalid="this.setCustomValidity('Input Email Address')" oninput="setCustomValidity('')">
				</div>
				<div class="wrap-input100 m-b-26">
					<label>Del Number</label>
					<input class="input100" type="text" name="del_number" placeholder="Del Number">
				</div>
				<div class="col-md-12" id="loadBottom"></div>
				<!-- <div class="wrap-input100 m-b-26">
					<label>Press4U</label>
					<input class="input100" type="text" name="press4u" placeholder="Press4U">
				</div>
				<div class="wrap-input100 m-b-26">
					<label>eForm ID</label>
					<input class="input100" type="text" name="eform_id" placeholder="eForm ID">
				</div>
				<div class="wrap-input100 m-b-5">
					<label>Price <span style="color: red;">*</span></label>
					<input class="input100" type="text" name="harga_paket" placeholder="28.50" required oninvalid="this.setCustomValidity('Input Harga Paket')" oninput="setCustomValidity('')">
				</div> -->
				<!-- <small class="red">Use point (.)</small> -->
				<div id="signature"></div>
				<div class="center">
					<input type="button" class="btn" id="click" value="Click To Finish Signature">
				</div>
				<!-- <textarea id="output" name="signature"></textarea></br> -->
				<input type="hidden" name="hdnSignature" id="hdnSignature" />
				<!-- Preview image -->
				<img src="" id="sign_prev" style="display: none;" />
				<div class="container-login100-form-btn mt-4">
					<button type="submit" class="login100-form-btn submit" style="width: 100% !important;">
						Save | Send Email | Whatsapp
					</button>
				</div>
				<?= form_close() ?>
				<!-- <div class="footer"> -->
				<!-- <?= $sett->name ?> &copy; <?= date('Y') ?> -->
				<!-- </div> -->
			</div>
		</div>
	</div>

	<script src="<?= base_url() ?>assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>

	<script src="<?= base_url() ?>assets/login/vendor/animsition/js/animsition.min.js"></script>

	<script src="<?= base_url() ?>assets/login/vendor/bootstrap/js/popper.js"></script>
	<script src="<?= base_url() ?>assets/login/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?= base_url() ?>assets/login/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

	<!-- <script src="<?= base_url() ?>assets/login/vendor/select2/select2.min.js"></script> -->
	<!-- <script src="<?= base_url() ?>assets/login/vendor/daterangepicker/moment.min.js"></script> -->
	<!-- <script src="<?= base_url() ?>assets/login/vendor/daterangepicker/daterangepicker.js"></script> -->

	<!-- <script src="<?= base_url() ?>assets/login/vendor/countdowntime/countdowntime.js"></script> -->
	<script src="<?= base_url() ?>assets/sweetalert/sweetalert.min.js"></script>
	<script src="<?= base_url() ?>assets/login/js/main.js"></script>
	<!-- <script src="<?= base_url() ?>assets/myscript/myscript.js"></script> -->

	<script src="<?= base_url() ?>assets/signature/libs/jSignature.min.js"></script>
	<script src="<?= base_url() ?>assets/signature/libs/modernizr.js"></script>
	<script>
		$(document).on('change','.orderSubPackage', function () {
			var id = $(this).val();
			// console.log('berubah '+id);
			$.ajax({
				url: "get-sub-package",
				method: "POST",
				data: {
					id: id,
				},
				async: true,
				dataType: "json",
				success: function (data) {
					var html = "";
					var i;
					html += '<select class="form-control" name="sub_package">';
					html += '<option value = "" > Select Sub '+data.Name+'</option>';
					for (i = 0; i < data['Result'].length; i++) {
						html +=
							"<option value=" +
							data['Result'][i].id_sub +
							" >" +
							data['Result'][i].name +
							"</option>";
					}
					html += "</select>";
					$(".orderSubPackageBefore").hide();
					$(".subPackageList").html(html);
				},
			});
			return false;
		});

		$(document).ready(function() {
			// Initialize jSignature
			var $sigdiv = $("#signature").jSignature({
				'UndoButton': true
			});


			$('#click').click(function() {
				// Get response of type image
				var data = $sigdiv.jSignature('getData', 'image');
				$('#hdnSignature').val(data[1]);
				// Storing in textarea
				$('#output').val(data);

				// Alter image source 
				$('#sign_prev').attr('src', "data:" + data);
				$('#sign_prev').show();
			});

			$('.datepicker').datepicker({
				format: "dd/mm/yyyy",
				autoclose: true,
				todayHighlight: true,
				todayBtn: "linked"
			});

			$(document).on('keyup', '.numberIc', function() {
				var nilai = $(this).val();
				if (nilai.length == 6 || nilai.length == 9) {
					this.value += '-';
				}
			});

			$(document).on('click', '.segment', function() {
				var nilai = $(this).val();
				// console.log('nilai : '+nilai);
				if (nilai == '2') {
					$('#loadTop').load('<?= base_url('Loadfile/sme_top') ?>');
					$('#loadBottom').load('<?= base_url('Loadfile/sme_upload') ?>');
				} else {
					$('#loadTop').load('<?= base_url('Loadfile/consumer_top') ?>');
					$('#loadBottom').load('<?= base_url('Loadfile/consumer_upload') ?>');
				}
			});

		});
	</script>
	<!-- <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script> -->
	<!-- <script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-23581568-13');
	</script> -->
</body>

</html>