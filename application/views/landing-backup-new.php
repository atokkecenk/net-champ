<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?= base_url() ?>assets/images/setting/<?= $setting->favicon ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    <?= $setting->lp_title ?>
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> -->
  <link href="<?= base_url() ?>assets/landing-page/fa-icon/css/all.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="<?= base_url() ?>assets/landing-page/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?= base_url() ?>assets/landing-page/css/now-ui-dashboard.css?v=1.3.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?= base_url() ?>assets/landing-page/demo/demo.css" rel="stylesheet" />
</head>

<body class="offline-doc">
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
    <div class="container">
      <div class="navbar-wrapper">
        <div class="navbar-toggle">
          <button type="button" class="navbar-toggler">
            <span class="navbar-toggler-bar bar1"></span>
            <span class="navbar-toggler-bar bar2"></span>
            <span class="navbar-toggler-bar bar3"></span>
          </button>
        </div>
        <a class="navbar-brand" href="#">Landing Page <?= $setting->name ?></a>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-bar navbar-kebab"></span>
        <span class="navbar-toggler-bar navbar-kebab"></span>
        <span class="navbar-toggler-bar navbar-kebab"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navigation">
        <ul class="navbar-nav">
          <li class="nav-item">
            <!-- <a class="nav-link" href="#" onclick="openOrder()">
              <i class="fas fa-box-open mr-2"></i>Order Now
            </a> -->
            <a class="nav-link" href="<?= base_url('register') ?>" target="_blank">
              <i class="fas fa-user-plus mr-1"></i>Register
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('login') ?>">
              <i class="fas fa-sign-in-alt mr-1"></i>Portal Login
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar -->
  <div class="page-header clear-filter" filter-color="orange">
    <div class="page-header-image" style="background-image: url('<?= base_url('assets/landing-page/img/' . $setting->lp_background) ?>');"></div>
    <div class="container text-center">
      <div class="col-md-8 ml-auto mr-auto">
        <div class="brand">
          <h1 class="title">
            <?= $setting->lp_text ?>
          </h1>
          <!-- <h3 class="description">Documentation v1.3.0</h3> -->
          <br />
          <?= form_open('search', 'method="post"') ?>
          <div class="row">
            <div class="col-md-8">
              <input type="text" class="form-control mt-2" name="search" placeholder="Search by Order Number, Name Customer, Sales Agent..." style="height: 50px !important; font-size: 14px !important; background-color: #fff;" required oninvalid="this.setCustomValidity('Please Input This Field')" oninput="setCustomValidity('')">
            </div>
            <div class="col-md-4">
              <button type="submit" class="btn btn-primary btn-round btn-lg"><i class="fas fa-search mr-2"></i>Search Order</button>
            </div>
          </div>
          <?= form_close() ?>
          <?php
          if (isset($search)) {
            if ($search->num_rows() < 1) {
              echo '
              <div class="row mt-3">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body" style="color: #000 !important;">
                        <h4 style="padding: 0; margin: 0;"><strong>Data Not Found..</strong></h4>
                    </div>
                  </div>
                </div>
              </div>
              ';
            } else {
              echo '<div class="row mt-3">
                  <div class="col-md-12">
                    <div class="card">
                      <div class="card-body">
                        <div class="table-responsive">
                          <table class="table">
                            <thead class="text-primary">
                              <tr style="font-size: 13px; font-weight: bold;">
                                <th>Order Number</th>
                                <th style="align: left;">Name Customer</th>
                                <th style="align: left;">Sales Agent</th>
                                <th style="align: left;">Status</th>
                              </tr>
                            </thead>
                            <tbody>';
              foreach ($search->result() as $src) {
                $no_order = ($src->number_order == '') ? 'Please update' : $src->number_order;
                switch ($src->approve) {
                  case 'pending':
                    $acc = '<i class="fas fa-circle f-10 mr-2" style="color: #ffeb00fa;"></i>PENDING';
                    break;

                  case 'paid':
                    $acc = '<i class="fas fa-circle text-c-green f-10 mr-2"></i>PAID';
                    break;

                  case 'unpaid':
                    $acc = '<i class="fas fa-circle text-c-red f-10 mr-2"></i>UNPAID';
                    break;

                  case 'press4U':
                    $acc = '<i class="fas fa-circle f-10 mr-2" style="color: #ff98b4;"></i>PRESS4U';
                    break;

                  case 'proses':
                    $acc = '<i class="fas fa-circle f-10 mr-2" style="color: #748892;"></i>PROSES';
                    break;

                  case 'reject':
                    $acc = '<i class="fas fa-circle f-10 mr-2" style="color: #37474f;"></i>REJECT';
                    break;

                  case 'complete':
                    $acc = '<i class="fas fa-circle f-10 mr-2" style="color: #04a9f5;"></i>COMPLETE';
                    break;
                }
          ?>
                <tr style="color: #000 !important; font-size: 13px;">
                  <td><?= $no_order ?></td>
                  <td align="left"><?= strtoupper($src->name_customer) ?></td>
                  <td align="left"><?= strtoupper($src->name_sales) ?></td>
                  <td align="left"><?= $acc ?></td>
                </tr>
          <?php
              }
              echo '</tbody>
                            </table>
                          </div>
                        <font style="color: #000; font-size: 16px;">Show <strong>' . $search->num_rows() . '</strong> result found</font>
                      </div>
                    </div>
                  </div>
                </div>';
            }
          }
          ?>
        </div>
      </div>
    </div>
  </div>
  <footer class="footer">
    <div class="container-fluid">
      <!-- <nav>
        <ul>
          <li>
            <a href="https://www.creative-tim.com">
              Creative Tim
            </a>
          </li>
          <li>
            <a href="http://presentation.creative-tim.com">
              About Us
            </a>
          </li>
          <li>
            <a href="http://blog.creative-tim.com">
              Blog
            </a>
          </li>
        </ul>
      </nav> -->
      <div class="copyright" id="copyright">
        &copy;
        <script>
          document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
        </script> - <strong><?= $setting->name ?></strong>
      </div>
    </div>
  </footer>

  <!--   Core JS Files   -->
  <script src="<?= base_url() ?>assets/landing-page/js/core/jquery.min.js"></script>
  <script src="<?= base_url() ?>assets/landing-page/js/core/popper.min.js"></script>
  <script src="<?= base_url() ?>assets/landing-page/js/core/bootstrap.min.js"></script>
  <!-- <script src="<?= base_url() ?>assets/landing-page/js/plugins/perfect-scrollbar.jquery.min.js"></script> -->

  <!-- Chart JS -->
  <script src="<?= base_url() ?>assets/landing-page/js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="<?= base_url() ?>assets/landing-page/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <!-- <script src="<?= base_url() ?>assets/landing-page/js/now-ui-dashboard.min.js?v=1.3.0" type="text/javascript"></script> -->
  <!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
  <script src="<?= base_url() ?>assets/landing-page/demo/demo.js"></script>

  <!-- <script>
      var w  = 800;
      var h = 700;
      var left = (window.screen.width / 2) - ((w / 2) + 10);
      var top = (window.screen.height / 2) - ((h / 2) + 50);
      setTimeout(function(){ 
        window.open("<?= base_url('register') ?>","","status=no,height=" + h + ",width=" + w + ",resizable=yes,left="+ left + ",top=" + top + ",screenX=" + left + ",screenY=" + top + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no");
        }, 1000);

      function openOrder(){
          window.open("<?= base_url('register') ?>","","status=no,height=" + h + ",width=" + w + ",resizable=yes,left="+ left + ",top=" + top + ",screenX=" + left + ",screenY=" + top + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no");
      }
  </script> -->

</body>

</html>