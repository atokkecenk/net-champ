<!DOCTYPE html>
<html lang="en">

<head>
    <title>Activate Website</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>
        .mt-3em {
            margin-top: 3em;
        }

        .mt-5em {
            margin-top: 5em;
        }

        .f12 {
            font-size: 12px;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="col-md-6 mx-auto">

            <div class="card mt-5em">
                <div class="card-header">
                    <strong>Form Activate</strong>
                </div>
                <div class="card-body">
                    <?php
                    if ($this->session->flashdata('error')) {
                        echo '<div class="col-sm-12 mt-3">
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    ' . $this->session->flashdata('error') . '
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="right: 0.5rem; padding: .75rem .2rem;">
                    <span aria-hidden="true">&times;</span>
                    </button>
					</div>
                    </div>';
                    } elseif ($this->session->flashdata('success')) {
                        echo '<div class="col-sm-12 mt-3">
					<div class="alert alert-success alert-dismissible fade show" role="alert">
                    ' . $this->session->flashdata('success') . '
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="right: 0.5rem; padding: .75rem .2rem;">
                    <span aria-hidden="true">&times;</span>
                    </button>
					</div>
                    </div>';
                    }
                    ?>
                    <!-- <h5 class="card-title">This form is use to activate this website</h5> -->
                    <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                    <p class="card-text text-center">This form is use to activate this website</p>
                    <div class="row">
                        <?= form_open('activate-website', 'method="post" class="form-inline mx-auto"') ?>
                        <div class="form-group mx-sm-3 mb-2">
                            <label for="inputPassword2" class="sr-only">Password</label>
                            <input type="password" class="form-control" name="key" id="inputPassword2" placeholder="Activate Key">
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">Activate</button>
                        <?= form_close() ?>
                    </div>
                </div>
                <span class="text-center">
                    <?= date('Y') . ' &copy; ' . $sett->name ?>
                </span>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</body>

</html>