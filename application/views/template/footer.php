    <!-- Required Js -->
    <script src="<?= base_url() ?>assets/template/js/vendor-all.min.js"></script>
    <script src="<?= base_url() ?>assets/template/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/template/js/pcoded.min.js"></script>
    <!-- <script src="<?= base_url() ?>assets/jquery/jquery-3.5.1.js"></script> -->
    <script src="<?= base_url() ?>assets/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

    <script src="<?= base_url() ?>assets/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url() ?>assets/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?= base_url() ?>assets/sweetalert/sweetalert.min.js"></script>
    <script src="<?= base_url() ?>assets/myscript/myscript.js"></script>
    <script src="<?= base_url() ?>assets/ckeditor/ckeditor.js"></script>

    <script>
        // document.addEventListener("keydown", function(e){
        //     if (e.ctrlKey || e.keyCode == 123) {
        //         e.stopPropagation();
        //         e.preventDefault();
        //     }
        // });

        var lv = document.getElementById('level');
        lv.addEventListener('change', function() {
            var url = '<?= base_url('User/set_by_credentials/') ?>' + this.value;
            $('#show').load(url);
        }, false);

        // var dh = document.getElementById('detailHirarki');
        // dh.addEventListener('click', function(e) {
        //     $('#modalDetailUser').modal('show');
        //     console.log('x');
        //     // location.href = 'view_user_admin?h=' + this.;
        // }, false);

        function detailHirarki(val, identitas) {
            document.querySelector('#headerDetailUser').innerHTML = '"' + identitas + '"';
            $('#modalDetailUser').modal('show');
            $('#showDetailUser').load('<?= base_url('detail-hirarki/') ?>' + val);
        }

        $(document).ready(function() {

            $("#tbl-package").DataTable({});
            $("#tbExcelOrder").DataTable({
                responsive: true,
            });
            // CKEDITOR.replace('textAnnoucement', {
            //     height: 300
            // });


            $("#tbl-view-user-leader").DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                ajax: {
                    url: "<?= base_url('User/get_all_user_leader') ?>",
                    type: "POST",
                },
                columnDefs: [{
                        targets: [0, 7],
                        className: "text-center",
                    },
                    {
                        orderable: false,
                        targets: [7],
                    },
                ],
                language: {
                    zeroRecords: "No Data Found !!",
                },
            });

            $("#tbl-view-zone").DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                ajax: {
                    url: "<?= base_url('Zone/get_all_zone') ?>",
                    type: "POST",
                },
                columnDefs: [{
                        targets: [0, 4],
                        className: "text-center",
                    },
                    {
                        orderable: false,
                        targets: [4],
                    },
                ],
                language: {
                    zeroRecords: "No Data Found !!",
                },
            });

            $("#tbl-view-segment").DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                ajax: {
                    url: "<?= base_url('Segment/get_all_segment') ?>",
                    type: "POST",
                },
                columnDefs: [{
                        targets: [0, 4],
                        className: "text-center",
                    },
                    {
                        orderable: false,
                        targets: [4],
                    },
                ],
                language: {
                    zeroRecords: "No Data Found !!",
                },
            });

            $("#tbl-view-type").DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                ajax: {
                    url: "<?= base_url('Type/get_all_type') ?>",
                    type: "POST",
                },
                columnDefs: [{
                        targets: [0, 4],
                        className: "text-center",
                    },
                    {
                        orderable: false,
                        targets: [4],
                    },
                ],
                language: {
                    zeroRecords: "No Data Found !!",
                },
            });

            $("#tbl-view-cat-product").DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                ajax: {
                    url: "<?= base_url('Product/get_all_product') ?>",
                    type: "POST",
                },
                columnDefs: [{
                        targets: [0, 4],
                        className: "text-center",
                    },
                    {
                        orderable: false,
                        targets: [4],
                    },
                ],
                language: {
                    zeroRecords: "No Data Found !!",
                },
            });

            $("#tbl-view-announcement").DataTable({
                responsive: true,
                autoWidth: false,
                processing: true,
                serverSide: true,
                aaSorting: [
                    [0, "DESC"]
                ],
                ajax: {
                    url: "<?= base_url('Announcement/get_all_announcement') ?>",
                    type: "POST",
                },
                columnDefs: [{
                        targets: [0],
                        className: "text-center",
                    },
                    {
                        orderable: false,
                        targets: [6],
                    },
                ],
                language: {
                    zeroRecords: "No Data Found !!",
                },
            });

        });

        function cekUsername() {
            var user = document.getElementById("username").value;
            if (user !== '') {
                $.ajax({
                    url: "<?= base_url('user/check_username/') ?>" + user,
                    type: "GET",
                    dataType: "JSON",
                    success: function(data) {
                        if (data.status == 0) {
                            $("#usernameCek").html('<small class="form-text" style="color: #04a9f5 !important;">' + data.message + '</small>');
                        } else {
                            $("#usernameCek").html('<small class="form-text text-c-red">' + data.message + '</small>');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alert("Error ajax !!");
                    },
                });
            }
        }


        function uploadReceipt(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $("#uploadReceipt")
                        .attr("src", "../assets/images/setting/check-1.png")
                        .attr("width", 120)
                        .attr("height", 120);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".uploadReceipt").change(function() {
            uploadReceipt(this);
        });

        $("#uploadReceipt")
            .attr("src", "../assets/images/order/noimage.png")
            .attr("width", 120)
            .attr("height", 120);

        $(document).on('click', '.modalPaidUser', function() {
            var id = $(this).data("id");
            var idsha = $(this).data("idsha");
            var img = "<?= base_url('assets/images/order/receipt/') ?>";
            var edit = "<?= base_url('edit_order/') ?>" + idsha;
            $.ajax({
                url: "<?= base_url('order/get_order_where/') ?>" + id,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    var sts = '<i class="fas fa-circle text-c-green f-10 m-r-10"></i>' + data.approve.toUpperCase();
                    $("#paidDetailOrderId").val(data.id);
                    $("#paidDetailOrderNum").val(data.number_order);
                    $("#paidDetailUserNumIc").text(data.number_ic);
                    $("#paidDetailUserAcc").html(data.name_approve_by + '&nbsp;&nbsp;<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;">' + data.acc_level + '</span>');
                    $("#paidDetailUserAccDate").text(data.acc_date);
                    $("#paidDetailUserSts").html(sts);
                    $('#paidDetailUserRcp').attr('src', img + data.image_receipt);
                    $("#editPaidOrderBtn").html('<a href="' + edit + '" class="btn btn-primary"><i class="feather icon-edit"></i>Edit Order</a>');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert("Error, ambil data dari ajax !!");
                },
            });
        });

        // $('#modalPaidUser, #modalUnpaidUser').on('hidden.bs.modal', function(e) {
        //     window.location.reload();
        // });

        $(document).on('click', '.modalUnpaidUser', function() {
            var id = $(this).data("id");
            var idsha = $(this).data("idsha");
            var img = "<?= base_url('assets/images/order/receipt/') ?>";
            var edit = "<?= base_url('edit_order/') ?>" + idsha;
            $.ajax({
                url: "<?= base_url('order/get_order_where/') ?>" + id,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    var sts = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>' + data.approve.toUpperCase();
                    $("#unpaidDetailOrderId").val(data.id);
                    $("#unpaidDetailOrderNum").val(data.number_order);
                    $("#unpaidDetailUserNumIc").text(data.number_ic);
                    $("#unpaidDetailUserAcc").html(data.name_approve_by + '&nbsp;&nbsp;<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;">' + data.acc_level + '</span>');
                    $("#unpaidDetailUserAccDate").text(data.acc_date);
                    $("#unpaidDetailUserSts").html(sts);
                    $("#unpaidDetailUserRmk").text(data.remarks);
                    $("#editUnpaidOrderBtn").html('<a href="' + edit + '" class="btn btn-primary"><i class="feather icon-edit"></i>Edit Order</a>');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert("Error, ambil data dari ajax !!");
                },
            });
        });

        $(document).on('click', '.modalCancelUser', function() {
            var id = $(this).data("id");
            var idsha = $(this).data("idsha");
            var img = "<?= base_url('assets/images/order/receipt/') ?>";
            var edit = "<?= base_url('edit_order/') ?>" + idsha;
            $.ajax({
                url: "<?= base_url('order/get_order_where/') ?>" + id,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    var sts = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>' + data.approve.toUpperCase();
                    $("#cancelDetailOrderId").val(data.id);
                    $("#cancelDetailOrderNum").val(data.number_order);
                    $("#cancelDetailUserNumIc").text(data.number_ic);
                    $("#cancelDetailUserAcc").html(data.name_approve_by + '&nbsp;&nbsp;<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;">' + data.acc_level + '</span>');
                    $("#cancelDetailUserAccDate").text(data.acc_date);
                    $("#cancelDetailUserSts").html(sts);
                    $("#cancelDetailUserRmk").text(data.remarks);
                    $("#editCancelOrderBtn").html('<a href="' + edit + '" class="btn btn-primary"><i class="feather icon-edit"></i>Edit Order</a>');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert("Error, ambil data dari ajax !!");
                },
            });
        });

        $(document).on('click', '.modalRejectUser', function() {
            var id = $(this).data("id");
            var idsha = $(this).data("idsha");
            var img = "<?= base_url('assets/images/order/receipt/') ?>";
            var edit = "<?= base_url('edit_order/') ?>" + idsha;
            $.ajax({
                url: "<?= base_url('order/get_order_where/') ?>" + id,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    var sts = '<i class="fas fa-circle f-10 m-r-10" style="color: #37474f;"></i>' + data.approve.toUpperCase();
                    $("#rejectDetailOrderId").val(data.id);
                    $("#rejectDetailOrderNum").val(data.number_order);
                    $("#rejectDetailUserNumIc").text(data.number_ic);
                    $("#rejectDetailUserAcc").html(data.name_approve_by + '&nbsp;&nbsp;<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;">' + data.acc_level + '</span>');
                    $("#rejectDetailUserAccDate").text(data.acc_date);
                    $("#rejectDetailUserSts").html(sts);
                    $("#rejectDetailUserRmk").text(data.remarks);
                    $("#editRejectOrderBtn").html('<a href="' + edit + '" class="btn btn-primary"><i class="feather icon-edit"></i>Edit Order</a>');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert("Error, ambil data dari ajax !!");
                },
            });
        });

        $(document).on('click', '.modalPressUser', function() {
            var id = $(this).data("id");
            var idsha = $(this).data("idsha");
            var img = "<?= base_url('assets/images/order/receipt/') ?>";
            var edit = "<?= base_url('edit_order/') ?>" + idsha;
            $.ajax({
                url: "<?= base_url('order/get_order_where/') ?>" + id,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    var sts = '<i class="fas fa-circle f-10 m-r-10" style="color: #ff98b4;"></i>' + data.approve.toUpperCase();
                    $("#pressDetailOrderId").val(data.id);
                    $("#pressDetailOrderNum").val(data.number_order);
                    $("#pressDetailUserNumIc").text(data.number_ic);
                    $("#pressDetailUserSubmit").html(data.name_user_input + '&nbsp;&nbsp;<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;">' + data.level_input + '</span>');
                    $("#pressDetailUserDate").text(data.new_date_input);
                    $("#pressDetailUserSts").html(sts);
                    $("#editPressOrderBtn").html('<a href="' + edit + '" class="btn btn-primary"><i class="feather icon-edit"></i>Edit Order</a>');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert("Error, ambil data dari ajax !!");
                },
            });
        });

        $(document).on('click', '.modalProsesUser', function() {
            var id = $(this).data("id");
            var idsha = $(this).data("idsha");
            var img = "<?= base_url('assets/images/order/receipt/') ?>";
            var edit = "<?= base_url('edit_order/') ?>" + idsha;
            $.ajax({
                url: "<?= base_url('order/get_order_where/') ?>" + id,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    var sts = '<i class="fas fa-circle f-10 m-r-10" style="color: #748892;"></i>' + data.approve.toUpperCase();
                    $("#prosesDetailOrderId").val(data.id);
                    $("#prosesDetailOrderNum").val(data.number_order);
                    $("#prosesDetailUserNumIc").text(data.number_ic);
                    $("#prosesDetailUserSubmit").html(data.name_user_input + '&nbsp;&nbsp;<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;">' + data.level_input + '</span>');
                    $("#prosesDetailUserDate").text(data.new_date_input);
                    $("#prosesDetailUserSts").html(sts);
                    $("#editProsesOrderBtn").html('<a href="' + edit + '" class="btn btn-primary"><i class="feather icon-edit"></i>Edit Order</a>');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert("Error, ambil data dari ajax !!");
                },
            });
        });

        $(document).on('click', '.modalPendingUser', function() {
            var id = $(this).data("id");
            var idsha = $(this).data("idsha");
            var img = "<?= base_url('assets/images/order/receipt/') ?>";
            var edit = "<?= base_url('edit_order/') ?>" + idsha;
            $.ajax({
                url: "<?= base_url('order/get_order_where/') ?>" + id,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    var sts = '<i class="fas fa-circle f-10 m-r-10" style="color: #ffeb00fa;"></i>' + data.approve.toUpperCase();
                    $("#pendingDetailOrderId").val(data.id);
                    $("#pendingDetailOrderNum").val(data.number_order);
                    $("#pendingDetailUserNumIc").text(data.number_ic);
                    $("#pendingDetailUserSubmit").html(data.name_user_input + '&nbsp;&nbsp;<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;">' + data.level_input + '</span>');
                    $("#pendingDetailUserDate").text(data.new_date_input);
                    $("#pendingDetailUserSts").html(sts);
                    $("#pendingDetailUserRemarks").html(data.remarks);
                    $("#editPendingOrderBtn").html('<a href="' + edit + '" class="btn btn-primary"><i class="feather icon-edit"></i>Edit Order</a>');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert("Error, ambil data dari ajax !!");
                },
            });
        });

        $(document).on('click', '.modalCompleteUser', function() {
            var id = $(this).data("id");
            var idsha = $(this).data("idsha");
            var img = "<?= base_url('assets/images/order/receipt/') ?>";
            var edit = "<?= base_url('edit_order/') ?>" + idsha;
            $.ajax({
                url: "<?= base_url('order/get_order_where/') ?>" + id,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    var sts = '<i class="fas fa-circle f-10 m-r-10" style="color: #04a9f5;"></i>' + data.approve.toUpperCase();
                    $("#completeDetailOrderId").val(data.id);
                    $("#completeDetailOrderNum").val(data.number_order);
                    $("#completeDetailUserNumIc").text(data.number_ic);
                    $("#completeDetailUserSubmit").html(data.name_user_input + '&nbsp;&nbsp;<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;">' + data.level_input + '</span>');
                    $("#completeDetailUserDate").text(data.new_date_input);
                    $("#completeDetailUserSts").html(sts);
                    $("#editCompleteOrderBtn").html('<a href="' + edit + '" class="btn btn-primary"><i class="feather icon-edit"></i>Edit Order</a>');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert("Error, ambil data dari ajax !!");
                },
            });
        });
    </script>

    </body>

    </html>