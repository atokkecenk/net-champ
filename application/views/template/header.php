<?php
$name = $this->session->userdata('name');
$level = $this->session->userdata('level');
$pwd = $this->session->userdata('password');
$method2 = ['pending', 'proses', 'spora', 'paid', 'unpaid', 'cancel', 'presales', 'ossgform', 'portfull', 'reject', 'complete'];
$setting = $this->mSett->get_setting()->row();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= $setting->title ?></title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?= $setting->meta_description ?>" />
    <meta name="keywords" content="<?= $setting->meta_keyword ?>" />
    <meta name="author" content="https://juwalan.site" />

    <!-- Favicon icon -->
    <link rel="icon" href="<?= base_url() ?>assets/images/setting/<?= $setting->favicon ?>" type="image/x-icon">
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/template/fonts/fontawesome/css/fontawesome-all.min.css">
    <!-- animation css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/template/plugins/animation/css/animate.min.css">
    <!-- vendor css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/template/css/style.css">
    <!-- datepicker -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/datatables/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/myscript/mystyle.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/sweetalert/sweetalert.min.css">
    <!-- Chart Js -->
    <script src="<?= base_url() ?>assets/chart-js/dist/Chart.js"></script>
</head>

<body>
    <!-- [ Pre-loader ] start -->
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
    <!-- [ Pre-loader ] End -->
    <!-- [ navigation menu ] start -->
    <nav class="pcoded-navbar">
        <div class="navbar-wrapper">
            <div class="navbar-brand header-logo">
                <a href="<?= base_url() ?>" class="b-brand">
                    <!-- <div class="b-bg"> -->
                    <!-- <i class="feather icon-trending-up"></i> -->
                    <div class="b-bg" style="background-image: url('<?= base_url() ?>assets/images/setting/<?= $setting->logo ?>') !important; background-repeat: no-repeat; background-size: 35px 35px">
                        <!-- <img src="<?= base_url() ?>assets/images/setting/<?= $setting->logo ?>" alt="img-logo" height="30px" /> -->
                    </div>
                    <span class="b-title"><?= $setting->name ?></span>
                </a>
                <a class="mobile-menu" id="mobile-collapse" href="javascript:"><span></span></a>
            </div>
            <div class="navbar-content scroll-div">
                <ul class="nav pcoded-inner-navbar">
                    <li class="nav-item pcoded-menu-caption">
                        <label>Main Menu</label>
                    </li>
                    <li <?php
                        $method = ['announcement'];
                        if (in_array($uri1, $method)) {
                            echo 'class="nav-item active"';
                        } else {
                            echo 'class="nav-item"';
                        } ?>>
                        <a href="<?= base_url('announcement') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-bell"></i></span><span class="pcoded-mtext">Announcement</span></a>
                    </li>
                    <li <?php
                        $method = ['dashboard', 'search-sales-report', 'search-sales-report-category'];
                        if (in_array($uri1, $method)) {
                            echo 'class="nav-item active"';
                        } else {
                            echo 'class="nav-item"';
                        } ?>>
                        <a href="<?= base_url('dashboard') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
                    </li>
                    <?php
                    $cek_pwd = ($pwd == 'bbf2dead374654cbb32a917afd236656') ? 'no' : 'yes';
                    if ($cek_pwd == 'no') {
                        echo '<li class="nav-item disabled"><a href="#!" class="nav-link"><span class="pcoded-micon"><i class="feather icon-settings"></i></span><span class="pcoded-mtext">No Menu Display</span></a></li>';
                    } else {
                        if ($level == 'admin') {
                            $method = ['view_user_admin', 'add_user', 'edit-user', 'view_id', 'add_id', 'view_zone', 'add_zone', 'view_segment', 'add_segment', 'view_type', 'add_type', 'view_category_product', 'add_category_product', 'view-announcement', 'add-announcement', 'view-package', 'add-package', 'setting', 'setting-landing-page'];
                    ?>

                            <li <?php if (in_array($uri2, $method2)) {
                                    echo 'class="nav-item pcoded-hasmenu active pcoded-trigger"';
                                } else {
                                    echo 'class="nav-item pcoded-hasmenu"';
                                } ?>>
                                <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-package"></i></span><span class="pcoded-mtext">View Order</span></a>
                                <ul class="pcoded-submenu">
                                    <li <?php if ($uri2 == 'pending') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order/pending') ?>" class="">Pending</a></li>
                                    <li <?php if ($uri2 == 'proses') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order/proses') ?>" class="">Proses</a></li>
                                    <li <?php if ($uri2 == 'spora') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order/spora') ?>" class="">Spora</a></li>
                                    <li <?php if ($uri2 == 'paid') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order/paid') ?>" class="">Paid</a></li>
                                    <li <?php if ($uri2 == 'unpaid') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order/unpaid') ?>" class="">Unpaid</a></li>
                                    <li <?php if ($uri2 == 'cancel') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order/cancel') ?>" class="">Cancel</a></li>
                                    <li <?php if ($uri2 == 'presales') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order/presales') ?>" class="">Presales</a></li>
                                    <li <?php if ($uri2 == 'ossgform') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order/ossgform') ?>" class="">Oss/Gform</a></li>
                                    <li <?php if ($uri2 == 'portfull') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order/portfull') ?>" class="">Port Full</a></li>
                                    <li <?php if ($uri2 == 'reject') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order/reject') ?>" class="">Reject</a></li>
                                    <li <?php if ($uri2 == 'complete') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order/complete') ?>" class="">Complete</a></li>
                                </ul>
                            </li>
                            <li class="nav-item pcoded-menu-caption">
                                <label>Setting Menu</label>
                            </li>
                            <li <?php if (in_array($uri1, $method)) {
                                    echo 'class="nav-item pcoded-hasmenu active pcoded-trigger"';
                                } else {
                                    echo 'class="nav-item pcoded-hasmenu"';
                                } ?>>
                                <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-settings"></i></span><span class="pcoded-mtext">Setting</span></a>
                                <ul class="pcoded-submenu">
                                    <li <?php if ($uri1 == 'view_user_admin' || $uri1 == 'add_user' || $uri1 == 'edit-user') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view_user_admin') ?>" class="">User</a></li>
                                    <li <?php if ($uri1 == 'view_id' || $uri1 == 'add_id') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view_id') ?>" class="">ID Code</a></li>
                                    <li <?php if ($uri1 == 'view_zone' || $uri1 == 'add_zone') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view_zone') ?>" class="">Zone</a></li>
                                    <li <?php if ($uri1 == 'view_type' || $uri1 == 'add_type') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view_type') ?>" class="">Type</a></li>
                                    <li <?php if ($uri1 == 'view-package' || $uri1 == 'add_package') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-package') ?>" class="">Package</a></li>
                                    <li <?php if ($uri1 == 'view_segment' || $uri1 == 'add_segment') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view_segment') ?>" class="">Segment</a></li>
                                    <li <?php if ($uri1 == 'view_category_product' || $uri1 == 'add_category_product') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view_category_product') ?>" class="">Category Product</a></li>
                                    <li <?php if ($uri1 == 'view-announcement' || $uri1 == 'add-announcement') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-announcement') ?>" class="">Announcement</a></li>
                                    <li <?php if ($uri1 == 'setting') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('setting') ?>" class="">Website</a></li>
                                    <li <?php if ($uri1 == 'setting-landing-page') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('setting-landing-page') ?>" class="">Landing Page</a></li>
                                </ul>
                            </li>
                            <li class="nav-item pcoded-menu-caption">
                                <label>Create Report</label>
                            </li>
                            <li <?php if ($uri1 == 'report-pdf') {
                                    echo 'class="nav-item pcoded-hasmenu active pcoded-trigger"';
                                } else {
                                    echo 'class="nav-item pcoded-hasmenu"';
                                } ?>>
                                <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Report</span></a>
                                <ul class="pcoded-submenu">
                                    <li <?php if ($uri1 == 'excel-order') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('excel-order') ?>" class="">Excel</a></li>
                                    <li <?php if ($uri1 == 'report-pdf') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('report-pdf') ?>" class="">Pdf</a></li>
                                </ul>
                            </li>
                        <?php
                        } elseif ($level == 'leader') {
                        ?>
                            <li <?php if (in_array($uri2, $method2)) {
                                    echo 'class="nav-item pcoded-hasmenu active pcoded-trigger"';
                                } else {
                                    echo 'class="nav-item pcoded-hasmenu"';
                                } ?>>
                                <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-package"></i></span><span class="pcoded-mtext">View Order</span></a>
                                <ul class="pcoded-submenu">
                                    <li <?php if ($uri2 == 'pending') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-leader/pending') ?>" class="">Pending</a></li>
                                    <li <?php if ($uri2 == 'proses') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-leader/proses') ?>" class="">Proses</a></li>
                                    <li <?php if ($uri2 == 'spora') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-leader/spora') ?>" class="">Spora</a></li>
                                    <li <?php if ($uri2 == 'paid') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-leader/paid') ?>" class="">Paid</a></li>
                                    <li <?php if ($uri2 == 'unpaid') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-leader/unpaid') ?>" class="">Unpaid</a></li>
                                    <li <?php if ($uri2 == 'cancel') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-leader/cancel') ?>" class="">Cancel</a></li>
                                    <li <?php if ($uri2 == 'presales') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-leader/presales') ?>" class="">Presales</a></li>
                                    <li <?php if ($uri2 == 'ossgform') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-leader/ossgform') ?>" class="">Oss/Gform</a></li>
                                    <li <?php if ($uri2 == 'portfull') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-leader/portfull') ?>" class="">Port Full</a></li>
                                    <li <?php if ($uri2 == 'reject') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-leader/reject') ?>" class="">Reject</a></li>
                                    <li <?php if ($uri2 == 'complete') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-leader/complete') ?>" class="">Complete</a></li>
                                </ul>
                            </li>
                            <li <?php if ($uri1 == 'view_user_leader' || $uri1 == 'add_user_leader') {
                                    echo 'class="nav-item active"';
                                } else {
                                    echo 'class="nav-item"';
                                } ?>>
                                <a href="<?= base_url('view_user_leader') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-user"></i></span><span class="pcoded-mtext">User</span></a>
                            </li>
                        <?php
                        } elseif ($level == 'user') {
                            $method = ['input_order', 'view_order_user'];
                        ?>
                            <li <?php if (in_array($uri1, $method)) {
                                    echo 'class="nav-item active"';
                                } else {
                                    echo 'class="nav-item"';
                                } ?>>
                                <a href="<?= base_url('input_order') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-plus-circle"></i></span><span class="pcoded-mtext">Add Order</span></a>
                            </li>
                            <li <?php if (in_array($uri2, $method2)) {
                                    echo 'class="nav-item pcoded-hasmenu active pcoded-trigger"';
                                } else {
                                    echo 'class="nav-item pcoded-hasmenu"';
                                } ?>>
                                <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-package"></i></span><span class="pcoded-mtext">View Order</span></a>
                                <ul class="pcoded-submenu">
                                    <li <?php if ($uri2 == 'pending') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-user/pending') ?>" class="">Pending</a></li>
                                    <li <?php if ($uri2 == 'proses') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-user/proses') ?>" class="">Proses</a></li>
                                    <li <?php if ($uri2 == 'spora') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-user/spora') ?>" class="">Spora</a></li>
                                    <li <?php if ($uri2 == 'paid') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-user/paid') ?>" class="">Paid</a></li>
                                    <li <?php if ($uri2 == 'unpaid') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-user/unpaid') ?>" class="">Unpaid</a></li>
                                    <li <?php if ($uri2 == 'cancel') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-user/cancel') ?>" class="">Cancel</a></li>
                                    <li <?php if ($uri2 == 'presales') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-user/presales') ?>" class="">Presales</a></li>
                                    <li <?php if ($uri2 == 'ossgform') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-user/ossgform') ?>" class="">Oss/Gform</a></li>
                                    <li <?php if ($uri2 == 'portfull') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-user/portfull') ?>" class="">Port Full</a></li>
                                    <li <?php if ($uri2 == 'reject') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-user/reject') ?>" class="">Reject</a></li>
                                    <li <?php if ($uri2 == 'complete') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-order-user/complete') ?>" class="">Complete</a></li>
                                </ul>
                            </li>
                    <?php
                        }
                    }
                    ?>
                    <li class="nav-item pcoded-menu-caption" style="font-weight: 500; font-style: italic;">
                        <label>Version 1.1.2</label>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- [ navigation menu ] end -->

    <!-- [ Header ] start -->
    <header class="navbar pcoded-header navbar-expand-lg navbar-light">
        <div class="m-header">
            <a class="mobile-menu" id="mobile-collapse1" href="javascript:"><span></span></a>
            <a href="<?= base_url() ?>" class="b-brand">
                <!-- <div class="b-bg">
                    <i class="feather icon-trending-up"></i>
                </div> -->
                <div>
                    <img src="<?= base_url() ?>assets/images/setting/<?= $setting->logo ?>" alt="img-logo" height="30px" />
                </div>
                <span class="b-title">Muafakat Technology</span>
            </a>
        </div>
        <a class="mobile-menu" id="mobile-header" href="javascript:">
            <i class="feather icon-more-horizontal"></i>
        </a>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li><a href="javascript:" class="full-screen" onclick="javascript:toggleFullScreen()"><i class="feather icon-maximize"></i></a></li>
                <!-- <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:" data-toggle="dropdown">Dropdown</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="javascript:">Action</a></li>
                        <li><a class="dropdown-item" href="javascript:">Another action</a></li>
                        <li><a class="dropdown-item" href="javascript:">Something else here</a></li>
                    </ul>
                </li> -->
                <li class="nav-item">
                    <div class="main-search">
                        <div class="input-group">
                            <input type="text" id="m-search" class="form-control" placeholder="Search . . .">
                            <a href="javascript:" class="input-group-append search-close">
                                <i class="feather icon-x input-group-text"></i>
                            </a>
                            <span class="input-group-append search-btn btn btn-primary">
                                <i class="feather icon-search input-group-text"></i>
                            </span>
                        </div>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <span class="name-header">
                    Hello <?= ucwords($this->session->userdata('level')) . ', <b>' . $this->session->userdata('name') . '</b> | Date Now : ' . date('d/m/Y') ?>
                </span>
                <li>
                    <div class="dropdown drp-user">
                        <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon feather icon-settings"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-notification">
                            <div class="pro-head">
                                <img src="<?= base_url() ?>assets/template/images/user/avatar-2.jpg" class="img-radius" alt="User-Profile-Image">
                                <?php
                                $hash = $this->session->hash;
                                $name = $this->db->query("SELECT `name` FROM tb_user WHERE `hash` = '$hash'")->row()->name;
                                echo '<span>' . $name . '</span>';
                                ?>
                                <a href="<?= base_url('auth/logout') ?>" class="dud-logout" title="Logout">
                                    <i class="feather icon-log-out"></i>
                                </a>
                            </div>
                            <ul class="pro-body">
                                <?php
                                if ($level == 'admin') {
                                    echo '<li><a href="#" class="dropdown-item" data-toggle="modal" data-target="#modalMaintenance"><i class="feather icon-power"></i> Maintenance Web</a></li>';
                                } else {
                                    echo '<li><a href="#" data-toggle="modal" data-target="#modalEditPwdNoAdmin" class="dropdown-item"><i class="feather icon-edit"></i> Change Password</a></li>';
                                }
                                ?>
                                <li><a href="<?= base_url('auth/logout') ?>" class="dropdown-item"><i class="feather icon-lock"></i> Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </header>
    <!-- [ Header ] end -->


    <!-- Modal Edit Password -->
    <div class="modal fade" id="modalEditPwdNoAdmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Change Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?= form_open('change-password', 'method="post"') ?>
                <input type="hidden" name="id" value="<?= $this->session->id ?>">
                <input type="hidden" name="hash" value="<?= $this->session->hash ?>">
                <div class="modal-body">
                    <div class="alert alert-danger text-center"><i class="feather icon-info"></i> After change password, system automatic close. Please login again with <b>New Password !!</b></div>
                    <div class="form-group">
                        <label>New Password</label>
                        <div class="input-group mb-3">
                            <input type="password" class="form-control" name="pwd" placeholder="New password" id="input3" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-primary" onclick="return show3()" type="button"><span id="t_pwd3">Show</span></button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Confirm New Password</label>
                        <div class="input-group mb-3">
                            <input type="password" class="form-control" name="cf_pwd" placeholder="Confirm new password" id="input4" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-primary" onclick="return show4()" type="button"><span id="t_pwd4">Show</span></button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger my-2" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

    <!-- Modal Maintenance -->
    <div class="modal fade" id="modalMaintenance" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
                </div>
                <div class="modal-body text-center">
                    <?= form_open('off-system', 'method="post"') ?>
                    Are you sure <b>Set Maintenance</b> this website ?
                    <br>
                    <!-- </div>
          <div class="modal-footer"> -->
                    <button type="button" class="btn btn-danger my-2" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-primary">Yes</button>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>