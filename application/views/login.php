﻿<?php
$setting = $this->mSett->get_setting()->row();
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<title>Login - <?= $setting->title ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="icon" href="<?= base_url() ?>assets/images/setting/<?= $setting->favicon ?>" type="image/x-icon">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/vendor/bootstrap/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">

	<!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/vendor/animate/animate.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/vendor/css-hamburgers/hamburgers.min.css"> -->

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/vendor/animsition/css/animsition.min.css">

	<!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/vendor/select2/select2.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/vendor/daterangepicker/daterangepicker.css"> -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/sweetalert/sweetalert.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/login/css/main.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/myscript/mystyle.css">

</head>

<body>
	<?php
	if ($this->session->flashdata('success')) {
		echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
	} elseif ($this->session->flashdata('error')) {
		echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
	}
	?>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(<?= base_url() ?>assets/login/images/bg-02.png);">
					<span class="login100-form-title-1">
						<?= $setting->text_login ?>
					</span>
				</div>
				<div class="text-center" style="margin-top: 15px !important;">
					<img src="<?= base_url() . 'assets/images/setting/' . $setting->logo_login ?>" class="img-responsive" width="<?= $setting->logo_login_size ?>%">
				</div>
				<?= form_open('auth/login', 'class="login100-form validate-form" method="post"') ?>
				<div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
					<span class="label-input100">Username</span>
					<input class="input100" type="text" name="username" placeholder="Enter username">
					<span class="focus-input100"></span>
				</div>
				<div class="wrap-input100 validate-input m-b-18" data-validate="Password is required">
					<span class="label-input100">Password</span>
					<input class="input100" type="password" name="password" placeholder="Enter password">
					<span class="focus-input100"></span>
				</div>
				<span class="text-danger m-b-15 m-t-0" style="font-size: 13px;">* For Default Password <strong>ABC123</strong></span>
				<div class="container-login100-form-btn">
					<button class="login100-form-btn" type="submit"><i class="fa fa-sign-in mr-2" aria-hidden="true"></i>Login</button>
					<a href="<?= base_url() ?>" class="login100-form-btn-2" style="color: #fff;"><i class="fa fa-reply mr-2" aria-hidden="true"></i>Order Status Checkup</a>
					<button class="login100-form-btn-2" type="button" onclick="window.open('https://api.whatsapp.com/send?phone=<?= $setting->no_hp ?>&text=Hello%20Farid...','_blank')"><i class="fa fa-telegram mr-2" aria-hidden="true"></i>Contact Us</button>
				</div>
				<?= form_close() ?>
			</div>
		</div>
	</div>

	<script src="<?= base_url() ?>assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>

	<script src="<?= base_url() ?>assets/login/vendor/animsition/js/animsition.min.js"></script>

	<script src="<?= base_url() ?>assets/login/vendor/bootstrap/js/popper.js"></script>
	<script src="<?= base_url() ?>assets/login/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- <script src="<?= base_url() ?>assets/login/vendor/select2/select2.min.js"></script> -->
	<!-- <script src="<?= base_url() ?>assets/login/vendor/daterangepicker/moment.min.js"></script> -->
	<!-- <script src="<?= base_url() ?>assets/login/vendor/daterangepicker/daterangepicker.js"></script> -->

	<!-- <script src="<?= base_url() ?>assets/login/vendor/countdowntime/countdowntime.js"></script> -->
	<script src="<?= base_url() ?>assets/sweetalert/sweetalert.min.js"></script>
	<script src="<?= base_url() ?>assets/login/js/main.js"></script>
	<script src="<?= base_url() ?>assets/myscript/myscript.js"></script>
	<!-- <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script> -->
	<!-- <script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-23581568-13');
	</script> -->
</body>

</html>