<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class OrderCustomer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        require APPPATH . 'libraries/phpmailer/src/Exception.php';
        require APPPATH . 'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH . 'libraries/phpmailer/src/SMTP.php';
    }

    public function save_order_customer()
    {
        // lat & long
        $myip = $this->getMyIp();
        // $myip = $this->apiLocation('125.166.117.210');
        $decode = json_decode($myip, true);
        $lat = ($decode['type'] == null) ? 0 : $decode['latitude'];
        $lng = ($decode['type'] == null) ? 0 : $decode['longitude'];
        // lat & long

        $nm_cust = $this->input->post('cust_name');
        $ic = $this->input->post('number_ic');
        $passport = $this->input->post('passport');
        $segment = $this->input->post('segment');
        $type = $this->input->post('type');
        $package = $this->input->post('package');
        $sub_package = $this->input->post('sub_package');
        $cat_product = $this->input->post('cat_product');
        $date = $this->input->post('date');
        $ndate = explode('/', $date);
        $new_date = $ndate[2] . '-' . $ndate[1] . '-' . $ndate[0];
        $address = $this->input->post('address');
        $mobile_phone = $this->input->post('mobile_phone');
        $email = $this->input->post('email');
        $del_number = $this->input->post('del_number');
        $signature = $this->input->post('hdnSignature');
        
        $sett = $this->mSett->get_setting()->row();
        $cek = $this->mOrder->cek_ic_number($ic)->row();
        $price = $this->db->get_where('tb_package', array('id_pkg' => $package))->row()->price;
        $segment_name = $this->db->get_where('tb_segment', ['id' => $segment])->row()->name;
        $package_name = $this->db->get_where('tb_package', ['id_pkg' => $package])->row()->package;
        $sub_package_name = $this->db->get_where('tb_sub_package', ['id' => $package])->row();
        $type_name = $this->db->get_where('tb_type_order', ['id' => $type])->row()->value;
        $category_name = $this->db->get_where('tb_category_product', ['id' => $cat_product])->row()->name;
        if (count($cek) > 0) {
            $this->session->set_flashdata('error', 'Oops, duplicate order number !!');
            header('Location:' . $_SERVER['HTTP_REFERER']);
        } else {
            // 1 = consummer
            // 2 = sme
            if ($segment == '1') {
                $config['upload_path']          = 'assets/images/order/';
                $config['allowed_types']        = 'jpeg|jpg|png|pdf';
                $config['max_size']             = 5024;
                $config['remove_space']         = TRUE;
                $config['encrypt_name']         = TRUE;
                $this->load->library('upload', $config);

                $dataInfo = array();
                $files = $_FILES;
                $cpt = count($_FILES['uploadFileCustomer']['name']);
                for ($i = 0; $i < $cpt; $i++) {
                    $_FILES['uploadFileCustomer']['name'] = $files['uploadFileCustomer']['name'][$i];
                    $_FILES['uploadFileCustomer']['type'] = $files['uploadFileCustomer']['type'][$i];
                    $_FILES['uploadFileCustomer']['tmp_name'] = $files['uploadFileCustomer']['tmp_name'][$i];
                    $_FILES['uploadFileCustomer']['error'] = $files['uploadFileCustomer']['error'][$i];
                    $_FILES['uploadFileCustomer']['size'] = $files['uploadFileCustomer']['size'][$i];

                    $this->upload->do_upload('uploadFileCustomer');
                    $dataInfo[] = $this->upload->data();
                }

                $arr_name = array(
                    'img1' => $dataInfo[0]['file_name'],
                    'img2' => $dataInfo[1]['file_name'],
                    'img3' => $dataInfo[2]['file_name']
                );

                $name = array_unique($arr_name);
                $img1 = $name['img1'] ? $name['img1'] : null;
                $img2 = $name['img2'] ? $name['img2'] : null;
                $img3 = $name['img3'] ? $name['img3'] : null;

                $data['name_customer'] = $nm_cust;
                $data['number_ic'] = $ic;
                $data['number_passport'] = $passport;
                $data['segment'] = $segment;
                $data['package'] = $package;
                $data['sub_package'] = $sub_package;
                $data['price'] = $sub_package_name->price;
                $data['type'] = $type;
                $data['category_product'] = $cat_product;
                $data['date'] = $new_date;
                $data['address'] = $address;
                $data['mobile_phone'] = $mobile_phone;
                $data['email'] = $email;
                $data['del_number'] = $del_number;
                $data['image1'] = $img1;
                $data['image2'] = $img2;
                $data['image3'] = $img3;
                $data['approve'] = 'pending';
                $data['date_input'] = date('Y-m-d H:i:s');
                $data['signature'] = $signature;
                $data['latitude'] = $lat;
                $data['longitude'] = $lng;
                // Send email
                $text = 'Segment : ' . $segment_name . '<br>';
                $text .= 'Customer Name : ' . $nm_cust . '<br>';
                $text .= 'IC Number : ' . $ic . '<br>';
                $text .= 'Passport : ' . $passport . '<br>';
                $text .= 'Package : ' . $package_name . '<br>';
                $text .= 'Sub Package : ' . $sub_package_name->name . '<br>';
                $text .= 'Price : ' . $sub_package_name->price . '<br>';
                $text .= 'Type : ' . $type_name . '<br>';
                $text .= 'Category Product : ' . $category_name . '<br>';
                $text .= 'Installation Date : ' . $new_date . '<br>';
                $text .= 'Address : ' . $address . '<br>';
                $text .= 'Mobile Phone : ' . $mobile_phone . '<br>';
                $text .= 'Email : ' . $email . '<br>';
                $text .= 'Del Number : ' . $del_number . '<br>';
                $this->send_email($sett->order_email, $text);
                // Send email
                $ins = $this->db->insert('tb_order', $data);
                if ($ins) {
                    $this->session->set_flashdata('success', 'Yeaa, save data successfully..');
                    redirect('https://web.whatsapp.com/send?text=' . $this->reformat_to_wa($text) . '&phone=' . $sett->order_wa);
                    // header('Location:' . $_SERVER['HTTP_REFERER']);
                } else {
                    $this->session->set_flashdata('error', 'Oops, save data failed !!');
                    header('Location:' . $_SERVER['HTTP_REFERER']);
                }
            } elseif ($segment == '2') {
                $config['upload_path']          = 'assets/images/order/';
                $config['allowed_types']        = 'jpeg|jpg|png|pdf';
                $config['max_size']             = 5024;
                $config['remove_space']         = TRUE;
                $config['encrypt_name']         = TRUE;
                $this->load->library('upload', $config);

                $dataInfo = array();
                $files = $_FILES;
                $cpt = count($_FILES['uploadFileCustomer']['name']);
                for ($i = 0; $i < $cpt; $i++) {
                    $_FILES['uploadFileCustomer']['name'] = $files['uploadFileCustomer']['name'][$i];
                    $_FILES['uploadFileCustomer']['type'] = $files['uploadFileCustomer']['type'][$i];
                    $_FILES['uploadFileCustomer']['tmp_name'] = $files['uploadFileCustomer']['tmp_name'][$i];
                    $_FILES['uploadFileCustomer']['error'] = $files['uploadFileCustomer']['error'][$i];
                    $_FILES['uploadFileCustomer']['size'] = $files['uploadFileCustomer']['size'][$i];

                    $this->upload->do_upload('uploadFileCustomer');
                    $dataInfo[] = $this->upload->data();
                }

                $arr_name = array(
                    'img1' => $dataInfo[0]['file_name'],
                    'img2' => $dataInfo[1]['file_name'],
                    'img3' => $dataInfo[2]['file_name'],
                    'img4' => $dataInfo[3]['file_name'],
                    'img5' => $dataInfo[4]['file_name'],
                    'img6' => $dataInfo[5]['file_name'],
                    'img7' => $dataInfo[6]['file_name'],
                );

                $name = array_unique($arr_name);
                $img1 = $name['img1'] ? $name['img1'] : null;
                $img2 = $name['img2'] ? $name['img2'] : null;
                $img3 = $name['img3'] ? $name['img3'] : null;
                $img4 = $name['img4'] ? $name['img4'] : null;
                $img5 = $name['img5'] ? $name['img5'] : null;
                $img6 = $name['img6'] ? $name['img6'] : null;
                $img7 = $name['img7'] ? $name['img7'] : null;

                $compy_name = $this->input->post('compy_name');
                $compy_regno = $this->input->post('compy_regno');
                $dir_name = $this->input->post('dir_name');
                $dir_ic = $this->input->post('dir_ic');
                $auth_person = $this->input->post('auth_person');
                $auth_ic_number = $this->input->post('auth_ic_number');
                $data['name_customer'] = $nm_cust;
                $data['number_ic'] = $ic;
                $data['number_passport'] = $passport;
                $data['segment'] = $segment;
                $data['package'] = $package;
                $data['sub_package'] = $sub_package;
                $data['price'] = $sub_package_name->price;
                $data['type'] = $type;
                $data['category_product'] = $cat_product;
                $data['company_name'] = $compy_name;
                $data['company_reg_no'] = $compy_regno;
                $data['director_name'] = $dir_name;
                $data['director_ic'] = $dir_ic;
                $data['authorize_person'] = $auth_person;
                $data['authorize_ic_number'] = $auth_ic_number;
                $data['date'] = $new_date;
                $data['address'] = $address;
                $data['mobile_phone'] = $mobile_phone;
                $data['email'] = $email;
                $data['del_number'] = $del_number;
                $data['image1'] = $img1;
                $data['image2'] = $img2;
                $data['image3'] = $img3;
                $data['image4'] = $img4;
                $data['image5'] = $img5;
                $data['image6'] = $img6;
                $data['image7'] = $img7;
                $data['approve'] = 'pending';
                $data['date_input'] = date('Y-m-d H:i:s');
                $data['signature'] = $signature;
                $data['latitude'] = $lat;
                $data['longitude'] = $lng;
                // Send email
                $text = 'Segment : ' . $segment_name . '<br>';
                $text .= 'Customer Name : ' . $nm_cust . '<br>';
                $text .= 'IC Number : ' . $ic . '<br>';
                $text .= 'Passport : ' . $passport . '<br>';
                $text .= 'Package : ' . $package_name . '<br>';
                $text .= 'Sub Package : ' . $sub_package_name->name . '<br>';
                $text .= 'Price : ' . $sub_package_name->price . '<br>';
                $text .= 'Company Name : ' . $compy_name . '<br>';
                $text .= 'Company Reg No : ' . $compy_regno . '<br>';
                $text .= 'Director Name : ' . $dir_name . '<br>';
                $text .= 'Director IC : ' . $dir_ic . '<br>';
                $text .= 'Authorisation Person : ' . $auth_person . '<br>';
                $text .= 'IC Number : ' . $auth_ic_number . '<br>';
                $text .= 'Type : ' . $type_name . '<br>';
                $text .= 'Category Product : ' . $category_name . '<br>';
                $text .= 'Installation Date : ' . $new_date . '<br>';
                $text .= 'Address : ' . $address . '<br>';
                $text .= 'Mobile Phone : ' . $mobile_phone . '<br>';
                $text .= 'Email : ' . $email . '<br>';
                $text .= 'Del Number : ' . $del_number . '<br>';
                $this->send_email($sett->order_email, $text);
                // Send email
                $ins = $this->db->insert('tb_order', $data);
                if ($ins) {
                    $this->session->set_flashdata('success', 'Yeaa, save data successfully..');
                    redirect('https://web.whatsapp.com/send?text=' . $this->reformat_to_wa($text) . '&phone=' . $sett->order_wa);
                    // header('Location:' . $_SERVER['HTTP_REFERER']);
                } else {
                    $this->session->set_flashdata('error', 'Oops, save data failed !!');
                    header('Location:' . $_SERVER['HTTP_REFERER']);
                }
            }
        }
    }

    function reformat_to_wa($string)
    {
        $replace = str_replace('<br>', '%0A', $string);
        return $replace;
    }

    function test()
    {
        // $myip = $this->getMyIp();
        $myip = $this->apiLocation('125.166.117.210');
        // $coba = $this->gelocation($myip);
        $decode = json_decode($myip, true);
        $typ = ($decode['type'] == null || $decode['type'] == '') ? 'false' : 'success';
        $lat = ($decode['type'] == null) ? 0 : $decode['latitude'];
        $lng = ($decode['type'] == null) ? 0 : $decode['longitude'];
        $arr = array(
            'msg' => $typ,
            'city' => $decode['city'],
            'lat' => $lat,
            'lng' => $lng
        );
        echo json_encode($arr);
    }

    function getMyIp()
    {
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }
        return $ip;
    }

    function apiLocation($addr)
    {
        $json = file_get_contents('http://api.ipstack.com/' . $addr . '?access_key=36e1a5496ae96acd3d9f05fbe3427788');
        return $json;
    }

    public function send_email($email, $txt_email)
    {
        // PHPMailer object
        $response = false;
        $mail = new PHPMailer();
        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = 'leona.sg.domainesia.com'; //sesuaikan sesuai nama domain hosting/server yang digunakan
        $mail->SMTPAuth = true;
        $mail->Username = 'referral@internet-unifi.com'; // user email
        $mail->Password = 'F@rid012'; // password email
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = 465;
        $mail->setFrom('referral@internet-unifi.com', 'Muafakat Technology'); // user email
        $mail->addReplyTo('referral@internet-unifi.com', ''); //user email
        // Add a recipient
        $mail->addAddress($email); //email tujuan pengiriman email
        // Email subject
        $mail->Subject = 'FORM ORDER CUSTOMER'; //subject email
        // Set email format to HTML
        $mail->isHTML(true);
        // Email body content
        // $mailContent = "<p>" . $txt_email . "</p>"; // isi email
        $mail->Body = $txt_email;
        $mail->send();

        // Send email
        // if (!$mail->send()) {
        //     echo 'Message could not be sent.';
        //     echo 'Mailer Error: ' . $mail->ErrorInfo;
        // } else {
        //     echo 'Message has been sent';
        // }
    }
}
