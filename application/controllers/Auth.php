<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $cek = $this->mUser->cek_maintenance()->maintenance;
    if ($cek == '1') {
      redirect('maintenance');
    }
  }

  public function landing()
  {
    $cek = $this->mUser->cek_maintenance()->maintenance;
    if ($cek !== '1') {
      $data['setting'] = $this->mSett->get_setting()->row();
      $this->load->view('landing', $data);
    }
  }

  public function search()
  {
    $search = $this->input->post('search');
    $data = array(
      'setting' => $this->mSett->get_setting()->row(),
      'search'  => $this->mOrder->search($search)
    );

    $this->load->view('landing', $data);
  }

  public function index()
  {
    if ($this->session->userdata('authenticated') && $this->session->userdata('muafakat_technology'))
      redirect('dashboard');

    $data['setting'] = $this->mSett->get_setting()->row();
    $this->load->view('login', $data);
  }

  public function form_order()
  {
    $data['zone'] = $this->db->order_by('name', 'ASC')->get_where('tb_zone', array('active' => 'Y'))->result();
    $data['sales'] = $this->db->order_by('level', 'ASC')->get_where('tb_user', array('active' => 'Y'))->result();
    $data['cat'] = $this->db->get_where('tb_category_product', array('active' => 'Y'))->result();
    $data['segmt'] = $this->db->get_where('tb_segment', array('active' => 'Y'))->result();
    $data['type'] = $this->db->get_where('tb_type_order', array('active' => 'Y'))->result();
    $this->load->view('form_order', $data);
  }

  public function login()
  {
    $username = $this->input->post('username');
    $password = md5($this->input->post('password'));

    $user = $this->mUser->get($username);

    if (empty($user)) {
      $this->session->set_flashdata('error', 'Username not found !!');
      redirect('auth');
    } else {
      if ($password == $user->password) {
        $session = array(
          'authenticated' => true,
          'muafakat_technology' => true,
          'username' => $user->username,
          'password' => $user->password,
          'name' => $user->name,
          'level' => $user->level,
          'hash' => $user->hash,
          'hash_leader' => $user->leader,
          'id' => $user->id
        );

        $cek_annc_active = $this->mDash->cek_announcement_active();
        if ($cek_annc_active->num_rows() > 0) {
          foreach ($cek_annc_active->result() as $key) {
            $this->mDash->update_announcement($key->id);
          }
        }

        $this->session->set_flashdata('success', 'Welcome ' . $user->name);
        $this->session->set_userdata($session);
        redirect('announcement');
      } else {
        $this->session->set_flashdata('error', 'Wrong Password !!');
        redirect('auth');
      }
    }
  }

  public function logout()
  {
    $this->session->sess_destroy();
    redirect('auth');
  }

  public function get_sub_package()
  {
      $id = $this->input->post('id', true);
      // $data = $this->db->get_where('tb_sub_package', array('id'=>'9'))->result();
      $data = $this->db->select('a.*, b.package as name_package')
                      ->from('tb_sub_package a')
                      ->join('tb_package b','a.id = b.id_pkg','left')
                      ->where('a.id', $id)
                      ->get();
      $data2 = array(
                'Name' => $data->row()->name_package,
                'Result' => $data->result()
              );
      header('Content-type:application/json;charset=utf-8');
      echo json_encode($data2);
  }
}
