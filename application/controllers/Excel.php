<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Excel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ModelExcel', 'mEx');
    }

    public function ex_order($month, $year)
    {
        $spreadsheet = new Spreadsheet;
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->getStyle('A1:H1')->getFont()->setBold(true);
        $sheet->getStyle('A1:H1')->getAlignment()->setHorizontal('center');

        $sheet->setCellValue('A1', 'NO.');
        $sheet->setCellValue('B1', 'CUSTOMERS NAME');
        $sheet->setCellValue('C1', 'IC NO');
        $sheet->setCellValue('D1', 'CONTACT');
        $sheet->setCellValue('E1', 'EMAIL');
        $sheet->setCellValue('F1', 'ADDRESS');
        $sheet->setCellValue('G1', 'PACKAGE');
        $sheet->setCellValue('H1', 'COMPLETE DATE');

        $get = $this->mEx->order($month, $year);
        $no = 1;
        $x = 2;
        foreach ($get as $row) {
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $row->name_customer);
            $sheet->setCellValue('C' . $x, $row->number_ic);
            $sheet->setCellValue('D' . $x, $row->mobile_phone);
            $sheet->setCellValue('E' . $x, $row->email);
            $sheet->setCellValue('F' . $x, $row->address);
            $sheet->setCellValue('G' . $x, $row->nm_product);
            $sheet->setCellValue('H' . $x, $row->acc_date);
            $x++;
        }
        foreach (range('A', 'H') as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
        }
        $writer = new Xlsx($spreadsheet);
        $date = date('Y-m-d');
        $filename = 'ORDER-COMPLETE-' . $date;

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }
}
