<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');
defined('BASEPATH') or exit('No direct script access allowed');

class Order extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $cek = $this->mUser->cek_maintenance()->maintenance;
        if ($cek == '1') {
            redirect('maintenance');
        }
    }

    function get_order($mode)
    {
        $list = $this->mOrder->get_datatables($mode);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            switch ($item->approve) {
                case 'pending':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalPendingUser" data-toggle="modal" data-target="#modalPendingUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle f-10 m-r-10" style="color: #ffeb00fa;"></i>PENDING';
                    break;

                case 'paid':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalPaidUser" data-toggle="modal" data-target="#modalPaidUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-green f-10 m-r-10"></i>PAID';
                    break;

                case 'unpaid':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalUnpaidUser" data-toggle="modal" data-target="#modalUnpaidUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>UNPAID';
                    break;

                case 'cancel':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalCancelUser" data-toggle="modal" data-target="#modalCancelUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>CANCEL';
                    break;

                case 'presales':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalCancelUser" data-toggle="modal" data-target="#modalCancelUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>PRESALES';
                    break;

                case 'ossgform':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalCancelUser" data-toggle="modal" data-target="#modalCancelUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>OSS/GFORM';
                    break;

                case 'portfull':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalCancelUser" data-toggle="modal" data-target="#modalCancelUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>PORT FULL';
                    break;

                case 'spora':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalPressUser" data-toggle="modal" data-target="#modalPressUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle f-10 m-r-10" style="color: #ff98b4;"></i>SPORA';
                    break;

                case 'proses':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalProsesUser" data-toggle="modal" data-target="#modalProsesUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle f-10 m-r-10" style="color: #748892;"></i>PROSES';
                    break;

                case 'reject':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalRejectUser" data-toggle="modal" data-target="#modalRejectUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle f-10 m-r-10" style="color: #37474f;"></i>REJECT';
                    break;

                case 'complete':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalCompleteUser" data-toggle="modal" data-target="#modalCompleteUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle f-10 m-r-10" style="color: #04a9f5;"></i>COMPLETE';
                    break;
            }

            $btn_view_detail_all = '<a href="' . base_url('view-detail-order/' . sha1($item->id)) . '" class="btn btn-danger btn-view-detail-user" target="_blank" title="Click to View Detail"><i class="feather icon-eye" style="margin: 0 !important;"></i></a>';
            $no_ic = ($item->number_order !== '') ? $item->number_order : '<font style="font-size: 12px;">Please Update</font>';

            $no++;
            $row = array();
            $row[] = $no . '.';
            $row[] = $btn_view_detail_all . '&nbsp;' . $btn_view_detail . '&nbsp;&nbsp;' . $no_ic;
            // $row[] = $no_ic;
            $row[] = '<b>' . $item->number_ic . '</b>';
            $row[] = strtoupper($item->name_customer);
            $row[] = strtoupper($item->name_sales);
            $row[] = 'RM ' . $item->price;
            $row[] = $item->new_date;
            $row[] = $acc;
            $row[] = '<a href="' . base_url('delete-order/' . $item->id) . '" class="btn btn-danger btn-delete-modif" title="Delete"><i class="feather icon-trash" style="margin: 0 !important;"></i></a>';
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mOrder->count_all(),
            "recordsFiltered" => $this->mOrder->count_filtered($mode),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function get_order_by_leader($mode)
    {
        $list = $this->mOrder->get_datatables_leader($mode);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            switch ($item->approve) {
                case 'pending':
                    $btn_view_detail = '<button type="button" class="btn btn-primary btn-view-detail-user modalPendingUser" data-toggle="modal" data-target="#modalPendingUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle f-10 m-r-10" style="color: #ffeb00fa;"></i>PENDING';
                    break;

                case 'paid':
                    $btn_view_detail = '<button type="button" class="btn btn-primary btn-view-detail-user modalPaidUser" data-toggle="modal" data-target="#modalPaidUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-green f-10 m-r-10"></i>PAID';
                    break;

                case 'unpaid':
                    $btn_view_detail = '<button type="button" class="btn btn-primary btn-view-detail-user m-r-5 modalUnpaidUser" data-toggle="modal" data-target="#modalUnpaidUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>UNPAID';
                    break;

                case 'cancel':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalCancelUser" data-toggle="modal" data-target="#modalCancelUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>CANCEL';
                    break;

                case 'presales':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalCancelUser" data-toggle="modal" data-target="#modalCancelUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>PRESALES';
                    break;

                case 'ossgform':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalCancelUser" data-toggle="modal" data-target="#modalCancelUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>OSS/GFORM';
                    break;

                case 'portfull':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalCancelUser" data-toggle="modal" data-target="#modalCancelUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>PORT FULL';
                    break;

                case 'spora':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalPressUser" data-toggle="modal" data-target="#modalPressUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle f-10 m-r-10" style="color: #ff98b4;"></i>SPORA';
                    break;

                case 'proses':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalProsesUser" data-toggle="modal" data-target="#modalProsesUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle f-10 m-r-10" style="color: #748892;"></i>PROSES';
                    break;

                case 'reject':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalRejectUser" data-toggle="modal" data-target="#modalRejectUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle f-10 m-r-10" style="color: #37474f;"></i>REJECT';
                    break;

                case 'complete':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalCompleteUser" data-toggle="modal" data-target="#modalCompleteUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle f-10 m-r-10" style="color: #04a9f5;"></i>COMPLETE';
                    break;
            }

            $btn_view_detail_all = '<a href="' . base_url('view-detail-order/' . sha1($item->id)) . '" class="btn btn-danger btn-view-detail-user" target="_blank" title="Click to View Detail"><i class="feather icon-eye" style="margin: 0 !important;"></i></a>';
            $no_ic = ($item->number_order !== '') ? $item->number_order : '<font style="font-size: 12px;">Please Update</font>';

            $no++;
            $row = array();
            $row[] = $no . '.';
            $row[] = $btn_view_detail_all . '&nbsp;' . $btn_view_detail . '&nbsp;&nbsp;' . $no_ic;
            $row[] = '<b>' . $item->number_ic . '</b>';
            $row[] = strtoupper($item->name_customer);
            $row[] = strtoupper($item->name_sales);
            $row[] = strtoupper($item->name_zone);
            // $row[] = $item->segment;
            $row[] = strtoupper($item->new_type);
            $row[] = 'RM ' . $item->price;
            $row[] = $item->new_date;
            $row[] = $acc;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mOrder->count_all_leader($mode),
            "recordsFiltered" => $this->mOrder->count_filtered_leader($mode),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function get_order_by_user($mode)
    {
        $list = $this->mOrder->get_datatables_user($mode);
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            switch ($item->approve) {
                case 'pending':
                    $btn_view_detail = '<button type="button" class="btn btn-primary btn-view-detail-user modalPendingUser" data-toggle="modal" data-target="#modalPendingUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle f-10 m-r-10" style="color: #ffeb00fa;"></i>PENDING';
                    break;

                case 'paid':
                    $btn_view_detail = '<button type="button" class="btn btn-primary btn-view-detail-user modalPaidUser" data-toggle="modal" data-target="#modalPaidUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-green f-10 m-r-10"></i>PAID';
                    break;

                case 'unpaid':
                    $btn_view_detail = '<button type="button" class="btn btn-primary btn-view-detail-user modalUnpaidUser" data-toggle="modal" data-target="#modalUnpaidUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>UNPAID';
                    break;

                case 'cancel':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalCancelUser" data-toggle="modal" data-target="#modalCancelUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>CANCEL';
                    break;

                case 'presales':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalCancelUser" data-toggle="modal" data-target="#modalCancelUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>PRESALES';
                    break;

                case 'ossgform':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalCancelUser" data-toggle="modal" data-target="#modalCancelUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>OSS/GFORM';
                    break;

                case 'portfull':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalCancelUser" data-toggle="modal" data-target="#modalCancelUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle text-c-red f-10 m-r-10"></i>PORT FULL';
                    break;

                case 'spora':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalPressUser" data-toggle="modal" data-target="#modalPressUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle f-10 m-r-10" style="color: #ff98b4;"></i>SPORA';
                    break;

                case 'proses':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalProsesUser" data-toggle="modal" data-target="#modalProsesUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle f-10 m-r-10" style="color: #748892;"></i>PROSES';
                    break;

                case 'reject':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalRejectUser" data-toggle="modal" data-target="#modalRejectUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle f-10 m-r-10" style="color: #37474f;"></i>REJECT';
                    break;

                case 'complete':
                    $btn_view_detail = '<button type="button" class="btn btn-info btn-view-detail-user modalCompleteUser" data-toggle="modal" data-target="#modalCompleteUser" data-id="' . $item->id . '" data-idsha="' . sha1($item->id) . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';
                    $acc = '<i class="fas fa-circle f-10 m-r-10" style="color: #04a9f5;"></i>COMPLETE';
                    break;
            }

            $btn_view_detail_all = '<a href="' . base_url('view-detail-order/' . sha1($item->id)) . '" class="btn btn-danger btn-view-detail-user" target="_blank" title="Click to View Detail"><i class="feather icon-eye" style="margin: 0 !important;"></i></a>';
            $no_ic = ($item->number_order !== '') ? $item->number_order : '<font style="font-size: 12px;">Please Update</font>';

            $no++;
            $row = array();
            $row[] = $no . '.';
            // $row[] = $btn_view_detail_all . '&nbsp;' . $btn_view_detail . '&nbsp;&nbsp;' . $no_ic;
            $row[] = $no_ic;
            $row[] = '<b>' . $item->number_ic . '</b>';
            $row[] = strtoupper($item->name_customer);
            $row[] = strtoupper($item->name_zone);
            // $row[] = $item->segment;
            $row[] = strtoupper($item->new_type);
            $row[] = 'RM ' . $item->price;
            $row[] = $item->new_date;
            $row[] = $acc;
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mOrder->count_all_user($mode),
            "recordsFiltered" => $this->mOrder->count_filtered_user($mode),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    public function index()
    {
        $session_id = $this->session->userdata('id');
        $data = array(
            'uri1'  => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
            'level' => $this->session->userdata('level'),
            'zone'  => $this->mOrder->get_zone()->result(),
            'segment'  => $this->mOrder->get_segment()->result(),
            'sales' => $this->mOrder->get_sales()->result(),
            'sales_ldr'   => $this->mOrder->get_sales('leader', $session_id)->result(),
            'cat_product' => $this->mOrder->get_cat_product()->result(),
            'type' => $this->mOrder->get_type()->result()
        );

        $this->render_page('menu/user/order', $data);
    }

    public function get_order_where($id = null)
    {
        $data = $this->mOrder->get_order_where($id)->row();
        echo json_encode($data);
    }

    public function view_order($mode)
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
            'mode' => $mode
        );

        $this->render_page('menu/admin/view/view_order', $data);
    }

    public function view_order_leader($mode)
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
            'mode' => $mode
        );

        $this->render_page('menu/leader/view_order_leader', $data);
    }

    public function view_order_user($mode)
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
            'mode' => $mode
        );

        $this->render_page('menu/user/view_order_user', $data);
    }

    public function view_detail_order($id)
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
            'order' => $this->mOrder->get_order_where_sha($id)->row(),
        );

        $this->render_page('menu/admin/view/view_detail_order', $data);
    }

    public function edit_order($id)
    {
        $session_id = $this->session->userdata('id');
        $data = array(
            'uri1'  => $this->uri->segment(1),
            'uri2'  => $this->uri->segment(2),
            'level' => $this->session->userdata('level'),
            'order' => $this->mOrder->get_order_where_sha($id)->result(),
            'zone'  => $this->mOrder->get_zone()->result(),
            'segment'  => $this->mOrder->get_segment()->result(),
            'sales' => $this->mOrder->get_sales()->result(),
            'sales_ldr'   => $this->mOrder->get_sales('leader', $session_id)->result(),
            'cat_product' => $this->mOrder->get_cat_product()->result(),
            'type' => $this->mOrder->get_type()->result()
        );

        $this->render_page('menu/user/edit_order', $data);
    }

    public function save_order()
    {
        $level = $this->session->userdata('level');
        $no_order = $this->input->post('order_num');
        $cek_no_order = $this->mOrder->cek_oder_number($no_order);

        $no_ic = $this->input->post('ic_num');
        $p1 = substr($no_ic, 0, 6);
        $p2 = substr($no_ic, 6, 2);
        $p3 = substr($no_ic, 8, 4);
        $new_noic = $p1 . '-' . $p2 . '-' . $p3;
        $cek_no_ic = $this->mOrder->cek_ic_number($new_noic);

        // if ($cek_no_order->num_rows() > 0) {
        //     $this->session->set_flashdata('error', 'Oops, duplicate order number !!');
        //     redirect('input_order');
        // }

        // if ($cek_no_ic->num_rows() > 0) {
        //     $this->session->set_flashdata('error', 'Oops, duplicate IC number !!');
        //     redirect('input_order');
        // }
        if ($no_order !== '') {
            if ($cek_no_order->num_rows() > 0) {
                $this->session->set_flashdata('error', 'Oops, duplicate order number !!');
                redirect('input_order');
            } else {
                $config['upload_path']          = 'assets/images/order/';
                $config['allowed_types']        = 'jpeg|jpg|png|pdf';
                $config['max_size']             = 5024;
                $config['max_width']            = 6000;
                $config['max_height']           = 6000;
                $config['remove_space']         = TRUE;
                $config['encrypt_name']         = TRUE;
                $this->load->library('upload', $config);

                $dataInfo = array();
                $files = $_FILES;
                $cpt = count($_FILES['uploadImg']['name']);
                for ($i = 0; $i < $cpt; $i++) {
                    $_FILES['uploadImg']['name'] = $files['uploadImg']['name'][$i];
                    $_FILES['uploadImg']['type'] = $files['uploadImg']['type'][$i];
                    $_FILES['uploadImg']['tmp_name'] = $files['uploadImg']['tmp_name'][$i];
                    $_FILES['uploadImg']['error'] = $files['uploadImg']['error'][$i];
                    $_FILES['uploadImg']['size'] = $files['uploadImg']['size'][$i];

                    $this->upload->do_upload('uploadImg');
                    $dataInfo[] = $this->upload->data();
                }

                $arr_name = array(
                    'img1' => $dataInfo[0]['file_name'],
                    'img2' => $dataInfo[1]['file_name'],
                    'img3' => $dataInfo[2]['file_name'],
                    'img4' => $dataInfo[3]['file_name'],
                    'img5' => $dataInfo[4]['file_name']
                );

                $name = array_unique($arr_name);
                $img1 = $name['img1'] ? $name['img1'] : null;
                $img2 = $name['img2'] ? $name['img2'] : null;
                $img3 = $name['img3'] ? $name['img3'] : null;
                $img4 = $name['img4'] ? $name['img4'] : null;
                $img5 = $name['img5'] ? $name['img5'] : null;

                $this->mOrder->save($img1, $img2, $img3, $img4, $img5);

                $order_num = $this->input->post('order_num');
                $history = array(
                    'note_1' => 'Add Order',
                    'status' => 'add',
                    'id' => null,
                    'order_number' => $order_num,
                    'user_input' => $this->session->hash,
                    'date_input' => date("Y-m-d H:i:s")
                );
                $this->mOrder->save_history($history);

                $this->session->set_flashdata('success', 'Yeaa, save data successfully..');
                header('Location:' . $_SERVER['HTTP_REFERER']);
                // if ($level == 'admin') {
                //     redirect('view_order');
                // } elseif ($level == 'leader') {
                //     redirect('view_order_leader');
                // } elseif ($level == 'user') {
                //     redirect('view_order_user');
                // }
            }
        } else {
            $config['upload_path']          = 'assets/images/order/';
            $config['allowed_types']        = 'jpeg|jpg|png|pdf';
            $config['max_size']             = 5024;
            $config['max_width']            = 6000;
            $config['max_height']           = 6000;
            $config['remove_space']         = TRUE;
            $config['encrypt_name']         = TRUE;
            $this->load->library('upload', $config);

            $dataInfo = array();
            $files = $_FILES;
            $cpt = count($_FILES['uploadImg']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['uploadImg']['name'] = $files['uploadImg']['name'][$i];
                $_FILES['uploadImg']['type'] = $files['uploadImg']['type'][$i];
                $_FILES['uploadImg']['tmp_name'] = $files['uploadImg']['tmp_name'][$i];
                $_FILES['uploadImg']['error'] = $files['uploadImg']['error'][$i];
                $_FILES['uploadImg']['size'] = $files['uploadImg']['size'][$i];

                $this->upload->do_upload('uploadImg');
                $dataInfo[] = $this->upload->data();
            }

            $arr_name = array(
                'img1' => $dataInfo[0]['file_name'],
                'img2' => $dataInfo[1]['file_name'],
                'img3' => $dataInfo[2]['file_name'],
                'img4' => $dataInfo[3]['file_name'],
                'img5' => $dataInfo[4]['file_name']
            );

            $name = array_unique($arr_name);
            $img1 = $name['img1'] ? $name['img1'] : null;
            $img2 = $name['img2'] ? $name['img2'] : null;
            $img3 = $name['img3'] ? $name['img3'] : null;
            $img4 = $name['img4'] ? $name['img4'] : null;
            $img5 = $name['img5'] ? $name['img5'] : null;

            $this->mOrder->save($img1, $img2, $img3, $img4, $img5);

            $order_num = $this->input->post('order_num');
            $history = array(
                'note_1' => 'Add Order',
                'status' => 'add',
                'id' => null,
                'order_number' => $order_num,
                'user_input' => $this->session->hash,
                'date_input' => date("Y-m-d H:i:s")
            );
            $this->mOrder->save_history($history);

            $this->session->set_flashdata('success', 'Yeaa, save data successfully..');
            header('Location:' . $_SERVER['HTTP_REFERER']);
            // if ($level == 'admin') {
            //     redirect('view_order');
            // } elseif ($level == 'leader') {
            //     redirect('view_order_leader');
            // } elseif ($level == 'user') {
            //     redirect('view_order_user');
            // }
        }
    }

    public function upload_file()
    {
        $config['upload_path']          = 'assets/images/order/';
        $config['allowed_types']        = 'jpeg|jpg|png|pdf';
        $config['max_size']             = 5024;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        return $config;
    }

    public function save_order_customer()
    {
        $nm_cust = $this->input->post('cust_name');
        $ic = $this->input->post('number_ic');
        $passport = $this->input->post('passport');
        $segment = $this->input->post('segment');
        $type = $this->input->post('type');
        $cat_product = $this->input->post('cat_product');
        $date = $this->input->post('date');
        $ndate = explode('/', $date);
        $new_date = $ndate[2] . '-' . $ndate[1] . '-' . $ndate[0];
        $address = $this->input->post('address');
        $mobile_phone = $this->input->post('mobile_phone');
        $email = $this->input->post('email');
        $del_number = $this->input->post('del_number');
        // 1 = consummer
        // 2 = sme
        if ($segment == '1') {
            $this->load->library('upload', $this->upload_file);
            $dataInfo = array();
            $files = $_FILES;
            $cpt = count($_FILES['uploadFileCustomer']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['uploadFileCustomer']['name'] = $files['uploadFileCustomer']['name'][$i];
                $_FILES['uploadFileCustomer']['type'] = $files['uploadFileCustomer']['type'][$i];
                $_FILES['uploadFileCustomer']['tmp_name'] = $files['uploadFileCustomer']['tmp_name'][$i];
                $_FILES['uploadFileCustomer']['error'] = $files['uploadFileCustomer']['error'][$i];
                $_FILES['uploadFileCustomer']['size'] = $files['uploadFileCustomer']['size'][$i];

                $this->upload->do_upload('uploadFileCustomer');
                $dataInfo[] = $this->upload->data();
            }

            $arr_name = array(
                'img1' => $dataInfo[0]['file_name'],
                'img2' => $dataInfo[1]['file_name'],
                'img3' => $dataInfo[2]['file_name']
            );

            $name = array_unique($arr_name);
            $img1 = $name['img1'] ? $name['img1'] : null;
            $img2 = $name['img2'] ? $name['img2'] : null;
            $img3 = $name['img3'] ? $name['img3'] : null;

            $data['name_customer'] = $nm_cust;
            $data['number_ic'] = $ic;
            $data['number_passport'] = $passport;
            $data['segment'] = $segment;
            $data['package'] = $package;
            $data['type'] = $type;
            $data['category_product'] = $cat_product;
            $data['date'] = $new_date;
            $data['address'] = $address;
            $data['mobile_phone'] = $mobile_phone;
            $data['email'] = $email;
            $data['del_number'] = $del_number;
            $data['image1'] = $img1;
            $data['image2'] = $img2;
            $data['image3'] = $img3;
            $data['approve'] = 'pending';
            $data['user_input'] = $this->session->hash;
            $data['date_input'] = date('Y-m-d H:i:s');
            $this->db->insert('tb_order', $data);
            $this->session->set_flashdata('success', 'Yeaa, save data successfully..');
        } else {
        }
    }

    public function save_edit_order()
    {
        $this->mOrder->save_edit();
        $this->session->set_flashdata('success', 'Yeaa, save edit data successfully..');
        if ($this->session->userdata('level') == 'admin') {
            // redirect('view_order');
            header('Location:' . $_SERVER['HTTP_REFERER']);
        } elseif ($this->session->userdata('level') == 'leader') {
            // redirect('view_order_leader');
            header('Location:' . $_SERVER['HTTP_REFERER']);
        } else {
            // redirect('view_order_user');
            header('Location:' . $_SERVER['HTTP_REFERER']);
        }

        // $reupload = $this->input->post('reupload');
        // if ($reupload == 'reupload') {
        //     $config['upload_path']          = 'assets/images/order/';
        //     $config['allowed_types']        = 'jpeg|jpg|png|pdf';
        //     $config['max_size']             = 1024;
        //     $config['max_width']            = 2500;
        //     $config['max_height']           = 2500;
        //     $config['remove_space']         = TRUE;
        //     $config['encrypt_name']         = TRUE;
        //     $this->load->library('upload', $config);

        //     $dataInfo = array();
        //     $files = $_FILES;
        //     $cpt = count($_FILES['uploadImg']['name']);
        //     for ($i = 0; $i <b $cpt; $i++) {
        //         $_FILES['uploadImg']['name'] = $files['uploadImg']['name'][$i];
        //         $_FILES['uploadImg']['type'] = $files['uploadImg']['type'][$i];
        //         $_FILES['uploadImg']['tmp_name'] = $files['uploadImg']['tmp_name'][$i];
        //         $_FILES['uploadImg']['error'] = $files['uploadImg']['error'][$i];
        //         $_FILES['uploadImg']['size'] = $files['uploadImg']['size'][$i];

        //         $this->upload->do_upload('uploadImg');
        //         $dataInfo[] = $this->upload->data();
        //     }

        //     $arr_name = array(
        //         'img1' => $dataInfo[0]['file_name'],
        //         'img2' => $dataInfo[1]['file_name'],
        //         'img3' => $dataInfo[2]['file_name'],
        //         'img4' => $dataInfo[3]['file_name'],
        //         'img5' => $dataInfo[4]['file_name']
        //     );

        //     $name = array_unique($arr_name);
        //     $img1 = $name['img1'] ? $name['img1'] : null;
        //     $img2 = $name['img2'] ? $name['img2'] : null;
        //     $img3 = $name['img3'] ? $name['img3'] : null;
        //     $img4 = $name['img4'] ? $name['img4'] : null;
        //     $img5 = $name['img5'] ? $name['img5'] : null;

        //     $this->mOrder->save_edit_reupload($img1, $img2, $img3, $img4, $img5);
        //     $this->session->set_flashdata('success', 'Yeaa, save edit data successfully..');
        //     if ($this->session->userdata('level') == 'admin') {
        //         redirect('view_order');
        //     } elseif ($this->session->userdata('level') == 'leader') {
        //         redirect('view_order_leader');
        //     } else {
        //         redirect('view_order_user');
        //     }
        // } else {
        // }
    }

    public function save_reupload($where)
    {
        $config['upload_path']          = 'assets/images/order/';
        $config['allowed_types']        = 'jpeg|jpg|png|pdf';
        $config['max_size']             = 5024;
        $config['max_width']            = 6000;
        $config['max_height']           = 6000;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);

        $id = $this->input->post('id');
        $new_id = sha1($id);

        switch ($where) {
            case 'file1':
                $this->upload->do_upload('file1');
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $this->mOrder->save_reupload_file('image1', $file_name);
                $this->session->set_flashdata('success', 'Yeaa, reupload file 1 successfully..');
                redirect('edit_order/' . $new_id);
                break;

            case 'file2':
                $this->upload->do_upload('file2');
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $this->mOrder->save_reupload_file('image2', $file_name);
                $this->session->set_flashdata('success', 'Yeaa, reupload file 2 successfully..');
                redirect('edit_order/' . $new_id);
                break;

            case 'file3':
                $this->upload->do_upload('file3');
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $this->mOrder->save_reupload_file('image3', $file_name);
                $this->session->set_flashdata('success', 'Yeaa, reupload file 3 successfully..');
                redirect('edit_order/' . $new_id);
                break;

            case 'file4':
                $this->upload->do_upload('file4');
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $this->mOrder->save_reupload_file('image4', $file_name);
                $this->session->set_flashdata('success', 'Yeaa, reupload file 4 successfully..');
                redirect('edit_order/' . $new_id);
                break;

            case 'file5':
                $this->upload->do_upload('file5');
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $this->mOrder->save_reupload_file('image5', $file_name);
                $this->session->set_flashdata('success', 'Yeaa, reupload file 5 successfully..');
                redirect('edit_order/' . $new_id);
                break;
        }
    }

    public function save_remarks($where)
    {
        $set = $this->db->get_where('tb_setting', array('id' => '1'))->row();
        $server = $_SERVER['SERVER_NAME'] . '/my-project/portal/';
        $level = $this->session->userdata('level');
        $id = $this->input->post('id');
        $order_num = $this->input->post('order_num');

        $config['upload_path']          = 'assets/images/order/receipt/';
        $config['allowed_types']        = 'jpeg|jpg|png|pdf';
        $config['max_size']             = 5024;
        $config['max_width']            = 6000;
        $config['max_height']           = 6000;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);

        $cek_no_order = $this->mOrder->cek_oder_number($order_num);

        $chk = $this->mOrder->cek_edit_leader('no_sha', $id);
        $chk2 = $chk + 1;

        $sendto = $this->mOrder->tujuan_email($id)->row();
        $get_nm = $this->db->get_where('tb_order', ['id' => $id])->row();

        // if ($cek_no_order->num_rows() > 0) {
        //     $this->session->set_flashdata('error', 'Oops, duplicate order number !!');
        //     if ($level == 'admin') {
        //         redirect('view_order');
        //     } elseif ($level == 'leader') {
        //         redirect('view_order_leader');
        //     } else {
        //         redirect('view_order_user');
        //     }
        // } else {
        if ($where == 'pending') {
            $message = '<!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8" />
                        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                        <title>Document</title>
                    </head>
                        <img
                        src="https://contohsite.com/my-project/portal/assets/images/setting/' . $set->logo_login . '"
                        alt="img"
                        width="150"
                        />
                        <p style="font-family: Arial, Helvetica, sans-serif;">
                        Dear <b>' . $sendto->sales . '</b>,<br />We Have received new application from your
                        application Link.<br /><br />Regards,<br/>' . $set->name . '<br/><br/>Url <a href="' . $server . '">' . $server . '</a><br/>Please visit
                        url to view more detail.
                        </p>
                        <div style="background-color: #c2cad2;color: #000;font-family: Arial, Helvetica, sans-serif; padding: 20px;text-align: center;font-size: 16px;font-weight: 700;">&copy; ' . $set->name . '</div>
                    </body>
                    </html>';
            $this->send_email('UPDATE STATUS ORDER', $message);
            $remarks = $this->input->post('remarks');
            // test upload
            $data = array(
                'number_order' => $order_num,
                'approve' => $where,
                'remarks' => $remarks,
                'approve_by' => $this->session->userdata('hash'),
                'approve_date' => date("Y-m-d H:i:s"),
                'edit_leader' => $chk2,
                'edit_leader_hash' => $this->session->userdata('hash'),
                'edit_leader_date' => date("Y-m-d H:i:s")
            );

            $this->db->where('id', $id);
            $this->db->update('tb_order', $data);

            $history = array(
                'note_1' => 'Pending Order',
                'note_2' => $remarks,
                'status' => 'Pending',
                'id' => $id,
                'order_number' => $order_num,
                'user_input' => $this->session->userdata('hash'),
                'date_input' => date("Y-m-d H:i:s")
            );
            $this->mOrder->save_history($history);

            $this->session->set_flashdata('success', 'This order set status Pending..');
            header('Location:' . $_SERVER['HTTP_REFERER']);
        } elseif ($where == 'proses') {
            $message = '<!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8" />
                        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                        <title>Document</title>
                    </head>
                        <img
                        src="https://contohsite.com/my-project/portal/assets/images/setting/' . $set->logo_login . '"
                        alt="img"
                        width="150"
                        />
                        <p style="font-family: Arial, Helvetica, sans-serif;">
                        Dear <b>' . $sendto->sales . '</b>,<br />Please be inform that your Customer <b>' . $get_nm->name_customer . ' / ' . $order_num . '</b> (Order
                        Number) is under processing.<br /><br />Regards,<br/>' . $set->name . '<br/><br/>Url <a href="' . $server . '">' . $server . '</a><br/>Please visit
                        url to view more detail.
                        </p>
                        <div style="background-color: #c2cad2;color: #000;font-family: Arial, Helvetica, sans-serif; padding: 20px;text-align: center;font-size: 16px;font-weight: 700;">&copy; ' . $set->name . '</div>
                    </body>
                    </html>';
            $this->send_email('UPDATE STATUS ORDER', $message);
            $remarks = $this->input->post('remarks');

            $data = array(
                'number_order' => $order_num,
                'approve' => $where,
                'remarks' => $remarks,
                'approve_by' => $this->session->userdata('hash'),
                'approve_date' => date("Y-m-d H:i:s"),
                'edit_leader' => $chk2,
                'edit_leader_hash' => $this->session->userdata('hash'),
                'edit_leader_date' => date("Y-m-d H:i:s")
            );

            $this->db->where('id', $id);
            $this->db->update('tb_order', $data);

            $history = array(
                'note_1' => 'Proses Order',
                'note_2' => $remarks,
                'status' => 'proses',
                'id' => $id,
                'order_number' => $order_num,
                'user_input' => $this->session->userdata('hash'),
                'date_input' => date("Y-m-d H:i:s")
            );
            $this->mOrder->save_history($history);

            $this->session->set_flashdata('success', 'This order set status Proses..');
            header('Location:' . $_SERVER['HTTP_REFERER']);
        } elseif ($where == 'spora') {
            $message = '<!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8" />
                        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                        <title>Document</title>
                    </head>
                        <img
                        src="https://contohsite.com/my-project/portal/assets/images/setting/' . $set->logo_login . '"
                        alt="img"
                        width="150"
                        />
                        <p style="font-family: Arial, Helvetica, sans-serif;">
                        Dear <b>' . $sendto->sales . '</b>,<br />Please Be inform that your Customer <b>' . $get_nm->name_customer . '</b> is under SPORA processing.<br /><br />Regards,<br/>' . $set->name . '<br/><br/>Url <a href="' . $server . '">' . $server . '</a><br/>Please visit
                        url to view more detail.
                        </p>
                        <div style="background-color: #c2cad2;color: #000;font-family: Arial, Helvetica, sans-serif; padding: 20px;text-align: center;font-size: 16px;font-weight: 700;">&copy; ' . $set->name . '</div>
                    </body>
                    </html>';
            $this->send_email('UPDATE STATUS ORDER', $message);
            $remarks = $this->input->post('remarks');

            $data = array(
                'number_order' => $order_num,
                'approve' => $where,
                'remarks' => $remarks,
                'approve_by' => $this->session->userdata('hash'),
                'approve_date' => date("Y-m-d H:i:s"),
                'edit_leader' => $chk2,
                'edit_leader_hash' => $this->session->userdata('hash'),
                'edit_leader_date' => date("Y-m-d H:i:s")
            );

            $this->db->where('id', $id);
            $this->db->update('tb_order', $data);

            $history = array(
                'note_1' => 'Spora Order',
                'note_2' => $remarks,
                'status' => 'spora',
                'id' => $id,
                'order_number' => $order_num,
                'user_input' => $this->session->userdata('hash'),
                'date_input' => date("Y-m-d H:i:s")
            );
            $this->mOrder->save_history($history);

            $this->session->set_flashdata('success', 'This order set status Spora..');
            header('Location:' . $_SERVER['HTTP_REFERER']);
        } elseif ($where == 'paid') {
            $message = '<!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8" />
                        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                        <title>Document</title>
                    </head>
                        <img
                        src="https://contohsite.com/my-project/portal/assets/images/setting/' . $set->logo_login . '"
                        alt="img"
                        width="150"
                        />
                        <p style="font-family: Arial, Helvetica, sans-serif;">
                        Hi <b>' . $sendto->sales . '</b>,<br/><br/>Yeay!!,<br/><br/>Your Customer <b>' . $get_nm->name_customer . ' / ' . $order_num . '</b> (Order Number) has been paid to your bank account <b>' . $sendto->bank_name . '</b> via bank transfer.<br /><br />Regards,<br/>' . $set->name . '<br/><br/>update-order-number Url <a href="' . $server . '">' . $server . '</a><br/>Please visit
                        url to view more detail.
                        </p>
                        <div style="background-color: #c2cad2;color: #000;font-family: Arial, Helvetica, sans-serif; padding: 20px;text-align: center;font-size: 16px;font-weight: 700;">&copy; ' . $set->name . '</div>
                    </body>
                    </html>';
            $this->send_email('UPDATE STATUS ORDER', $message);
            $this->upload->do_upload('receipt');
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];

            $data = array(
                'number_order' => $order_num,
                'approve' => $where,
                'remarks' => null,
                'image_receipt' => $file_name,
                'approve_by' => $this->session->userdata('hash'),
                'approve_date' => date("Y-m-d H:i:s"),
                'edit_leader' => $chk2,
                'edit_leader_hash' => $this->session->userdata('hash'),
                'edit_leader_date' => date("Y-m-d H:i:s")
            );

            $this->db->where('id', $id);
            $this->db->update('tb_order', $data);

            $history = array(
                'note_1' => 'Paid Order',
                'note_2' => $file_name,
                'status' => 'paid',
                'id' => $id,
                'order_number' => $order_num,
                'user_input' => $this->session->userdata('hash'),
                'date_input' => date("Y-m-d H:i:s")
            );
            $this->mOrder->save_history($history);

            $this->session->set_flashdata('success', 'Yeaa, save data successfully..');
            header('Location:' . $_SERVER['HTTP_REFERER']);
        } elseif ($where == 'unpaid' || $where == 'cancel' || $where == 'presales' || $where == 'ossgform' || $where == 'reject' || $where == 'portfull') {
            $remarks = $this->input->post('remarks');
            // template message
            switch ($where) {
                case 'unpaid':
                    $message = '<!DOCTYPE html>
                                <html lang="en">
                                <head>
                                    <meta charset="UTF-8" />
                                    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                                    <title>Document</title>
                                </head>
                                    <img
                                    src="https://contohsite.com/my-project/portal/assets/images/setting/' . $set->logo_login . '"
                                    alt="img"
                                    width="150"
                                    />
                                    <p style="font-family: Arial, Helvetica, sans-serif;">
                                    Hi <b>' . $sendto->sales . '</b>,<br/><br/>Congratulation,<br/><br/>Your Customer <b>' . $get_nm->name_customer . ' / ' . $order_num . '</b> (Order Number) has been unpaid due to <b>' . $remarks . '</b>.<br /><br />Regards,<br/>' . $set->name . '<br/><br/>update-order-number Url <a href="' . $server . '">' . $server . '</a><br/>Please visit
                                    url to view more detail.
                                    </p>
                                    <div style="background-color: #c2cad2;color: #000;font-family: Arial, Helvetica, sans-serif; padding: 20px;text-align: center;font-size: 16px;font-weight: 700;">&copy; ' . $set->name . '</div>
                                </body>
                                </html>';
                    break;
                case 'cancel':
                    $message = '<!DOCTYPE html>
                                    <html lang="en">
                                    <head>
                                        <meta charset="UTF-8" />
                                        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                                        <title>Document</title>
                                    </head>
                                        <img
                                        src="https://contohsite.com/my-project/portal/assets/images/setting/' . $set->logo_login . '"
                                        alt="img"
                                        width="150"
                                        />
                                        <p style="font-family: Arial, Helvetica, sans-serif;">
                                        Hi <b>' . $sendto->sales . '</b>,<br/><br/>Congratulation,<br/><br/>Your Customer <b>' . $get_nm->name_customer . ' / ' . $order_num . '</b> (Order Number) has been cancelled due to <b>' . $remarks . '</b>.<br /><br />Regards,<br/>' . $set->name . '<br/><br/>update-order-number Url <a href="' . $server . '">' . $server . '</a><br/>Please visit
                                        url to view more detail.
                                        </p>
                                        <div style="background-color: #c2cad2;color: #000;font-family: Arial, Helvetica, sans-serif; padding: 20px;text-align: center;font-size: 16px;font-weight: 700;">&copy; ' . $set->name . '</div>
                                    </body>
                                    </html>';
                    break;
                case 'presales':
                    $message = '<!DOCTYPE html>
                                <html lang="en">
                                <head>
                                    <meta charset="UTF-8" />
                                    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                                    <title>Document</title>
                                </head>
                                    <img
                                    src="https://contohsite.com/my-project/portal/assets/images/setting/' . $set->logo_login . '"
                                    alt="img"
                                    width="150"
                                    />
                                    <p style="font-family: Arial, Helvetica, sans-serif;">
                                    Dear <b>' . $sendto->sales . '</b>,<br/>Your Sales Under Customer Name <b>' . $get_nm->name_customer . '</b> And address at <b>' . $get_nm->address . '</b> . Will put under presales due to customer address don`t have infra yet.<br /><br />Regards,<br/>' . $set->name . '<br/><br/>Url <a href="' . $server . '">' . $server . '</a><br/>Please visit
                                    url to view more detail.
                                    </p>
                                    <div style="background-color: #c2cad2;color: #000;font-family: Arial, Helvetica, sans-serif; padding: 20px;text-align: center;font-size: 16px;font-weight: 700;">&copy; ' . $set->name . '</div>
                                </body>
                                </html>';
                    break;
                case 'ossgform':
                    $message = '<!DOCTYPE html>
                                    <html lang="en">
                                    <head>
                                        <meta charset="UTF-8" />
                                        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                                        <title>Document</title>
                                    </head>
                                        <img
                                        src="https://contohsite.com/my-project/portal/assets/images/setting/' . $set->logo_login . '"
                                        alt="img"
                                        width="150"
                                        />
                                        <p style="font-family: Arial, Helvetica, sans-serif;">
                                        Dear <b>' . $sendto->sales . '</b>,<br/>Your Sales Under Customer Name <b>' . $get_nm->name_customer . ' / ' . $order_num . '</b> (Order Number). Under OSS Submission.<br /><br />Regards,<br/>' . $set->name . '<br/><br/>Url <a href="' . $server . '">' . $server . '</a><br/>Please visit
                                        url to view more detail.
                                        </p>
                                        <div style="background-color: #c2cad2;color: #000;font-family: Arial, Helvetica, sans-serif; padding: 20px;text-align: center;font-size: 16px;font-weight: 700;">&copy; ' . $set->name . '</div>
                                    </body>
                                    </html>';
                    break;
                case 'portfull':
                    $message = '<!DOCTYPE html>
                                        <html lang="en">
                                        <head>
                                            <meta charset="UTF-8" />
                                            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                                            <title>Document</title>
                                        </head>
                                            <img
                                            src="https://contohsite.com/my-project/portal/assets/images/setting/' . $set->logo_login . '"
                                            alt="img"
                                            width="150"
                                            />
                                            <p style="font-family: Arial, Helvetica, sans-serif;">
                                            Hi <b>' . $sendto->sales . '</b>,<br/><br/>Congratulation,<br/><br/>Your Customer <b>' . $get_nm->name_customer . ' / ' . $order_num . '</b> (Order Number). Cannot be proceed due to Port Full <b>' . $remarks . '</b>.<br /><br />Regards,<br/>' . $set->name . '<br/><br/>update-order-number Url <a href="' . $server . '">' . $server . '</a><br/>Please visit
                                            url to view more detail.
                                            </p>
                                            <div style="background-color: #c2cad2;color: #000;font-family: Arial, Helvetica, sans-serif; padding: 20px;text-align: center;font-size: 16px;font-weight: 700;">&copy; ' . $set->name . '</div>
                                        </body>
                                        </html>';
                    break;
                case 'reject':
                    $message = '<!DOCTYPE html>
                                            <html lang="en">
                                            <head>
                                                <meta charset="UTF-8" />
                                                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                                                <title>Document</title>
                                            </head>
                                                <img
                                                src="https://contohsite.com/my-project/portal/assets/images/setting/' . $set->logo_login . '"
                                                alt="img"
                                                width="150"
                                                />
                                                <p style="font-family: Arial, Helvetica, sans-serif;">
                                                Hi <b>' . $sendto->sales . '</b>,<br/><br/>Congratulation,<br/><br/>Your Customer <b>' . $get_nm->name_customer . ' / ' . $order_num . '</b> (Order Number) has been rejected due to <b>' . $remarks . '</b>.<br /><br />Regards,<br/>' . $set->name . '<br/><br/>update-order-number Url <a href="' . $server . '">' . $server . '</a><br/>Please visit
                                                url to view more detail.
                                                </p>
                                                <div style="background-color: #c2cad2;color: #000;font-family: Arial, Helvetica, sans-serif; padding: 20px;text-align: center;font-size: 16px;font-weight: 700;">&copy; ' . $set->name . '</div>
                                            </body>
                                            </html>';
                    break;
            }
            $this->send_email('UPDATE STATUS ORDER', $message);

            $data = array(
                'number_order' => $order_num,
                'approve' => $where,
                'remarks' => $remarks,
                'approve_by' => $this->session->userdata('hash'),
                'approve_date' => date("Y-m-d H:i:s"),
                'edit_leader' => $chk2,
                'edit_leader_hash' => $this->session->userdata('hash'),
                'edit_leader_date' => date("Y-m-d H:i:s")
            );

            $this->db->where('id', $id);
            $this->db->update('tb_order', $data);

            $history = array(
                'note_1' => ucwords($where) . ' Order',
                'note_2' => $remarks,
                'status' => $where,
                'id' => $id,
                'order_number' => $order_num,
                'user_input' => $this->session->userdata('hash'),
                'date_input' => date("Y-m-d H:i:s")
            );
            $this->mOrder->save_history($history);

            $this->session->set_flashdata('success', 'Yeaa, save remarks successfully..');
            header('Location:' . $_SERVER['HTTP_REFERER']);
        }
        // }
    }

    public function send_email($subject, $msg)
    {
        $set = $this->db->get_where('tb_setting', array('id' => '1'))->row();
        $from_email = "trial-email@contohsite.com";
        //Load email library
        $this->load->library('email');
        $this->email->from($from_email, strtoupper($set->name));
        $this->email->to('atokkecenk@gmail.com');
        $this->email->cc('faridkucai@yahoo.com');
        $this->email->subject($subject);
        $this->email->message($msg);
        //Send mail
        if ($this->email->send()) {
            print 'sukses';
            // $this->session->set_flashdata("email_sent", "Congragulation Email Send Successfully.");
        } else {
            show_error($this->email->print_debugger());
        }
    }

    public function update_order_number()
    {
        $id = $this->input->post('id');
        $order_num = $this->input->post('order_num');
        $cek_no_order = $this->mOrder->cek_oder_number($order_num);

        if ($cek_no_order->num_rows() > 0) {
            $this->session->set_flashdata('error', 'Oops, duplicate order number !!');
            header('Location:' . $_SERVER['HTTP_REFERER']);
            // if ($this->session->userdata('level') == 'admin') {
            //     redirect('view_order');
            // } elseif ($this->session->userdata('level') == 'leader') {
            //     redirect('view_order_leader');
            // } else {
            //     redirect('view_order_user');
            // }
        } else {
            $data = array(
                'number_order' => $order_num,
                'user_edit' => $this->session->userdata('hash'),
                'date_edit' => date('Y-m-d H:i:s')
            );

            $this->db->where('id', $id);
            $this->db->update('tb_order', $data);
            $this->session->set_flashdata('success', 'Yeaa, save order number successfully..');
            header('Location:' . $_SERVER['HTTP_REFERER']);
            // if ($this->session->userdata('level') == 'admin') {
            //     redirect('view_order');
            // } elseif ($this->session->userdata('level') == 'leader') {
            //     redirect('view_order_leader');
            // } else {
            //     redirect('view_order_user');
            // }
        }
    }

    public function pending($redirect, $id)
    {
        $chk = $this->mOrder->cek_edit_leader('no_sha', $id);
        $chk2 = $chk + 1;
        $this->db->where(array('id' => $id));
        $this->db->update(
            'tb_order',
            array(
                'approve' => 'pending',
                'remarks' => null,
                'approve_by' => $this->session->hash,
                'approve_date' => date("Y-m-d H:i:s"),
                'edit_leader' => $chk2,
                'edit_leader_hash' => $this->session->userdata('hash'),
                'edit_leader_date' => date("Y-m-d H:i:s")
            )
        );

        $odr = $this->mOrder->get_order_where($id)->row();
        $history = array(
            'note_1' => 'Pending Order',
            'status' => 'pending',
            'id' => $id,
            'order_number' => $odr->number_order,
            'user_input' => $this->session->hash,
            'date_input' => date("Y-m-d H:i:s")
        );
        $this->mOrder->save_history($history);

        $this->session->set_flashdata('success', 'This order set status Pending..');
        redirect($redirect);
    }

    public function complete($redirect, $id)
    {
        $set = $this->db->get_where('tb_setting', array('id' => '1'))->row();
        $server = $_SERVER['SERVER_NAME'];
        $chk = $this->mOrder->cek_edit_leader('no_sha', $id);
        $chk2 = $chk + 1;

        $order_num = $this->db->get_where('tb_order', ['id' => $id])->row()->number_order;
        $sendto = $this->mOrder->tujuan_email($id)->row();
        $get_nm = $this->db->get_where('tb_order', ['id' => $id])->row();
        $message = '<!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8" />
                        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                        <title>Document</title>
                    </head>
                        <img
                        src="https://contohsite.com/my-project/portal/assets/images/setting/d230792f383c88db8db535281bf6c9cb.png"
                        alt="img"
                        width="150"
                        />
                        <p style="font-family: Arial, Helvetica, sans-serif;">
                        Hi <b>' . $sendto->sales . '</b>,<br/><br/>Congratulation,<br/><br/>Your Customer <b>' . $get_nm->name_customer . ' / ' . $order_num . '</b> (Order Number) as been completed and we will arrange a payment via bank transfer.<br /><br />Regards,<br/>' . $set->name . '<br/><br/>update-order-number Url <a href="' . $server . '">' . $server . '</a><br/>Please visit
                        url to view more detail.
                        </p>
                        <div style="background-color: #c2cad2;color: #000;font-family: Arial, Helvetica, sans-serif; padding: 20px;text-align: center;font-size: 16px;font-weight: 700;">&copy; ' . $set->name . '</div>
                    </body>
                    </html>';
        $this->send_email('UPDATE STATUS ORDER', $message);
        $this->db->where(array('id' => $id));
        $this->db->update(
            'tb_order',
            array(
                'approve' => 'complete',
                'remarks' => null,
                'approve_by' => $this->session->hash,
                'approve_date' => date("Y-m-d H:i:s"),
                'edit_leader' => $chk2,
                'edit_leader_hash' => $this->session->userdata('hash'),
                'edit_leader_date' => date("Y-m-d H:i:s")
            )
        );

        $odr = $this->mOrder->get_order_where($id)->row();
        $history = array(
            'note_1' => 'Complete Order',
            'status' => 'complete',
            'id' => $id,
            'order_number' => $odr->number_order,
            'user_input' => $this->session->hash,
            'date_input' => date("Y-m-d H:i:s")
        );
        $this->mOrder->save_history($history);

        $this->session->set_flashdata('success', 'This order set status Complete and Send the email to sales agent..');
        // redirect($redirect);
        header('Location:' . $_SERVER['HTTP_REFERER']);
    }

    // Save Order Cust

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tb_order');
        $this->session->set_flashdata('success', 'This order has been deleted..');
        header('Location:' . $_SERVER['HTTP_REFERER']);
    }
}
