<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Zone extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $cek = $this->mUser->cek_maintenance()->maintenance;
        if ($cek == '1') {
            redirect('maintenance');
        }
    }

    function get_all_zone()
    {
        $list = $this->mZone->get_datatables();
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = strtoupper($item->name);
            $row[] = strtoupper($item->name_input) . '&nbsp;&nbsp;<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;">' . strtoupper($item->level_input) . '</span>';
            $row[] = '<i class="fas fa-circle text-c-green f-10 m-r-15"></i>' . $item->date . ', ' . $item->time;;
            $row[] =
                '<button type="button" class="btn btn-danger btn-delete-modif modalDeleteZone" data-toggle="modal" data-target="#modalDeleteZone" data-id="' . sha1($item->id) . '" title="Delete"><i class="feather icon-trash" style="margin: 0 !important;"></i></a>';
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mZone->count_all(),
            "recordsFiltered" => $this->mZone->count_filtered(),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    public function add_zone()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2)
        );

        $this->render_page('menu/admin/add/add_zone', $data);
    }

    public function view_zone()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2)
        );

        $this->render_page('menu/admin/view/view_zone', $data);
    }

    public function save_zone()
    {
        $this->mZone->save_zone();
        $this->session->set_flashdata('success', 'Yeaa, save data zone successfully..');
        redirect('view_zone');
    }
}
