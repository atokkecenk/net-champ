<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Maintenance extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data['sett'] = $this->db->get_where('tb_setting', array('id' => 1))->row();
    $this->load->view('maintenance', $data);
  }

  public function update_maintenance()
  {
    $data['sett'] = $this->db->get_where('tb_setting', array('id' => 1))->row();
    $this->load->view('upd_maintenance', $data);
  }

  public function off_system()
  {
    $this->db->where('id', 1);
    $upd = $this->db->update('tb_setting', array('maintenance' => 1));
    if ($upd) {
      $this->session->sess_destroy();
      redirect(base_url(), 'refresh');
    } else {
      $this->session->set_flashdata('error', 'Oops, missing some values.');
    }
    
  }

  public function activate_website()
  {
    $key = $this->input->post('key');
    $get_key = $this->db->get_where('tb_setting', array('id'=>1))->row()->activate_key;
    if ($key == $get_key) {
      $this->db->where('id', 1);
      $upd = $this->db->update('tb_setting', array('maintenance' => 0));
      if ($upd) {
        $this->session->set_flashdata('success', 'Activate website successfully..<br><a href="'.base_url().'" style="text-decoration: none;">Back To Website</a>');
      } else {
        $this->session->set_flashdata('error', 'Oops, activate website failed !!');
      }
      redirect('activate');
    } else {
      $this->session->set_flashdata('error', 'Oops, check your activate key !!');
      redirect('activate');
    }
  }
}
