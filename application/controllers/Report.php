<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $cek = $this->mUser->cek_maintenance()->maintenance;
        if ($cek == '1') {
            redirect('maintenance');
        }
        $this->load->library('Pdf');
        $this->load->model('ModelExcel', 'mEx');
    }

    // public function contoh()
    // {
    //     $data = array(
    //         'sett' => $this->mSett->get_setting()->row(),
    //         'data' => $this->mRept->get_report_by_sales()
    //     );
    //     $this->render_page('menu/report/contoh', $data);
    // }

    public function pdf_by_sales()
    {
        $sales = $this->input->post('sales');
        $month = $this->input->post('month');
        $year = $this->input->post('year');

        $data['sett'] = $this->mSett->get_setting()->row();
        $data['data'] = $this->mRept->get_report_by_sales($sales, $month, $year);
        $this->render_page('menu/report/contoh', $data);
    }

    public function view_pdf()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
            'sales' => $this->mUser->get_user()
        );
        $this->render_page('menu/report/report_pdf', $data);
    }

    public function excel_order()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
            // 'data' => $this->mEx->order(),
        );
        $this->render_page('menu/report/excel_order', $data);
    }
}
