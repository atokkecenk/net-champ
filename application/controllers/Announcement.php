<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Announcement extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $cek = $this->mUser->cek_maintenance()->maintenance;
        if ($cek == '1') {
            redirect('maintenance');
        }
    }

    function get_all_announcement()
    {
        $list = $this->mAnnc->get_datatables();
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            switch ($item->publish) {
                case 'publish':
                    $view = '<a href="#" class="modalDraftAnnc" data-id="' . sha1($item->id) . '" data-toggle="modal" data-target="#modalDraftAnnc" style="color: #9591a5;" title="Click To Draft"><i class="feather icon-eye m-r-5" style="color: #04a9f5;"></i>Publish</a>';
                    break;

                case 'draft':
                    $view = '<a href="#" class="modalPublishAnnc" data-id="' . sha1($item->id) . '" data-toggle="modal" data-target="#modalPublishAnnc" style="color: #9591a5;" title="Click To Publish"><i class="feather icon-slash m-r-5" style="color: #dc3545;"></i>Draft</a>';
                    break;
            }

            switch ($item->masa) {
                case 'U':
                    $new_masa = 'Unlimited';
                    $date = null;
                    break;

                case 'D':
                    $new_masa = 'By Date';
                    $date = $item->new_start . ' until ' . $item->new_end;
                    break;
            }

            if ($item->foto == '') {
                $image = null;
            } else {
                $image = '<p><img src="' . base_url('assets/images/announcement/' . $item->foto) . '" style="width: 100%;"></p>';
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->subject;
            $row[] = '<div class="alert alert-danger">' . $image . '<p>' . $item->text . '</p></div>';
            $row[] = $view;
            $row[] = $new_masa;
            $row[] = $date;
            $row[] = '<button type="button" class="btn btn-danger btn-delete-modif modalDeleteAnnc" data-toggle="modal" data-target="#modalDeleteAnnc" data-id="' . sha1($item->id) . '" title="Delete"><i class="feather icon-trash" style="margin: 0 !important;"></i></a>';
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mAnnc->count_all(),
            "recordsFiltered" => $this->mAnnc->count_filtered(),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    public function add()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2)
        );

        $this->render_page('menu/admin/add/add_announcement', $data);
    }

    public function view()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2)
        );

        $this->render_page('menu/admin/view/view_announcement', $data);
    }

    public function save()
    {
        $text = $this->input->post('text');
        $config['upload_path']          = 'assets/images/announcement/';
        $config['allowed_types']        = 'jpeg|jpg|png';
        $config['max_size']             = 5024;
        $config['max_width']            = 6000;
        $config['max_height']           = 6000;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {

            // $cek = $this->db->select('*')
            //     ->from('tb_user')
            //     ->where('active', 'Y')
            //     ->get()->result();

            // foreach ($cek as $val) {
            //     $this->db->insert('tb_history', array(
            //         'note_1' => 'Add Announcement',
            //         'note_2' => $text,
            //         'status' => 'N',
            //         'id' => $val->id,
            //         'user_input' => $this->session->hash,
            //         'date_input' => date('Y-m-d H:i:s'),
            //     ));
            // }

            $this->mAnnc->save($file_name = '');
            $this->session->set_flashdata('success', 'Save announcement successfully..');
            redirect('add-announcement');
        } else {
            // $this->upload->do_upload('image');
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];

            // $cek = $this->db->select('*')
            //     ->from('tb_user')
            //     ->where('active', 'Y')
            //     ->get()->result();

            // foreach ($cek as $val) {
            //     $this->db->insert('tb_history', array(
            //         'note_1' => 'Add Announcement With Image',
            //         'note_2' => $text,
            //         'status' => 'N',
            //         'id' => $val->id,
            //         'user_input' => $this->session->hash,
            //         'date_input' => date('Y-m-d H:i:s'),
            //     ));
            // }

            $this->mAnnc->save($file_name);
            $this->session->set_flashdata('success', 'Save announcement successfully..');
            redirect('add-announcement');
        }
    }

    public function save_announcement()
    {
        $this->mAnnc->save();
        $this->session->set_flashdata('success', 'Yeaa, save announcement successfully..');
        redirect('dashboard');
    }

    public function save_announcement_img()
    {
        $img = $this->catch_that_image($this->input->post('uploadAnncImg'));
        $config['upload_path']          = 'assets/images/announcement/';
        $config['allowed_types']        = 'jpeg|jpg|png|ico';
        $config['max_size']             = 5024;
        $config['max_width']            = 6000;
        $config['max_height']           = 6000;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);
        $this->upload->do_upload('uploadAnncImg');
        $upload_data = $this->upload->data();
        $file_name = $upload_data['file_name'];
        $source = $upload_data['full_path'];

        $this->db->insert('tb_announcement_img', array(
            'name_file' => $file_name,
            // 'source' => $source,
            'source' => $img,
            'user_input' => $this->session->hash,
            'date_input' => date('Y-m-d H:i:s')
        ));
        $this->session->set_userdata(array(
            'annc_source' => $source
        ));
        $this->session->set_flashdata('success', 'Yeaa, upload file image successfully..');
        redirect('add-announcement');
    }

    public function update_announcement_status($mode)
    {
        $id = $this->input->post('id');
        $this->db->where(array('sha1(id)' => $id));
        $this->db->update('tb_announcement', array('publish' => $mode));
        $this->session->set_flashdata('success', 'Yeaa, set status announcement successfully..');
        redirect('view-announcement');
    }

    function catch_that_image($post)
    {
        $first_img = NULL;
        $output = preg_match_all('@src="([^"]+)"@', $post, $matches, PREG_SET_ORDER);
        $first_img = $matches[0][0];
        return $first_img;
    }
}
