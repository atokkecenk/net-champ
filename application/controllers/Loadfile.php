<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Loadfile extends CI_Controller
{
	public function consumer_top()
	{
		$this->load->view('load_file/consumer_top');
	}

	public function consumer_upload()
	{
		$this->load->view('load_file/consumer_upload');
	}

	public function sme_top()
	{
		$this->load->view('load_file/sme_top');
	}

	public function sme_upload()
	{
		$this->load->view('load_file/sme_upload');
	}
}