<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Package extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $cek = $this->mUser->cek_maintenance()->maintenance;
        if ($cek == '1') {
            redirect('maintenance');
        }
    }

    function data()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2)
        );
        return $data;
    }

    public function view()
    {
        $data = $this->data();
        $data['package_list'] = $this->mPack->get_data_package();
        $this->render_page('menu/admin/view/view_package', $data);
    }

    public function add()
    {
        $data = $this->data();
        $this->render_page('menu/admin/add/add_package', $data);
    }

    public function save()
    {
        $this->db->insert('tb_package', array(
            'type' => $this->input->post('type'),
            'package' => $this->input->post('package'),
            'price' => $this->input->post('price'),
            'active' => 'Y',
            'user_input' => $this->session->hash,
            'date_input' => date('Y-m-d H:i:s'),
        ));
        $this->session->set_flashdata('success', 'Save data package successfully..');
        redirect('view-package');
    }
    
    public function save_sub()
    {
        $id_pkg = $this->input->post('id_pkg');
        $price = $this->input->post('price');
        $subname = $this->input->post('sub_package');
        foreach ($subname as $key => $value) {
            $this->db->insert('tb_sub_package', array('id' => $id_pkg, 'name' => $subname[$key], 'price' => $price[$key] , 'active' => 'Y'));
        }
        $this->session->set_flashdata('success', 'Save data sub package successfully..');
        redirect('view-package', 'refresh');
    }

    public function delete($params)
    {
        switch ($params) {
            case 'package':
                $field = 'id_pkg';
                $key = $this->input->post('id');
                $tb = 'tb_package';
                $rdr = 'view-package';
                $msg = 'package';
                break;

            case 'del-sub':
                $field = 'id_sub';
                $key = $this->input->post('id');
                $tb = 'tb_sub_package';
                $rdr = 'view-package';
                $msg = 'sub package';
                break;
        }
        $this->db->where($field, $key);
        $this->db->delete($tb);
        $this->session->set_flashdata('success', 'Delete data ' . $msg . ' successfully..');
        redirect($rdr, 'refresh');
    }
}
