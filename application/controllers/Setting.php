<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $cek = $this->mUser->cek_maintenance()->maintenance;
        if ($cek == '1') {
            redirect('maintenance');
        }
    }

    public function setting_website()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
            'setting' => $this->mSett->get_setting()->result()
        );

        $this->render_page('menu/admin/setting_web', $data);
    }

    public function setting_landing()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
            'setting' => $this->mSett->get_setting()->result()
        );

        $this->render_page('menu/admin/setting_landing', $data);
    }

    public function setting_text()
    {
        $title = $this->input->post('title');
        $name = $this->input->post('name');
        $hp = $this->input->post('hp');
        $email = $this->input->post('email');
        $alamat = $this->input->post('alamat');
        $tlp = $this->input->post('tlp');
        $faks = $this->input->post('faks');
        $website = $this->input->post('website');
        $meta_description = $this->input->post('meta_description');
        $meta_keywords = $this->input->post('meta_keywords');
        $hash = $this->session->userdata('hash');

        $data = array(
            'title' => $title,
            'name'  => $name,
            'alamat' => $alamat,
            'telephone' => $tlp,
            'email' => $email,
            'faks' => $faks,
            'no_hp' => $hp,
            'website' => $website,
            'meta_description'  => $meta_description,
            'meta_keyword'      => $meta_keywords,
            'user_edit' => $hash,
            'date_edit' => date('Y-m-d H:i:s')
        );

        $this->db->where(array('id' => 1));
        $this->db->update('tb_setting', $data);
        $this->session->set_flashdata('success', 'Yeaa, save setting website successfully..');
        redirect('setting');
    }

    public function setting_favicon()
    {
        $config['upload_path']          = 'assets/images/setting/';
        $config['allowed_types']        = 'jpeg|jpg|png|ico';
        $config['max_size']             = 512;
        $config['max_width']            = 1500;
        $config['max_height']           = 1500;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);

        $this->upload->do_upload('favicon');
        $upload_data = $this->upload->data();

        //Compress Image
        $config['image_library'] = 'gd2';
        $config['source_image'] = 'assets/images/setting/' . $upload_data['file_name'];
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = '50%';
        $config['width'] = 35;
        $config['height'] = 35;
        $config['new_image'] = 'assets/images/setting/' . $upload_data['file_name'];
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();

        $file_name = $upload_data['file_name'];

        $data = array(
            'favicon' => $file_name,
            'user_edit' => $this->session->userdata('hash'),
            'date_edit' => date("Y-m-d H:i:s")
        );

        $this->db->where('id', 1);
        $this->db->update('tb_setting', $data);

        $this->session->set_flashdata('success', 'Yeaa, save setting website successfully..');
        redirect('setting');
    }

    public function setting_logo()
    {
        $config['upload_path']          = 'assets/images/setting/';
        $config['allowed_types']        = 'jpeg|jpg|png|ico';
        $config['max_size']             = 1024;
        $config['max_width']            = 2000;
        $config['max_height']           = 2000;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);

        $this->upload->do_upload('logo');
        $upload_data = $this->upload->data();

        //Compress Image
        $config['image_library'] = 'gd2';
        $config['source_image'] = 'assets/images/setting/' . $upload_data['file_name'];
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = '50%';
        $config['width'] = 35;
        $config['height'] = 35;
        $config['new_image'] = 'assets/images/setting/' . $upload_data['file_name'];
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();

        $file_name = $upload_data['file_name'];

        $data = array(
            'logo' => $file_name,
            'user_edit' => $this->session->userdata('hash'),
            'date_edit' => date("Y-m-d H:i:s")
        );

        $this->db->where('id', 1);
        $this->db->update('tb_setting', $data);

        $this->session->set_flashdata('success', 'Yeaa, save setting website successfully..');
        redirect('setting');
    }

    public function setting_logo_login()
    {
        $text = $this->input->post('text');
        $size = $this->input->post('size');

        $config['upload_path']          = 'assets/images/setting/';
        $config['allowed_types']        = 'jpeg|jpg|png|ico';
        $config['max_size']             = 5024;
        $config['max_width']            = 6000;
        $config['max_height']           = 6000;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('logo_login')) {
            $data = array(
                'text_login' => $text,
                'logo_login_size' => $size,
                'user_edit' => $this->session->userdata('hash'),
                'date_edit' => date("Y-m-d H:i:s")
            );

            $this->db->where('id', 1);
            $this->db->update('tb_setting', $data);
            $this->session->set_flashdata('success', 'Yeaa, save setting website successfully..');
            redirect('setting');
        } else {
            $this->upload->do_upload('logo_login');
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];

            $data = array(
                'text_login' => $text,
                'logo_login' => $file_name,
                'logo_login_size' => $size,
                'user_edit' => $this->session->userdata('hash'),
                'date_edit' => date("Y-m-d H:i:s")
            );

            $this->db->where('id', 1);
            $this->db->update('tb_setting', $data);
            $this->session->set_flashdata('success', 'Yeaa, save setting website successfully..');
            redirect('setting');
        }
    }

    public function setting_landing_text()
    {
        $title = $this->input->post('title');
        $text = $this->input->post('text');
        $hash = $this->session->userdata('hash');

        $data = array(
            'lp_title' => $title,
            'lp_text'  => $text,
            'user_edit' => $hash,
            'date_edit' => date('Y-m-d H:i:s')
        );

        $this->db->where(array('id' => 1));
        $this->db->update('tb_setting', $data);
        $this->session->set_flashdata('success', 'Yeaa, save setting landing page successfully..');
        redirect('setting-landing-page');
    }

    public function setting_background_landing()
    {
        $config['upload_path']          = 'assets/landing-page/img/';
        $config['allowed_types']        = 'jpeg|jpg|png|ico';
        $config['max_size']             = 5024;
        $config['max_width']            = 6000;
        $config['max_height']           = 6000;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);

        $this->upload->do_upload('background');
        $upload_data = $this->upload->data();
        $file_name = $upload_data['file_name'];

        $data = array(
            'lp_background' => $file_name,
            'user_edit' => $this->session->userdata('hash'),
            'date_edit' => date("Y-m-d H:i:s")
        );

        $this->db->where('id', 1);
        $this->db->update('tb_setting', $data);
        $this->session->set_flashdata('success', 'Yeaa, save background landing page successfully..');
        redirect('setting-landing-page');
    }

    public function setting_receiver_order()
    {
        $no_wa = $this->input->post('order_wa');
        $cek = substr($no_wa, 0, 1);
        if ($cek == '0' || $cek == '+') {
            $this->session->set_flashdata("error", "Oops, Don't use character 0 OR + at the beginning !!");
            redirect('setting', 'refresh');
        } else {
            $this->db->where('id', 1);
            $this->db->update('tb_setting', array(
                'order_email' => $this->input->post('order_email'),
                'order_wa' => $no_wa
            ));
            $this->session->set_flashdata('success', 'Yeaa, save email & whatsapp number receiver successfully..');
            redirect('setting', 'refresh');
        }
    }
}
