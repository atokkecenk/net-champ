<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$cek = $this->mUser->cek_maintenance()->maintenance;
		if ($cek == '1') {
			redirect('maintenance');
		}
	}

	function data()
	{
		$data = array(
			'uri1' => $this->uri->segment(1),
			'uri2' => $this->uri->segment(2),
			'sess_level' => $this->session->userdata('level'),
			'pwd' => $this->session->userdata('password'),

			'odr_pending' => $this->mDash->get_data_order('admin', 'pending'),
			'odr_proses' => $this->mDash->get_data_order('admin', 'proses'),
			'odr_press' => $this->mDash->get_data_order('admin', 'spora'),
			'odr_paid' => $this->mDash->get_data_order('admin', 'paid'),
			'odr_unpaid' => $this->mDash->get_data_order('admin', 'unpaid'),
			'odr_presales' => $this->mDash->get_data_order('admin', 'presales'),
			'odr_ossgform' => $this->mDash->get_data_order('admin', 'ossgform'),
			'odr_portfull' => $this->mDash->get_data_order('admin', 'portfull'),
			'odr_reject' => $this->mDash->get_data_order('admin', 'reject'),
			'odr_cancel' => $this->mDash->get_data_order('admin', 'cancel'),
			'odr_complete' => $this->mDash->get_data_order('admin', 'complete'),
			'odr_total' => $this->mDash->get_data_order_total('admin'),

			'odr_pending_usr' => $this->mDash->get_data_order('user', 'pending'),
			'odr_proses_usr' => $this->mDash->get_data_order('user', 'proses'),
			'odr_press_usr' => $this->mDash->get_data_order('user', 'spora'),
			'odr_paid_usr' => $this->mDash->get_data_order('user', 'paid'),
			'odr_unpaid_usr' => $this->mDash->get_data_order('user', 'unpaid'),
			'odr_presales_usr' => $this->mDash->get_data_order('user', 'presales'),
			'odr_ossgform_usr' => $this->mDash->get_data_order('user', 'ossgform'),
			'odr_portfull_usr' => $this->mDash->get_data_order('user', 'portfull'),
			'odr_reject_usr' => $this->mDash->get_data_order('user', 'reject'),
			'odr_cancel_usr' => $this->mDash->get_data_order('user', 'cancel'),
			'odr_complete_usr' => $this->mDash->get_data_order('user', 'complete'),
			'odr_total_usr' => $this->mDash->get_data_order_total('user'),

			'odr_pending_ldr' => $this->mDash->get_data_order('leader', 'pending'),
			'odr_proses_ldr' => $this->mDash->get_data_order('leader', 'proses'),
			'odr_press_ldr' => $this->mDash->get_data_order('leader', 'spora'),
			'odr_paid_ldr' => $this->mDash->get_data_order('leader', 'paid'),
			'odr_unpaid_ldr' => $this->mDash->get_data_order('leader', 'unpaid'),
			'odr_presales_ldr' => $this->mDash->get_data_order('leader', 'presales'),
			'odr_ossgform_ldr' => $this->mDash->get_data_order('leader', 'ossgform'),
			'odr_portfull_ldr' => $this->mDash->get_data_order('leader', 'portfull'),
			'odr_reject_ldr' => $this->mDash->get_data_order('leader', 'reject'),
			'odr_cancel_ldr' => $this->mDash->get_data_order('leader', 'cancel'),
			'odr_complete_ldr' => $this->mDash->get_data_order('leader', 'complete'),
			'odr_total_ldr' => $this->mDash->get_data_order_total('leader'),

			'crd_admin' => $this->mDash->get_data_user('admin'),
			'crd_leader' => $this->mDash->get_data_user('leader'),
			'crd_user' => $this->mDash->get_data_user('user'),
			'crd_total' => $this->mDash->get_data_user_total(),

			'category' => $this->mProd->get_all_category()->result(),
			'announcement' => $this->mDash->get_all_announcement_active()
		);

		return $data;
	}

	public function index()
	{
		$data = $this->data();
		$data['c_sales_all'] = $this->mDash->get_data_order_chart();
		$data['c_cat_all'] = $this->mDash->get_data_order_chart_by_category();
		$this->render_page('dashboard', $data);
	}

	public function announcement()
	{
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['announcement'] = $this->mDash->get_all_announcement_active();
		$this->render_page('announcement', $data);
	}

	public function search_by_sales()
	{
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		if ($month !== '' &&  $year == '') {
			$where = '1';
		} elseif ($month == '' && $year !== '') {
			$where = '2';
		} elseif ($month !== '' && $year !== '') {
			$where = '3';
		} else {
			$where = null;
		}

		$data = $this->data();
		$data['c_sales_all'] = $this->mDash->get_data_order_chart($where);
		$data['c_cat_all'] = $this->mDash->get_data_order_chart_by_category();
		$this->render_page('dashboard', $data);
	}

	public function search_by_category()
	{
		$data = $this->data();
		$data['c_sales_all'] = $this->mDash->get_data_order_chart();
		$data['c_cat_all'] = $this->mDash->get_data_order_chart_by_category('search');
		$this->render_page('dashboard', $data);
	}
}