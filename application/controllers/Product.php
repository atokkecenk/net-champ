<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $cek = $this->mUser->cek_maintenance()->maintenance;
        if ($cek == '1') {
            redirect('maintenance');
        }
    }

    function get_all_product()
    {
        $list = $this->mProd->get_datatables();
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = strtoupper($item->name);
            $row[] = strtoupper($item->name_input) . '&nbsp;&nbsp;<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;">' . strtoupper($item->level_input) . '</span>';
            $row[] = '<i class="fas fa-circle text-c-green f-10 m-r-15"></i>' . $item->date . ', ' . $item->time;;
            $row[] = '<button type="button" class="btn btn-danger btn-delete-modif modalDeleteCat" data-toggle="modal" data-target="#modalDeleteCat" data-id="' . sha1($item->id) . '" title="Delete"><i class="feather icon-trash" style="margin: 0 !important;"></i></a>';
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mProd->count_all(),
            "recordsFiltered" => $this->mProd->count_filtered(),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    public function add_category()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2)
        );

        $this->render_page('menu/admin/add/add_category', $data);
    }

    public function view_category()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2)
        );

        $this->render_page('menu/admin/view/view_cat_product', $data);
    }

    public function save_category()
    {
        $this->mProd->save_category();
        $this->session->set_flashdata('success', 'Yeaa, save data category product successfully..');
        redirect('view_category_product');
    }
    
}
