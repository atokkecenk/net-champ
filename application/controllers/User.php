<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $cek = $this->mUser->cek_maintenance()->maintenance;
        if ($cek == '1') {
            redirect('maintenance');
        }
    }

    function get_all_user_admin()
    {
        $list = $this->mUser->get_datatables();
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            $u_hash = $this->db->get_where('tb_user', ['username' => $item->username, 'active' => 'Y'])->row();
            $lev = ($item->level == 'mainleader') ? 'MAIN LEADER' : strtoupper($item->level);
            $btn = ($item->level == 'user') ? '<i class="feather icon-eye eye-btn" role="button" id="detailHirarki" onclick="detailHirarki(\'' . base64_encode($u_hash->hash) . '\',\'' . strtoupper($u_hash->name) . '\')"></i> ' : null;
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = strtoupper($item->name);
            $row[] = $item->code;
            $row[] = $item->username;
            $row[] = $lev;
            $row[] = strtoupper($item->name_input) . '&nbsp;&nbsp;<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;">' . strtoupper($item->level_input) . '</span>';
            $row[] = '<i class="fas fa-circle text-c-green f-10 m-r-15"></i>' . $item->date . ', ' . $item->time;
            $row[] = '<button type="button" class="btn btn-success btnChangePwd btn-delete-modif mr-3" data-toggle="modal" data-target="#modalEditPwd" data-id="' . sha1($item->id) . '" data-hash="' . $item->hash . '" data-idd="' . $item->id . '" data-user="' . $item->username . '" title="Change Password"><i class="feather icon-unlock" style="margin: 0 !important;"></i></button>
                    <a href="' . base_url('edit-user/' . sha1($item->id)) . '" class="btn btn-primary btn-delete-modif" title="Edit"><i class="feather icon-edit" style="margin: 0 !important;"></i></a>
                    <button type="button" class="btn btn-warning btn-delete-modif mr-3 modalResetUser" data-toggle="modal" data-target="#modalResetUser" data-id="' . sha1($item->id) . '" data-idnohash="' . $item->id . '" data-user="' . $item->username . '" title="Reset Password"><i class="feather icon-refresh-cw" style="margin: 0 !important;"></i></button>
                    <button type="button" class="btn btn-danger btn-delete-modif modalDeleteUser" data-toggle="modal" data-target="#modalDeleteUser" data-id="' . sha1($item->id) . '" title="Delete"><i class="feather icon-trash" style="margin: 0 !important;"></i></button>';
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mUser->count_all(),
            "recordsFiltered" => $this->mUser->count_filtered(),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function get_all_user_leader()
    {
        $list = $this->mUser->get_datatables_leader();
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = strtoupper($item->name);
            $row[] = $item->code;
            $row[] = $item->username;
            $row[] = strtoupper($item->level);
            $row[] = strtoupper($item->name_input) . '&nbsp;&nbsp;<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;">' . strtoupper($item->level_input) . '</span>';
            $row[] = '<i class="fas fa-circle text-c-green f-10 m-r-15"></i>' . $item->date . ', ' . $item->time;
            $row[] = '<button type="button" class="btn btn-danger btn-delete-modif modalDeleteUser" data-toggle="modal" data-target="#modalDeleteUser" data-id="' . sha1($item->id) . '" title="Delete"><i class="feather icon-trash" style="margin: 0 !important;"></i></a>';
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mUser->count_all_leader(),
            "recordsFiltered" => $this->mUser->count_filtered_leader(),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    function get_all_id()
    {
        $list = $this->mID->get_datatables();
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = strtoupper($item->department);
            $row[] = $item->new_type;
            $row[] = strtoupper($item->name) . '&nbsp;&nbsp;<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;">' . strtoupper($item->level_input) . '</span>';;
            $row[] = '<i class="fas fa-circle text-c-green f-10 m-r-15"></i>' . $item->date . ', ' . $item->time;
            $row[] = '<button type="button" class="btn btn-danger btn-delete-modif modalDeleteId" data-toggle="modal" data-target="#modalDeleteId" data-id="' . sha1($item->id) . '" title="Delete"><i class="feather icon-trash" style="margin: 0 !important;"></i></a>';
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mID->count_all(),
            "recordsFiltered" => $this->mID->count_filtered(),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    public function detail_hirarki($id)
    {
        $nid = base64_decode($id);
        $sql = $this->db->query("SELECT
                                            * 
                                        FROM
                                            (
                                            SELECT
                                                0 urut,
                                                a.`level`,
                                                '|------------ ' hirarki,
                                                a.`name`,
                                                a.email 
                                            FROM
                                                tb_user a
                                                LEFT JOIN tb_leader b ON a.uid_leader = b.uid
                                                LEFT JOIN tb_user c ON b.uid = c.uid 
                                            WHERE
                                                a.`hash` = '$nid' 
                                                AND a.active = 'Y' UNION ALL
                                            SELECT
                                                1 urut,
                                                c.`level`,
                                                '|------ ' hirarki,
                                                c.`name`,
                                                c.email 
                                            FROM
                                                tb_user a
                                                LEFT JOIN tb_leader b ON a.uid_leader = b.uid
                                                LEFT JOIN tb_user c ON b.uid = c.uid 
                                            WHERE
                                                a.`hash` = '$nid' 
                                                AND a.active = 'Y' UNION ALL
                                            SELECT
                                                2 urut,
                                                e.`level`,
                                                '|-- ' hirarki,
                                                e.`name`,
                                                e.email 
                                            FROM
                                                tb_user a
                                                LEFT JOIN tb_leader b ON a.uid_leader = b.uid
                                                LEFT JOIN tb_user c ON b.uid = c.uid
                                                LEFT JOIN tb_main_leader d ON b.uid_main_leader = d.uid
                                                LEFT JOIN tb_user e ON d.uid = e.uid 
                                            WHERE
                                                a.`hash` = '$nid' 
                                                AND a.active = 'Y' 
                                            ) us 
                                        ORDER BY
                                            us.urut DESC")->result();
        foreach ($sql as $s) {
            switch ($s->level) {
                case 'mainleader':
                    $lv = 'ML';
                    break;
                case 'leader':
                    $lv = 'LD';
                    break;
                case 'user':
                    $lv = 'US';
                    break;
            }
            echo '<span class="badge badge-pill" style="color: #fff; background-color: #04a9f5;min-width: 40px;">' . $lv . '</span> ' . $s->hirarki . ' <b>' . $s->name . '</b><br/>';
        }
        echo '<br/><span style="font-size: 11px;"><b>ML</b> : Main Leader&nbsp;&nbsp;&nbsp;<b>LD</b> : Leader&nbsp;&nbsp;&nbsp;<b>US</b> : User<br/></span>';
    }

    public function add_user()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
            'leader' => $this->mUser->get_leader()->result(),
            'data_id' => $this->mUser->get_id()->result()
        );

        $this->render_page('menu/admin/add/add_user', $data);
    }

    public function add_user_leader()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
            'user' => $this->mUser->get_user()->result(),
            'data_id' => $this->mUser->get_id()->result()
        );

        $this->render_page('menu/leader/add_user_leader', $data);
    }

    public function add_id()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
        );

        $this->render_page('menu/admin/add/add_id', $data);
    }

    public function view_id()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
        );

        $this->render_page('menu/admin/view/view_id', $data);
    }

    public function view_user_admin()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
        );

        $this->render_page('menu/admin/view/view_user', $data);
    }

    public function view_user_leader()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
        );

        $this->render_page('menu/leader/view_user_leader', $data);
    }

    public function set_credentials()
    {
        $data = array(
            'uri1' => $this->uri->segment(1),
            'uri2' => $this->uri->segment(2),
        );

        $this->render_page('menu/setup/setup-credentials', $data);
    }

    public function set_by_credentials($credential)
    {
        $staff_id = $this->mUser->get_id()->result();
        $form = '<div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nama Penuh</label>
                            <input type="text" class="form-control" name="name" placeholder="Nama Penuh" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>IC Number</label>
                            <input type="text" class="form-control" name="ic_num" placeholder="IC Number" maxlength="12">
                            <small class="form-text text-c-red">Input without strip (-)</small>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Username</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="username" id="username" oninput="cekUsername()" placeholder="Username" required>
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="feather icon-user"></i></span>
                                </div>
                            </div>
                            <p id="usernameCek"></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <div class="input-group mb-3">
                                <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="feather icon-mail"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Passport</label>
                            <input type="text" class="form-control" name="passport" placeholder="Passport">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>No HP</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" name="no_hp" placeholder="No HP" required>
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="feather icon-smartphone"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Staff ID</label>
                        <div class="form-group">';
        $no = 1;
        foreach ($staff_id as $da_id) {
            $form .= '<div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="radioTypeId' . $no . '" name="type_id" class="custom-control-input" value="' . $da_id->id . '">
                                    <label class="custom-control-label" for="radioTypeId' . $no . '">' . $da_id->new_id . '</label>
                                </div>';
            $no++;
        }
        $form .= '</div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Alamat Tempat Tinggal</label>
                            <textarea class="form-control" name="alamat" rows="3" placeholder="Alamat Tempat Tinggal" required></textarea>
                        </div>
                    </div>
                </div>
                <div>
                    <a href="' . base_url('view_user_admin') . '" class="btn btn-secondary"><i class="feather icon-arrow-left"></i>Back</a>
                    <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Save</button>
                </div>';

        //--------------------------------------- batas --------------------------------------//
        $html = '';
        if ($credential == 'admin') {
            $html = '<h5><i class="feather icon-sliders" style="margin-right:10px;"></i>Setup for credential <b class="my-lable">ADMINISTRATOR</b></h5>';
        } elseif ($credential == 'mainleader') {
            $html = '<h5><i class="feather icon-sliders" style="margin-right:10px;"></i>Setup for credential <b class="my-lable">MAIN LEADER</b></h5>';
        } elseif ($credential == 'leader') {
            $sql = $this->db->query("SELECT
                                        a.*,
                                        b.`name` 
                                    FROM
                                        `tb_main_leader` a
                                        LEFT JOIN tb_user b ON a.uid = b.uid 
                                    WHERE
                                        b.active = 'Y' 
                                        AND a.active = 'Y'")->result();
            $html = '<h5><i class="feather icon-sliders" style="margin-right:10px;"></i>Setup for credential <b class="my-lable">LEADER</b></h5>';
            $html .= '
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Leader</label>
                                    <select class="form-control" name="mainleader" required>
                                        <option value="">- Select Main Leader -</option>';
            foreach ($sql as $us) {
                $html .= '<option value="' . $us->uid . '">' . strtoupper(strtolower($us->name)) . '</option>';
            }
            $html .= '</select>
                                </div>
                            </div>
                        </div>';
        } elseif ($credential == 'user') {
            $sql = $this->db->query("SELECT
                                        a.*,
                                        b.`name`,
                                        c.`name` main_leader 
                                    FROM
                                        `tb_leader` a
                                        LEFT JOIN tb_user b ON a.uid = b.uid
                                        LEFT JOIN tb_user c ON a.uid_main_leader = c.uid 
                                    WHERE
                                        b.active = 'Y' 
                                        AND a.active = 'Y'")->result();
            $html = '<h5><i class="feather icon-sliders" style="margin-right:10px;"></i>Setup for credential <b class="my-lable">USER</b></h5>';
            $html .= '
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Leader</label>
                                    <select class="form-control" name="leader" required>
                                        <option value="">- Select Leader -</option>';
            foreach ($sql as $us) {
                $html .= '<option value="' . $us->uid . '">' . strtoupper(strtolower($us->name)) . '</option>';
            }
            $html .= '</select>
                                </div>
                            </div>
                        </div>';
        }
        echo $html . $form;
    }

    public function edit_user($id)
    {
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['data_id'] = $this->mUser->get_id()->result();
        $data['leader'] = $this->mUser->get_leader()->result();
        $data['user'] = $this->mUser->get_user_by_id_sha($id)->row();
        $this->render_page('menu/admin/edit/edit_user', $data);
    }

    public function save_user()
    {
        $this->mUser->save_user();
        $this->session->set_flashdata('success', 'Yeaa, save data user successfully..');
        redirect('view_user_admin');
    }

    public function save_user_leader()
    {
        $this->mUser->save_user_leader();
        $this->session->set_flashdata('success', 'Yeaa, save data user successfully..');
        redirect('view_user_leader');
    }

    public function save_id()
    {
        $this->mUser->save_id();
        $this->session->set_flashdata('success', 'Yeaa, save data ID successfully..');
        redirect('view_id');
    }

    public function save_edit_user()
    {
        $id = $this->input->post('id');
        $level = $this->input->post('level');
        $name = $this->input->post('name');
        $leader = $this->input->post('leader');
        $username = $this->input->post('username');
        $ic_num = $this->input->post('ic_num');
        $passport = $this->input->post('passport');
        $id_code = $this->input->post('type_id');
        $alamat = $this->input->post('alamat');
        $no_hp = $this->input->post('no_hp');
        $email = $this->input->post('email');
        $hash = $this->session->userdata('hash');

        $p1 = substr($ic_num, 0, 6);
        $p2 = substr($ic_num, 6, 2);
        $p3 = substr($ic_num, 8, 4);
        $ic_num2 = $p1 . '-' . $p2 . '-' . $p3;
        $new_noic = ($ic_num == '') ? null : $ic_num2;
        $new_nopassport = ($passport == '') ? null : $passport;

        $get_value_id = $this->mUser->get_value_number($id_code)->row();
        $get_id = $this->mUser->get_max_id($get_value_id->id)->row()->no;
        if ($get_id == '') {
            $new_digit = $get_value_id->tengah;
        } else {
            $new_digit = ($get_id + 1);
        }

        switch ($this->session->level) {
            case 'admin':
                $rdr = 'view_user_admin';
                break;

            case 'leader':
                $rdr = 'view_user_leader';
                break;
        }

        if ($id_code == '' || $id_code == null) {
            $this->db->where('id', $id);
            $this->db->update('tb_user', array(
                'name' => $name,
                'alamat'    => $alamat,
                'number_hp' => $no_hp,
                'email'     => $email,
                'number_ic' => $new_noic,
                'username'  => $username,
                'passport'  => $new_nopassport,
                'level'     => $level,
                'leader'    => $leader,
                'user_edit' => $this->session->hash,
                'date_edit' => date('Y-m-d H:i:s')
            ));

            $this->db->insert('tb_history', array(
                'note_1' => 'Edit data user',
                'status' => 'edit-user',
                'id' => $id,
                'user_input' => $this->session->hash,
                'date_input' => date('Y-m-d H:i:s')
            ));

            $this->session->set_flashdata('success', 'Yeaa, Save edit data user successfully..');
            redirect($rdr);
        } else {
            $this->db->where('id', $id);
            $this->db->update('tb_user', array(
                'name' => $name,
                'alamat'    => $alamat,
                'number_hp' => $no_hp,
                'email'     => $email,
                'number_ic' => $new_noic,
                'username'  => $username,
                'passport'  => $new_nopassport,
                'awal'      => $get_value_id->awal,
                'tengah'    => $new_digit,
                'akhir'     => $get_value_id->akhir,
                'level'     => $level,
                'leader'    => $leader,
                'user_edit' => $this->session->hash,
                'date_edit' => date('Y-m-d H:i:s')
            ));

            $new_id = $get_value_id->awal . $new_digit . $get_value_id->akhir;
            $this->db->insert('tb_history', array(
                'note_1' => 'Edit data user',
                'note_2' => 'Change ID to ' . $new_id,
                'status' => 'edit-user',
                'id' => $id,
                'user_input' => $this->session->hash,
                'date_input' => date('Y-m-d H:i:s')
            ));

            $this->session->set_flashdata('success', 'Yeaa, Save edit data user successfully..');
            redirect($rdr);
        }
    }

    public function check_username($username)
    {
        $chk = $this->mUser->check_username($username);
        if ($chk->num_rows() > 0) {
            $sts = 1;
            $msg = 'Username already exist !!';
        } else {
            $sts = 0;
            $msg = 'Username ready to use';
        }

        $data = array(
            'status' => $sts,
            'message'   => $msg
        );

        echo json_encode($data);
    }

    public function update_password()
    {
        $this->mUser->update_password();
        $this->session->set_flashdata('success', 'Yeaa, update password successfully..');
        redirect('auth/logout');
    }

    public function delete($where)
    {
        $level = $this->session->userdata('level');
        $id = $this->input->post('id');

        if ($where == 'id') {
            $data = array('active' => 'N');
            $where2 = 'sha1(id)';
            $table = 'tb_id';
            $rdr = 'view_id';
        } elseif ($where == 'user') {
            $data = array('active' => 'N');
            $where2 = 'sha1(id)';
            $table = 'tb_user';
            switch ($level) {
                case 'admin':
                    $rdr = 'view_user_admin';
                    break;

                case 'leader':
                    $rdr = 'view_user_leader';
                    break;
            }
        } elseif ($where == 'zone') {
            $data = array('active' => 'N');
            $where2 = 'sha1(id)';
            $table = 'tb_zone';
            $rdr = 'view_zone';
        } elseif ($where == 'segment') {
            $data = array('active' => 'N');
            $where2 = 'sha1(id)';
            $table = 'tb_segment';
            $rdr = 'view_segment';
        } elseif ($where == 'type') {
            $data = array('active' => 'N');
            $where2 = 'sha1(id)';
            $table = 'tb_type_order';
            $rdr = 'view_type';
        } elseif ($where == 'category') {
            $data = array('active' => 'N');
            $where2 = 'sha1(id)';
            $table = 'tb_category_product';
            $rdr = 'view_category_product';
        } elseif ($where == 'announcement') {
            $file = $this->db->query("SELECT foto FROM `tb_announcement` where sha(id) = '" . $id . "'")->row()->foto;
            if ($file !== '') {
                unlink('assets/images/announcement/' . $file);
            }

            $data = array('status' => 'N');
            $where2 = 'sha1(id)';
            $table = 'tb_announcement';
            $rdr = 'view-announcement';
        }

        $this->db->where($where2, $id);
        $this->db->update($table, $data);
        $this->session->set_flashdata('success', 'Yeaa, delete data successfully..');
        redirect($rdr);
    }

    public function reset_password()
    {
        $id = $this->input->post('id');
        $idno_hash = $this->input->post('idno_hash');
        $this->db->where('sha1(id)', $id);
        $this->db->update('tb_user', array('password' => 'bbf2dead374654cbb32a917afd236656'));

        $history = array(
            'note_1' => 'Admin Reset Password',
            'status' => 'reset-password',
            'id' => $idno_hash,
            'order_number' => null,
            'user_input' => $this->session->userdata('hash'),
            'date_input' => date("Y-m-d H:i:s")
        );
        $this->mOrder->save_history($history);
        $this->session->set_flashdata('success', 'Yeaa, reset password successfully..');
        redirect('view_user_admin');
    }

    // TAMBAHAN
    public function change_password()
    {
        $id = $this->input->post('id');
        $pwd = $this->input->post('pwd');
        $cf_pwd = $this->input->post('cf_pwd');
        $hash = $this->input->post('hash');

        if ($pwd !== $cf_pwd) {
            $this->session->set_flashdata('error', 'Oops, New password and confirm password not same!!');
            redirect('view_user_admin', 'refresh');
        } else {
            $this->db->where('id', $id);
            $upd = $this->db->update('tb_user', ['password' => md5($pwd), 'password_text' => $pwd]);
            if ($this->session->hash == $hash) {
                $this->session->set_flashdata('success', 'Yeaa, change password successfully..!!');
                redirect('logout', 'refresh');
            } else {
                $this->session->set_flashdata('success', 'Yeaa, change password successfully..!!');
                redirect('view_user_admin', 'refresh');
            }
        }
    }
}
