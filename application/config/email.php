<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
|---------------------------------------------------------------------
| CodeIgniter Email Configuration File.
|---------------------------------------------------------------------
|
| Location: ./application/config/email.php
|
| Please see the link below for Email Configuration values.
|
| https://codeigniter.com/user_guide/libraries/email.html#setting-email-preferences-in-a-config-file
*/

$config['useragent']        = 'CodeIgniter';
$config['protocol']         = 'smtp';
$config['mailpath']         = '/usr/sbin/sendmail';
$config['smtp_host']        = 'mail.contohsite.com';
$config['smtp_user']        = 'trial-email@contohsite.com';
$config['smtp_pass']        = 'yjW=J@QYNp?)';
$config['smtp_port']        = 465;
$config['smtp_timeout']     = 60;
$config['smtp_keepalive']   = FALSE;
$config['smtp_crypto']      = 'ssl';
$config['wordwrap']         = TRUE;
$config['wrapchars']        = 76;
$config['mailtype']         = 'html';
$config['charset']          = 'utf-8';
$config['validate']         = FALSE;
$config['priority']         = 1;
$config['crlf']             = '\n';
$config['newline']          = '\n';
$config['bcc_batch_mode']   = FALSE;
$config['bcc_batch_size']   = 200;
$config['dsn']              = FALSE;

/**
 * -----------------------------------------------------------------------
 * Filename: email.php
 * Location: ./application/config/email.php
 * -----------------------------------------------------------------------
 */
