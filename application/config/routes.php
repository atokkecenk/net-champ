<?php
defined('BASEPATH') or exit('No direct script access allowed');

// $route['default_controller'] = 'Auth';
$route['register'] = 'Auth/form_order';
$route['default_controller'] = 'Auth/landing';
$route['dashboard'] = 'Home';
$route['announcement'] = 'Home/announcement';

// FOR ADMIN
$route['add_user'] = 'User/add_user';
$route['add_id'] = 'User/add_id';
$route['add_zone'] = 'Zone/add_zone';
$route['add_segment'] = 'Segment/add_segment';
$route['add_type'] = 'type/add_type';
$route['add_category_product'] =  'Product/add_category';
$route['add-announcement'] =  'Announcement/add';

$route['edit_order/(.+)'] = 'order/edit_order/$1';

$route['view-order/(.+)'] = 'Order/view_order/$1';
$route['view_id'] = 'User/view_id';
$route['view_zone'] = 'Zone/view_zone';
$route['view_user_admin'] = 'User/view_user_admin';
$route['view_type'] = 'Type/view_type';
$route['view_segment'] = 'Segment/view_segment';
$route['view_category_product'] = 'Product/view_category';
$route['view-detail-order/(.+)'] = 'Order/view_detail_order/$1';
$route['view-announcement'] = 'Announcement/view';
$route['report-pdf'] = 'Report/view_pdf';
$route['report-pdf-by-sales'] = 'Report/pdf_by_sales';

$route['save_reupload/(.+)'] = 'Order/save_reupload/$1';
$route['save_remarks/(.+)'] = 'Order/save_remarks/$1';
$route['save_id'] = 'User/save_id';
$route['save_user'] = 'User/save_user';
$route['save_zone'] = 'Zone/save_zone';
$route['save_segment'] = 'Segment/save_segment';
$route['save_type'] = 'Type/save_type';
$route['save_category'] = 'Product/save_category';
$route['save_edit_password'] = 'User/update_password';
$route['save_order_number'] = 'Order/update_order_number';
$route['save-announcement'] = 'Announcement/save';

// tambahan
$route['change-password'] = 'User/change_password';
$route['logout'] = 'Auth/logout';


// SETTING
$route['setting_text'] = 'Setting/setting_text';
$route['setting_text'] = 'Setting/setting_text';
$route['setting_favicon'] = 'Setting/setting_favicon';
$route['setting_logo'] = 'Setting/setting_logo';
$route['setting_logo_login'] = 'Setting/setting_logo_login';
$route['setting-landing-text'] = 'Setting/setting_landing_text';
$route['setting-landing-background'] = 'Setting/setting_background_landing';
$route['setting-receiver-order'] = 'Setting/setting_receiver_order';

$route['delete_id'] = 'User/delete/id';
$route['delete_user'] = 'User/delete/user';
$route['delete_zone'] = 'User/delete/zone';
$route['delete_segment'] = 'User/delete/segment';
$route['delete_type'] = 'User/delete/type';
$route['delete_category'] = 'User/delete/category';
$route['delete_annc'] = 'User/delete/announcement';
$route['delete-order/(:num)'] = 'Order/delete/$1';

$route['edit-user/(.+)'] = 'User/edit_user/$1';
$route['save-edit-user'] = 'User/save_edit_user';

$route['reset-password'] = 'User/reset_password';

$route['status-announcement/(.+)'] = 'Announcement/update_announcement_status/$1';

$route['setting'] = 'Setting/setting_website';
$route['setting-landing-page'] = 'Setting/setting_landing';

$route['search'] = 'Auth/search';
$route['search-sales-report'] = 'Home/search_by_sales';
$route['search-sales-report-category'] = 'Home/search_by_category';
$route['pending/(.+)/(.+)'] = 'Order/pending/$1/$2';
$route['complete/(.+)/(.+)'] = 'Order/complete/$1/$2';

// LOGIN
$route['login'] = 'Auth';

// PACKAGE
$route['view-package'] = 'Package/view';
$route['add-package'] = 'Package/add';
$route['save-package'] = 'Package/save';
$route['save-sub-package'] = 'Package/save_sub';
$route['delete/(:any)'] = 'Package/delete/$1';

// GET DATA
$route['get-order/(.+)'] = 'Order/get_order/$1';
$route['get-order-leader/(.+)'] = 'Order/get_order_by_leader/$1';
$route['get-order-user/(.+)'] = 'Order/get_order_by_user/$1';
$route['get-sub-package'] = 'Auth/get_sub_package';

// MAINTENANCE
$route['maintenance'] = 'Maintenance';
$route['activate'] = 'Maintenance/update_maintenance';
$route['activate-website'] = 'Maintenance/activate_website';
$route['off-system'] = 'Maintenance/off_system';

// FOR LEADER
// $route['view_order_leader'] = 'Order/view_order_leader';
$route['view-order-leader/(:any)'] = 'Order/view_order_leader/$1';
$route['view_user_leader'] = 'User/view_user_leader';
$route['add_user_leader'] = 'User/add_user_leader';
$route['save_user_leader'] = 'User/save_user_leader';

// FOR USER
$route['input_order'] = 'Order';
$route['save-order-customer'] = 'OrderCustomer/save_order_customer';
$route['save_order'] = 'Order/save_order';
$route['view-order-user/(:any)'] = 'Order/view_order_user/$1';

$route['excel-order'] = 'Report/excel_order';
$route['ex-order/(.+)/(.+)'] = 'Excel/ex_order/$1/$2';

// Trial
$route['send-email'] = 'SendEmail/send_mail';
$route['send-email2'] = 'SendEmail/send_mail2';

// SETUP
$route['set-credentials'] = 'User/set_credentials';
$route['detail-hirarki/(.+)'] = 'User/detail_hirarki/$1';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
