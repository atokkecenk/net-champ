<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelUser extends CI_Model
{
    // start datatables admin
    var $column_order = array('a.id', 'a.name', 'a.tengah', 'a.username', 'a.level', 'a.name', 'a.date_input');
    var $column_search = array('a.name', 'a.awal', 'a.tengah', 'a.akhir', 'a.username', 'a.level', 'b.name', 'a.date_input');
    var $order = array('a.id' => 'desc');

    private function _get_datatables_query()
    {
        $this->db->select("a.*, CONCAT(a.awal, a.tengah, a.akhir) AS `code`, DATE_FORMAT(DATE(a.date_input),'%d %M %Y') AS date, TIME(a.date_input) AS time, b.name AS name_input, b.`level` as level_input");
        $this->db->from('tb_user as a');
        $this->db->join('tb_user as b', 'a.user_input = b.hash', 'left');
        $this->db->where('a.active', 'Y');
        $this->db->order_by('a.id', 'desc');
        $i = 0;
        foreach ($this->column_search as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all()
    {
        $this->db->from('tb_user');
        $this->db->where('active', 'Y');
        return $this->db->count_all_results();
    }
    // end datatables

    // start datatables leader
    var $column_order_leader = array('a.id', 'a.name', 'a.tengah', 'a.username', 'a.level', 'a.name', 'a.date_input');
    var $column_search_leader = array('a.name', 'a.awal', 'a.tengah', 'a.akhir', 'a.username', 'a.level', 'b.name', 'a.date_input');
    var $order_leader = array('id' => 'asc');

    private function _get_datatables_query_leader()
    {
        $hash = $this->session->userdata('hash');
        $id_leader = $this->mDash->get_id_leader($hash);

        $this->db->select("a.*, CONCAT(a.awal, a.tengah, a.akhir) AS `code`, DATE_FORMAT(DATE(a.date_input),'%d %M %Y') AS date, TIME(a.date_input) AS time, b.name AS name_input, b.`level` as level_input");
        $this->db->from('tb_user as a');
        $this->db->join('tb_user as b', 'a.user_input = b.hash', 'left');
        $this->db->where('a.active', 'Y');
        $this->db->where('a.`level`', 'user');
        $this->db->where('a.leader', $id_leader);
        $i = 0;
        foreach ($this->column_search_leader as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search_leader) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order_leader[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_leader()
    {
        $this->_get_datatables_query_leader();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_leader()
    {
        $this->_get_datatables_query_leader();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_leader()
    {
        $hash = $this->session->userdata('hash');
        $id_leader = $this->mDash->get_id_leader($hash);

        $this->db->select("a.*, CONCAT(a.awal, a.tengah, a.akhir) AS `code`, DATE_FORMAT(DATE(a.date_input),'%d %M %Y') AS date, TIME(a.date_input) AS time, b.name AS name_input");
        $this->db->from('tb_user as a');
        $this->db->join('tb_user as b', 'a.user_input = b.hash', 'left');
        $this->db->where('a.active', 'Y');
        $this->db->where('a.`level`', 'user');
        $this->db->where('a.leader', $id_leader);
        return $this->db->count_all_results();
    }
    // end datatables

    public function cek_maintenance()
    {
        $q = $this->db->select('maintenance')
            ->from('tb_setting')
            ->get()
            ->row();
        return $q;
    }

    public function get($username)
    {
        $this->db->where('username', $username);
        $this->db->where('active', 'Y');
        $q = $this->db->get('tb_user')->row();
        return $q;
    }

    public function get_leader()
    {
        $level = array('admin', 'leader');
        $this->db->where_in('level', $level);
        $this->db->where('active', 'Y');
        $this->db->order_by('level', 'asc');
        $q = $this->db->get('tb_user');
        return $q;
    }

    public function get_user()
    {
        $this->db->where('level', 'user');
        $this->db->where('active', 'Y');
        $this->db->order_by('name', 'ASC');
        $q = $this->db->get('tb_user');
        return $q;
    }

    public function get_user_by_id($id)
    {
        $data = $this->db->select('*')
            ->from('tb_user')
            ->where('id', $id)
            ->get();
        return $data;
    }

    public function get_user_by_id_sha($id)
    {
        $data = $this->db->select('*')
            ->from('tb_user')
            ->where('sha1(id)', $id)
            ->get();
        return $data;
    }

    public function get_id_leader($hash)
    {
        $this->db->select('leader as id_leader');
        $this->db->from('tb_user');
        $this->db->where('hash', $hash);
        $q = $this->db->get();
        return $q->row();
    }

    public function get_hash_admin()
    {
        $this->db->select('max(RIGHT(hash,6)) AS no');
        $this->db->from('tb_user');
        $this->db->where('level', 'admin');
        $q = $this->db->get();
        return $q;
    }

    public function get_hash_mainleader()
    {
        $this->db->select('max(RIGHT(hash,6)) AS no');
        $this->db->from('tb_user');
        $this->db->where('level', 'mainleader');
        $q = $this->db->get();
        return $q;
    }

    public function get_hash_leader($where = null, $id = null)
    {
        if ($where == 'order_by_leader' && $id !== null) {
            $this->db->select('hash AS no');
            $this->db->from('tb_user');
            $this->db->where('id', $id);
            $q = $this->db->get();
            return $q->row();
        } else {
            $this->db->select('max(RIGHT(hash,6)) AS no');
            $this->db->from('tb_user');
            $this->db->where('level', 'leader');
            $q = $this->db->get();
            return $q;
        }
    }

    public function get_hash_user()
    {
        $this->db->select('max(RIGHT(hash,6)) AS no');
        $this->db->from('tb_user');
        $this->db->where('level', 'user');
        $q = $this->db->get();
        return $q;
    }

    public function get_id()
    {
        $this->db->select('a.*, CONCAT(a.awal, a.tengah,a.akhir) AS new_id');
        $this->db->from('tb_id as a');
        $this->db->where('a.active', 'Y');
        $q = $this->db->get();
        return $q;
    }

    public function get_max_id($id)
    {
        $this->db->select('max(tengah) AS no');
        $this->db->from('tb_user');
        $this->db->where('id_code', $id);
        $q = $this->db->get();
        return $q;
    }

    public function get_value_number($id)
    {
        $this->db->where('id', $id);
        $q = $this->db->get('tb_id');
        return $q;
    }

    public function save_user()
    {
        $level = $this->input->post('level');
        $name = $this->input->post('name');
        $leader = $this->input->post('leader');
        $username = $this->input->post('username');
        $ic_num = $this->input->post('ic_num');
        $passport = $this->input->post('passport');
        $id_code = $this->input->post('type_id');
        $alamat = $this->input->post('alamat');
        $no_hp = $this->input->post('no_hp');
        $email = $this->input->post('email');
        $hash = $this->session->userdata('hash');

        $p1 = substr($ic_num, 0, 6);
        $p2 = substr($ic_num, 6, 2);
        $p3 = substr($ic_num, 8, 4);
        $ic_num2 = $p1 . '-' . $p2 . '-' . $p3;
        $new_noic = ($ic_num == '') ? null : $ic_num2;
        $new_nopassport = ($passport == '') ? null : $passport;
        $new_leader = ($leader == '') ? null : $leader;
        $uid = $this->uid(6);

        if ($level == 'admin') {
            $last_hash = $this->get_hash_admin()->row()->no;
            $last_hash2 = '000000' . ($last_hash + 1);
            $new_hash = 'admin' . substr($last_hash2, -6);
            $uid_leader = null;
        } elseif ($level == 'mainleader') {
            $last_hash = $this->get_hash_mainleader()->row()->no;
            $last_hash2 = '000000' . ($last_hash + 1);
            $new_hash = 'mainleader' . substr($last_hash2, -6);

            //--------------- insert tb_leader -------------//
            $uid_leader = null;
            $data = [
                'uid'           => $uid,
                'active'        => 'Y',
                'create_by'     => $this->session->hash,
                'create_date'   => date('Y-m-d H:i:s'),
            ];
            $this->db->insert('tb_main_leader', $data);
        } elseif ($level == 'leader') {
            $last_hash = $this->get_hash_leader()->row()->no;
            $last_hash2 = '000000' . ($last_hash + 1);
            $new_hash = 'leader' . substr($last_hash2, -6);

            //--------------- insert tb_leader -------------//
            $uid_ml = $this->input->post('mainleader');
            $uid_leader = null;
            $data = [
                'uid'   => $uid,
                'uid_main_leader' => $uid_ml,
                'active'        => 'Y',
                'create_by'     => $this->session->hash,
                'create_date'   => date('Y-m-d H:i:s'),
            ];
            $this->db->insert('tb_leader', $data);
        } elseif ($level == 'user') {
            $last_hash = $this->get_hash_user()->row()->no;
            $last_hash2 = '000000' . ($last_hash + 1);
            $new_hash = 'user' . substr($last_hash2, -6);
            $uid_leader = $this->input->post('leader');
        }

        $get_value_id = $this->get_value_number($id_code)->row();
        $get_id = $this->get_max_id($get_value_id->id)->row()->no;
        if ($get_id == '') {
            $new_digit = $get_value_id->tengah;
        } else {
            $new_digit = ($get_id + 1);
        }

        $data = array(
            'id_code'  => $id_code,
            'name'      => $name,
            'alamat'    => $alamat,
            'number_hp' => $no_hp,
            'email'     => $email,
            'number_ic' => $new_noic,
            'passport'  => $new_nopassport,
            'username'  => str_replace(' ', '', $username),
            'password'  => md5('ABC123'),
            'password_text' => 'ABC123456',
            'level'     => $level,
            'hash'      => $new_hash,
            'uid'       => $uid,
            'uid_leader' => $uid_leader,
            'awal'      => $get_value_id->awal,
            'tengah'    => $new_digit,
            'akhir'     => $get_value_id->akhir,
            'leader'    => $new_leader,
            'user_input'    => $hash,
            'date_input'    => date('Y-m-d H:i:s')
        );

        $this->db->insert('tb_user', $data);
    }

    public function uid($length)
    {
        $karakter = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcde1234567890';
        $uid = substr(str_shuffle($karakter), 0, $length) . '-' . substr(str_shuffle($karakter), 0, $length);
        return $uid;
    }

    public function save_user_leader()
    {
        $name = $this->input->post('name');
        $username = $this->input->post('username');
        $ic_num = $this->input->post('ic_num');
        $passport = $this->input->post('passport');
        $id_code = $this->input->post('type_id');
        $alamat = $this->input->post('alamat');
        $no_hp = $this->input->post('no_hp');
        $email = $this->input->post('email');
        $hash = $this->session->userdata('hash');
        $id_leader = $this->mDash->get_id_leader($hash);

        $p1 = substr($ic_num, 0, 6);
        $p2 = substr($ic_num, 6, 2);
        $p3 = substr($ic_num, 8, 4);
        $ic_num2 = $p1 . '-' . $p2 . '-' . $p3;
        $new_noic = ($ic_num == '') ? null : $ic_num2;
        $new_nopassport = ($passport == '') ? null : $passport;

        $last_hash = $this->get_hash_user()->row()->no;
        $last_hash2 = '000000' . ($last_hash + 1);
        $new_hash = 'user' . substr($last_hash2, -6);

        $get_value_id = $this->get_value_number($id_code)->row();
        $get_id = $this->get_max_id($get_value_id->id)->row()->no;
        if ($get_id == '') {
            $new_digit = $get_value_id->tengah;
        } else {
            $new_digit = ($get_id + 1);
        }

        $data = array(
            'id_code'  => $id_code,
            'name'      => $name,
            'alamat'    => $alamat,
            'number_hp' => $no_hp,
            'email'     => $email,
            'number_ic' => $new_noic,
            'passport'  => $new_nopassport,
            'username'  => $username,
            'password'  => md5('ABC123'),
            'password_text' => 'xxxxxxxx',
            'level'     => 'user',
            'hash'      => $new_hash,
            'awal'      => $get_value_id->awal,
            'tengah'    => $new_digit,
            'akhir'     => $get_value_id->akhir,
            'leader'    => $id_leader,
            'user_input'    => $hash,
            'date_input'    => date('Y-m-d H:i:s')
        );

        $this->db->insert('tb_user', $data);
    }

    public function save_id()
    {
        $department = $this->input->post('department');
        $company = $this->input->post('company');
        $dept_code = $this->input->post('dept_code');
        $type = $this->input->post('type');
        $hash = $this->session->userdata('hash');

        $data = array(
            'department'  => $department,
            'awal'      => $company,
            'tengah'    => $dept_code,
            'akhir'     => $type,
            'user_input'    => $hash,
            'date_input'    => date('Y-m-d H:i:s')
        );

        $this->db->insert('tb_id', $data);
    }

    public function check_username($user)
    {
        $q = $this->db->select('*')
            ->from('tb_user')
            ->where('username', $user)
            ->where('active', 'Y')
            ->get();
        return $q;
    }

    public function check_status_user($user, $pass)
    {
        $q = $this->db->select('status')
            ->from('tb_user')
            ->where('username', $user)
            ->where('password', $pass)
            ->get();
        return $q;
    }

    public function update_password()
    {
        $hash = $this->input->post('hash');
        $password = $this->input->post('password');
        $this->db->where('hash', $hash);
        $this->db->update('tb_user', array('password' => md5($password)));
    }
}
