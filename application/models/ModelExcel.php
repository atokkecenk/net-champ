<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelExcel extends CI_Model
{
    public function order($month, $year)
    {
        $mth = '';
        $yr = '';
        if ($month !== '0') {
            if ($month !== '0' && $month !== 'all') {
                $mth = " AND MONTH(a.approve_date) = '$month' ";
            }
        }
        if ($year !== '0') {
            if ($year !== '0' && $year !== 'all') {
                $yr = " AND YEAR(a.approve_date) = '$year' ";
            }
        }
        return $this->db->query("SELECT
                            a.id,
                            a.name_customer,
                            a.number_ic,
                            a.mobile_phone,
                            a.email,
                            a.address,
                            a.category_product,
                            b.`name` AS nm_product,
                            a.approve,
                            a.approve_by,
                            c.`name`,
                            DATE_FORMAT( a.approve_date, '%Y-%m-%d' ) AS acc_date 
                        FROM
                            `tb_order` a
                            LEFT JOIN tb_category_product b ON a.category_product = b.id
                            LEFT JOIN tb_user c ON a.approve_by = c.`hash` 
                        WHERE
                            a.approve = 'complete' 
                            $mth
                            $yr
                        ORDER BY
                            a.id DESC")->result();
    }
}
