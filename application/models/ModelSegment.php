<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelSegment extends CI_Model
{
    // start datatables admin
    var $column_order = array('a.id', 'a.name');
    var $column_search = array('a.name');
    var $order = array('id' => 'asc');

    private function _get_datatables_query()
    {
        $this->db->select("a.*, DATE_FORMAT(DATE(a.date_input),'%d %M %Y') AS date, TIME(a.date_input) AS time, b.name AS name_input, b.`level` as level_input");
        $this->db->from('tb_segment as a');
        $this->db->join('tb_user as b', 'a.user_input = b.hash', 'left');
        $this->db->where('a.active', 'Y');
        $i = 0;
        foreach ($this->column_search as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all()
    {
        $this->db->from('tb_segment');
        $this->db->where('active', 'Y');
        return $this->db->count_all_results();
    }
    // end datatables

    public function save_segment()
    {
        $segment = $this->input->post('segment');
        $hash = $this->session->userdata('hash');

        $data = array(
            'name' => $segment,
            'user_input'    => $hash,
            'date_input'    => date('Y-m-d H:i:s')
        );

        $this->db->insert('tb_segment', $data);
    }
}
