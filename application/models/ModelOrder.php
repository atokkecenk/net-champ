<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelOrder extends CI_Model
{
    // start datatables admin
    var $column_order = array('a.id', 'a.number_order', 'a.number_ic', 'a.name_customer', 'c.name', 'a.price', 'a.approve');
    var $column_search = array('a.id', 'a.number_order', 'a.number_ic', 'a.name_customer', 'c.name', 'a.price', 'a.approve');
    var $order = array('a.id' => 'DESC');

    private function _get_datatables_query($mode)
    {
        $this->db->select("b.`name` AS name_zone,
                            c.`name` AS name_sales,
                            d.`name` AS name_cat_product,
                            f.`value` AS new_type,
                            e.`name` AS name_user_input,
                            DATE_FORMAT( a.date_input, '%d %b %Y, %l:%i %p' ) AS new_date,
                            a.* ");
        $this->db->from('tb_order as a');
        $this->db->join('tb_zone as b', 'a.zone = b.id', 'left');
        $this->db->join('tb_user as c', 'a.sales = c.id', 'left');
        $this->db->join('tb_category_product as d', 'a.category_product = d.id', 'left');
        $this->db->join('tb_user as e', 'a.user_input = e.`hash`', 'left');
        $this->db->join('tb_type_order as f', 'a.type = f.id', 'left');
        $this->db->where('a.approve', $mode);
        $i = 0;
        foreach ($this->column_search as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables($mode)
    {
        $this->_get_datatables_query($mode);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered($mode)
    {
        $this->_get_datatables_query($mode);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all()
    {
        $this->db->from('tb_order');
        return $this->db->count_all_results();
    }
    // end datatables


    // start datatables leader
    var $column_order_leader = array('a.id', 'a.number_order', 'a.number_ic', 'a.name_customer', 'e.`name`', 'b.name', 'a.segment', 'f.value', 'a.price', 'a.approve');
    var $column_search_leader = array('a.number_order', 'a.number_ic', 'a.name_customer', 'e.`name`', 'b.name', 'a.segment', 'f.value', 'a.price', 'a.approve');
    var $order_leader = array('a.id' => 'asc');

    private function _get_datatables_query_leader($mode)
    {
        $this->db->select("b.`name` AS name_zone,
                            c.`name` AS name_sales,
                            d.`name` AS name_cat_product,
                            f.`value` AS new_type,
                            e.`name` AS name_user_input,
                            e.`level` AS level_input,
                            g.`hash` AS hash_leader,
                            g.`name` AS name_leader,
                            h.`name` AS name_sales,
                            DATE_FORMAT( a.date_input, '%d %b %Y, %l:%i %p' ) AS new_date,
                            a.* ");
        $this->db->from('tb_order as a');
        $this->db->join('tb_zone as b', 'a.zone = b.id', 'left');
        $this->db->join('tb_sales as c', 'a.sales = c.id', 'left');
        $this->db->join('tb_category_product as d', 'a.category_product = d.id', 'left');
        $this->db->join('tb_user as e', 'a.user_input = e.`hash`', 'left');
        $this->db->join('tb_type_order as f', 'a.type = f.id', 'left');
        $this->db->join('tb_user as g', 'e.leader = g.id', 'left');
        $this->db->join('tb_user as h', 'a.sales = h.id', 'left');
        $this->db->where('a.approve', $mode);
        $this->db->where('a.hash_leader', $this->session->userdata('hash'));

        $i = 0;
        foreach ($this->column_search_leader as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search_leader) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order_leader[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_leader($mode)
    {
        $this->_get_datatables_query_leader($mode);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_leader($mode)
    {
        $this->_get_datatables_query_leader($mode);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_leader($mode)
    {
        $this->db->select('a.*');
        $this->db->from('tb_order as a');
        $this->db->join('tb_user as b', 'a.user_input = b.`hash`', 'left');
        $this->db->join('tb_user as c', 'b.leader = c.id', 'left');
        $this->db->where('a.approve', $mode);
        $this->db->where('a.hash_leader', $this->session->userdata('hash'));
        return $this->db->count_all_results();
    }
    // end datatables


    // start datatables user
    var $column_order_user = array('a.id', 'a.number_order', 'a.number_ic', 'a.name_customer', 'b.name', 'a.segment', 'f.value', 'a.price', 'a.approve');
    var $column_search_user = array('a.number_order', 'a.number_ic', 'a.name_customer', 'b.name', 'a.segment', 'f.value', 'a.price', 'a.approve');
    var $order_user = array('a.id' => 'asc');

    private function _get_datatables_query_user($mode)
    {
        $this->db->select("b.`name` AS name_zone,
                            c.`name` AS name_sales,
                            d.`name` AS name_cat_product,
                            f.`value` AS new_type,
                            e.`name` AS name_user_input,
                            DATE_FORMAT( a.date_input, '%d %b %Y, %l:%i %p' ) AS new_date,
                            a.* ");
        $this->db->from('tb_order as a');
        $this->db->join('tb_zone as b', 'a.zone = b.id', 'left');
        $this->db->join('tb_sales as c', 'a.sales = c.id', 'left');
        $this->db->join('tb_category_product as d', 'a.category_product = d.id', 'left');
        $this->db->join('tb_user as e', 'a.user_input = e.`hash`', 'left');
        $this->db->join('tb_type_order as f', 'a.type = f.id', 'left');
        $this->db->where('a.approve', $mode);
        $this->db->where('a.sales', $this->session->userdata('id'));
        $i = 0;
        foreach ($this->column_search_user as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search_user) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order_user[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_user($mode)
    {
        $this->_get_datatables_query_user($mode);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_user($mode)
    {
        $this->_get_datatables_query_user($mode);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_user($mode)
    {
        $this->db->from('tb_order');
        $this->db->where('approve', $mode);
        $this->db->where('sales', $this->session->userdata('id'));
        return $this->db->count_all_results();
    }
    // end datatables

    public function get_zone()
    {
        $q = $this->db->select('*')
            ->from('tb_zone')
            ->where('active', 'Y')
            ->order_by('name', 'ASC')
            ->get();
        return $q;
    }

    public function get_segment()
    {
        $q = $this->db->select('*')
            ->from('tb_segment')
            ->where('active', 'Y')
            ->order_by('name', 'ASC')
            ->get();
        return $q;
    }

    public function get_id_leader($id)
    {
        $this->db->select('leader as id_leader');
        $this->db->from('tb_user');
        $this->db->where('id', $id);
        $q = $this->db->get();
        return $q->row();
    }

    public function get_hash($id)
    {
        $q = $this->db->select('hash as no')
            ->from('tb_user')
            ->where('id', $id)
            ->get();
        return $q->row();
    }

    public function get_sales($where = null, $id = null)
    {
        if ($where == 'leader') {
            $q = $this->db->select('*')
                ->from('tb_user')
                ->where('leader', $id)
                ->where('active', 'Y')
                ->order_by('name', 'ASC')
                ->get();
            return $q;
        } else {
            $q = $this->db->select('*')
                ->from('tb_user')
                // ->where('level', 'user')
                ->where('active', 'Y')
                ->order_by('level', 'ASC')
                ->order_by('name', 'ASC')
                ->get();
            return $q;
        }
    }

    public function get_cat_product()
    {
        $q = $this->db->select('*')
            ->from('tb_category_product')
            ->where('active', 'Y')
            ->order_by('name', 'ASC')
            ->get();
        return $q;
    }

    public function get_type()
    {
        $q = $this->db->select('*')
            ->from('tb_type_order')
            ->where('active', 'Y')
            ->order_by('value', 'ASC')
            ->get();
        return $q;
    }

    public function cek_oder_number($num)
    {
        $q = $this->db->select('number_order as no')
            ->from('tb_order')
            ->where('number_order', $num)
            ->get();
        return $q;
    }

    public function cek_ic_number($num)
    {
        $q = $this->db->select('number_ic as no')
            ->from('tb_order')
            ->where('number_ic', $num)
            ->get();
        return $q;
    }

    public function cek_edit_leader($mode, $id)
    {
        $this->db->select('edit_leader as no');
        $this->db->from('tb_order');
        if ($mode == 'sha') {
            $this->db->where('sha1(id)', $id);
        } else {
            $this->db->where('id', $id);
        }
        $q = $this->db->get();
        return $q->row()->no;
    }

    public function get_order_where($id)
    {
        $this->db->select("b.`name` AS name_zone,
                            d.`name` AS name_cat_product,
                            g.`value` AS new_type,
                            UPPER(e.`level`) AS level_input,
                            UPPER(e.`name`) AS name_user_input,
                            UPPER(DATE_FORMAT(DATE(a.date_input),'%d %M %Y')) AS new_date_input,
                            UPPER(f.`name`) AS name_approve_by,
                            UPPER(DATE_FORMAT(DATE(a.approve_date),'%d %M %Y')) AS acc_date,
                            UPPER(f.`level`) AS acc_level,
                            a.* ");
        $this->db->from('tb_order as a');
        $this->db->join('tb_zone as b', 'a.zone = b.id', 'left');
        $this->db->join('tb_type_order as g', 'a.type = g.id', 'left');
        $this->db->join('tb_category_product as d', 'a.category_product = d.id', 'left');
        $this->db->join('tb_user as e', 'a.user_input = e.`hash`', 'left');
        $this->db->join('tb_user as f', 'a.approve_by = f.`hash`', 'left');
        $this->db->where('a.id', $id);
        $q = $this->db->get();
        return $q;
    }

    public function get_order_where_sha($id)
    {
        $this->db->select("IF ( a.approve = 'paid', 'PAID', IF ( a.approve = 'unpaid', 'UNPAID', 'PENDING' ) ) AS `status`,
                            b.`name` AS name_zone,
                            j.`name` AS name_segment,
                            d.`name` AS name_cat_product,
                            i.`value` AS new_type,
                            REPLACE ( a.number_ic, '-', '' ) AS new_numberic,
                            g.`name` AS name_sales,
                            CONCAT( g.awal, g.tengah, g.akhir ) AS sales_id,
	                        h.`name` AS name_leader,
	                        CONCAT( h.awal, h.tengah, h.akhir ) AS leader_id,
	                        e.`name` AS name_user_input,
                            e.`level` AS level_user_input,
                            f.`name` AS name_approve_by,
                            f.`level` AS level_approve,
                            DATE_FORMAT( DATE( a.date ), '%d %M %Y' ) AS new_date,
                            DATE_FORMAT( DATE( a.date_input ), '%d %M %Y' ) AS new_date_input,
                            TIME_FORMAT(TIME( a.date_input ), '%H:%i %p') AS time,
                            DATE_FORMAT( DATE( a.approve_date ), '%d %M %Y' ) AS acc_date,
                            a.* ");
        $this->db->from('tb_order as a');
        $this->db->join('tb_zone as b', 'a.zone = b.id', 'left');
        $this->db->join('tb_segment as j', 'a.segment = j.id', 'left');
        $this->db->join('tb_type_order as i', 'a.type = i.id', 'left');
        $this->db->join('tb_category_product as d', 'a.category_product = d.id', 'left');
        $this->db->join('tb_user as e', 'a.user_input = e.`hash`', 'left');
        $this->db->join('tb_user as f', 'a.approve_by = f.`hash`', 'left');
        $this->db->join('tb_user as g', 'a.sales = g.id', 'left');
        $this->db->join('tb_user as h', 'a.hash_leader = h.`hash` ', 'left');
        $this->db->where('sha1(a.id)', $id);
        $q = $this->db->get();
        return $q;
    }

    public function save($img1 = null, $img2 = null, $img3 = null, $img4 = null, $img5 = null)
    {
        $level = $this->session->userdata('level');
        $hash = $this->session->userdata('hash');
        $id = $this->session->userdata('id');

        $no_order = $this->input->post('order_num');
        $no_ic = $this->input->post('ic_num');
        $no_passport = $this->input->post('passport_num');
        $name_cust = $this->input->post('name_cust');
        $mobile_phone = $this->input->post('mobile_phone');
        $del_number = $this->input->post('del_number');
        $date = $this->input->post('date');
        $zone = $this->input->post('zone');
        $sales = $this->input->post('sales');
        $cat_product = $this->input->post('cat_product');
        $segment = $this->input->post('segment');
        $harga_paket = $this->input->post('harga_paket');
        $type_order = $this->input->post('type_order');
        $email = $this->input->post('email');
        $address = $this->input->post('address');
        $press = $this->input->post('press');
        $g_form = $this->input->post('google_form');
        $eform_id = $this->input->post('eform_id');

        $p1 = substr($no_ic, 0, 6);
        $p2 = substr($no_ic, 6, 2);
        $p3 = substr($no_ic, 8, 4);
        $new_noic = $p1 . '-' . $p2 . '-' . $p3;

        $ndate = explode('/', $date);
        $new_date = $ndate[2] . '-' . $ndate[1] . '-' . $ndate[0];

        $id_leader = $this->get_id_leader($sales)->id_leader;
        $leader = $this->get_hash($id_leader)->no;

        $get_has_lead_by_user = $this->mUser->get_id_leader($hash)->id_leader;
        $get_has_lead_by_user2 = $this->mUser->get_hash_leader('order_by_leader', $get_has_lead_by_user)->no;

        switch ($level) {
            case 'admin':
                $hash_leader = $leader;
                break;

            case 'leader':
                $hash_leader = $hash;
                break;

            case 'user':
                $hash_leader = $get_has_lead_by_user2;
                break;
        }

        $data = array(
            'number_order'  => $no_order,
            'name_customer' => $name_cust,
            'number_ic'     => $new_noic,
            'number_passport' => $no_passport,
            'mobile_phone' => $mobile_phone,
            'del_number' => $del_number,
            'date'      => $new_date,
            'zone'      => $zone,
            'sales'     => $sales,
            'category_product'  => $cat_product,
            'segment'   => $segment,
            'price'     => $harga_paket,
            'type'      => $type_order,
            'email'      => $email,
            'address'      => $address,
            'press4u'      => $press,
            'google_form'  => $g_form,
            'eform_id'  => $eform_id,
            'image1'    => $img1,
            'image2'    => $img2,
            'image3'    => $img3,
            'image4'    => $img4,
            'image5'    => $img5,
            'approve'    => 'pending',
            'hash_leader' => $hash_leader,
            'user_input' => $this->session->userdata('hash'),
            'date_input' => date("Y-m-d H:i:s")
        );

        $this->db->insert('tb_order', $data);
    }

    public function save_edit_reupload($img1 = null, $img2 = null, $img3 = null, $img4 = null, $img5 = null)
    {
        $level = $this->session->userdata('level');
        $id = $this->input->post('id');
        $no_order = $this->input->post('order_num');
        $no_ic = $this->input->post('ic_num');
        $no_passport = $this->input->post('passport_num');
        $name_cust = $this->input->post('name_cust');
        $date = $this->input->post('date');
        $zone = $this->input->post('zone');
        $sales = $this->input->post('sales');
        $cat_product = $this->input->post('cat_product');
        $segment = $this->input->post('segment');
        $harga_paket = $this->input->post('harga_paket');
        $type_order = $this->input->post('type_order');
        $status = $this->input->post('status');

        $p1 = substr($no_ic, 0, 6);
        $p2 = substr($no_ic, 6, 2);
        $p3 = substr($no_ic, 8, 4);
        $new_noic = $p1 . '-' . $p2 . '-' . $p3;

        $ndate = explode('/', $date);
        $new_date = $ndate[2] . '-' . $ndate[1] . '-' . $ndate[0];

        switch ($level) {
            case 'admin':
                $data = array(
                    'number_order'  => $no_order,
                    'name_customer' => $name_cust,
                    'number_ic'     => $new_noic,
                    'number_passport' => $no_passport,
                    'date'      => $new_date,
                    'zone'      => $zone,
                    'sales'     => $sales,
                    'category_product'  => $cat_product,
                    'segment'   => $segment,
                    'price'     => $harga_paket,
                    'type'      => $type_order,
                    'image1'    => $img1,
                    'image2'    => $img2,
                    'image3'    => $img3,
                    'image4'    => $img4,
                    'image5'    => $img5,
                    'user_edit' => $this->session->userdata('hash'),
                    'date_edit' => date("Y-m-d H:i:s")
                );
                break;

            case 'leader':
                $chk = $this->cek_edit_leader('no_sha', $id);
                $chk2 = $chk + 1;
                $data = array(
                    'number_order'  => $no_order,
                    'name_customer' => $name_cust,
                    'number_ic'     => $new_noic,
                    'number_passport' => $no_passport,
                    'date'      => $new_date,
                    'zone'      => $zone,
                    'sales'     => $sales,
                    'category_product'  => $cat_product,
                    'segment'   => $segment,
                    'price'     => $harga_paket,
                    'type'      => $type_order,
                    'image1'    => $img1,
                    'image2'    => $img2,
                    'image3'    => $img3,
                    'image4'    => $img4,
                    'image5'    => $img5,
                    'edit_leader' => $chk2,
                    'edit_leader_hash' => $this->session->userdata('hash'),
                    'edit_leader_date' => date("Y-m-d H:i:s")
                );
                break;

            case 'user':
                $data = array(
                    'number_order'  => $no_order,
                    'name_customer' => $name_cust,
                    'number_ic'     => $new_noic,
                    'number_passport' => $no_passport,
                    'date'      => $new_date,
                    'zone'      => $zone,
                    'sales'     => $sales,
                    'category_product'  => $cat_product,
                    'segment'   => $segment,
                    'price'     => $harga_paket,
                    'type'      => $type_order,
                    'image1'    => $img1,
                    'image2'    => $img2,
                    'image3'    => $img3,
                    'image4'    => $img4,
                    'image5'    => $img5,
                    'user_edit' => $this->session->userdata('hash'),
                    'date_edit' => date("Y-m-d H:i:s")
                );
                break;
        }

        $this->db->where(array('id' => $id));
        $this->db->update('tb_order', $data);
    }

    public function save_edit()
    {
        $level = $this->session->userdata('level');
        $id = $this->input->post('id');
        $no_order = $this->input->post('order_num');
        $no_ic = $this->input->post('ic_num');
        $no_passport = $this->input->post('passport_num');
        $name_cust = $this->input->post('name_cust');
        $date = $this->input->post('date');
        $zone = $this->input->post('zone');
        $sales = $this->input->post('sales');
        $cat_product = $this->input->post('cat_product');
        $segment = $this->input->post('segment');
        $harga_paket = $this->input->post('harga_paket');
        $type_order = $this->input->post('type_order');
        $email = $this->input->post('email');
        $address = $this->input->post('address');
        $press = $this->input->post('press');
        $g_form = $this->input->post('google_form');
        $eform_id = $this->input->post('eform_id');

        $p1 = substr($no_ic, 0, 6);
        $p2 = substr($no_ic, 6, 2);
        $p3 = substr($no_ic, 8, 4);
        $new_noic = $p1 . '-' . $p2 . '-' . $p3;

        $ndate = explode('/', $date);
        $new_date = $ndate[2] . '-' . $ndate[1] . '-' . $ndate[0];

        switch ($level) {
            case 'admin':
                $remakrs = $this->input->post('remakrs');
                $data = array(
                    'number_order'  => $no_order,
                    'name_customer' => $name_cust,
                    'number_ic'     => $new_noic,
                    'number_passport' => $no_passport,
                    'date'      => $new_date,
                    'zone'      => $zone,
                    'sales'     => $sales,
                    'category_product'  => $cat_product,
                    'segment'   => $segment,
                    'price'     => $harga_paket,
                    'type'      => $type_order,
                    'email'      => $email,
                    'address'      => $address,
                    'press4u'      => $press,
                    'google_form'  => $g_form,
                    'eform_id'  => $eform_id,
                    'remarks'  => $remakrs,
                    'user_edit' => $this->session->userdata('hash'),
                    'date_edit' => date("Y-m-d H:i:s")
                );
                break;

            case 'leader':
                $chk = $this->cek_edit_leader('no_sha', $id);
                $chk2 = $chk + 1;
                $remakrs = $this->input->post('remakrs');
                $data = array(
                    'number_order'  => $no_order,
                    'name_customer' => $name_cust,
                    'number_ic'     => $new_noic,
                    'number_passport' => $no_passport,
                    'date'      => $new_date,
                    'zone'      => $zone,
                    'sales'     => $sales,
                    'category_product'  => $cat_product,
                    'segment'   => $segment,
                    'price'     => $harga_paket,
                    'type'      => $type_order,
                    'email'      => $email,
                    'address'      => $address,
                    'press4u'      => $press,
                    'google_form'  => $g_form,
                    'eform_id'  => $eform_id,
                    'remarks'  => $remakrs,
                    'edit_leader' => $chk2,
                    'edit_leader_hash' => $this->session->userdata('hash'),
                    'edit_leader_date' => date("Y-m-d H:i:s")
                );
                break;

            case 'user':
                $remakrs = $this->input->post('remakrs');
                $data = array(
                    'number_order'  => $no_order,
                    'name_customer' => $name_cust,
                    'number_ic'     => $new_noic,
                    'number_passport' => $no_passport,
                    'date'      => $new_date,
                    'zone'      => $zone,
                    'sales'     => $sales,
                    'category_product'  => $cat_product,
                    'segment'   => $segment,
                    'price'     => $harga_paket,
                    'type'      => $type_order,
                    'email'      => $email,
                    'address'      => $address,
                    'press4u'      => $press,
                    'google_form'  => $g_form,
                    'eform_id'  => $eform_id,
                    'remarks'  => $remakrs,
                    'user_edit' => $this->session->userdata('hash'),
                    'date_edit' => date("Y-m-d H:i:s")
                );
                break;
        }

        $this->db->where(array('id' => $id));
        $this->db->update('tb_order', $data);

        $history = array(
            'note_1' => 'Edit Order',
            'status' => 'edit',
            'id' => $id,
            'order_number' => $no_order,
            'user_input' => $this->session->hash,
            'date_input' => date("Y-m-d H:i:s")
        );
        $this->mOrder->save_history($history);
    }

    public function save_reupload_file($column, $file_name)
    {
        $id = $this->input->post('id');
        $this->db->where('id', $id);
        $this->db->update('tb_order', array($column => $file_name));
    }

    public function save_history($data)
    {
        $this->db->insert('tb_history', $data);
    }

    public function search($search)
    {
        $q = $this->db->query("SELECT b.`name` AS name_sales, a.number_order, a.name_customer, a.approve FROM `tb_order` a LEFT JOIN tb_user b ON a.sales = b.id WHERE
                                a.name_customer LIKE '%$search%' 
                                OR a.number_order LIKE '%$search%' 
                                OR b.`name` LIKE '%$search%'");
        return $q;
    }

    public function tujuan_email($id)
    {
        return $this->db->query("SELECT
                                    a.id,
                                    a.name_customer,
                                    a.sales id_sales,
                                    UPPER( b.`name` ) sales,
                                    b.email,
                                    b.bank_name,
                                    b.bank_no
                                FROM
                                    `tb_order` a
                                    LEFT JOIN tb_user b ON a.sales = b.id 
                                WHERE
                                    a.id = '$id'");
    }
}
