<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelPackage extends CI_Model
{
    function get_data_package()
    {
        return $this->db->select('a.*, DATE_FORMAT(DATE(a.date_input),"%d %M %Y") AS date, TIME(a.date_input) AS time, b.name, b.level')
            ->from('tb_package a')
            ->join('tb_user b', 'a.user_input = b.hash', 'left')
            ->where('a.active', 'Y')
            ->order_by('a.id_pkg', 'desc')
            ->get()->result();
    }
}
