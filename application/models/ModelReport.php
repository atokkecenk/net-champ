<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelReport extends CI_Model
{
    public function get_report_by_sales($sales, $month, $year)
    {
        // $q = $this->db->query("SELECT
        //                 a.sales,
        //                 a.number_order,
        //                 b.`name` AS name_sales,
        //                 a.name_customer,
        //                 a.price,
        //                 a.date,
        //                 DATE_FORMAT( a.date, '%y %M %Y' ) AS new_date,
        //                 YEAR ( a.date ) AS `year`,
        //                 a.approve,
        //                 c.`name` AS name_approve 
        //             FROM
        //                 tb_order a
        //                 LEFT JOIN tb_user b ON a.sales = b.id
        //                 LEFT JOIN tb_user c ON a.approve_by = c.`hash` 
        //             WHERE
        //                 a.sales = '$sales' 
        //                 AND YEAR ( a.date ) = '$year'
        //             ");
        
        $q = $this->db->select("a.sales,
                        a.number_order,
                        b.`name` AS name_sales,
                        a.name_customer,
                        a.price,
                        a.date,
                        DATE_FORMAT( a.date, '%y %M %Y' ) AS new_date,
                        YEAR ( a.date ) AS `year`,
                        a.approve,
                        c.`name` AS name_approve")
                    ->from('tb_order a')
                    ->join('tb_user b', 'a.sales = b.id','left')
                    ->join('tb_user c', 'a.approve_by = c.`hash`','left')
                    ->where('a.sales', $sales)
                    ->where('MONTH ( a.date ) = ', $month)
                    ->where('YEAR ( a.date ) = ', $year)
                    ->get();
        return $q;
    }
}
