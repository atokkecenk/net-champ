<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelDashboard extends CI_Model
{
    public function get_data_user($where)
    {
        $q = $this->db->select('*')
            ->from('tb_user')
            ->where('level', $where)
            ->where('active', 'Y')
            ->count_all_results();
        return $q;
    }

    public function get_data_user_total()
    {
        $q = $this->db->select('*')
            ->from('tb_user')
            ->where('active', 'Y')
            ->count_all_results();
        return $q;
    }

    public function get_id_leader($hash)
    {
        $this->db->select('id as no');
        $this->db->from('tb_user');
        $this->db->where('hash', $hash);
        $q = $this->db->get();
        return $q->row()->no;
    }

    public function get_hash_user($id)
    {
        $this->db->select('hash as no');
        $this->db->from('tb_user');
        $this->db->where('leader', $id);
        $q = $this->db->get();
        return $q->result();
    }

    public function get_data_order($level = null, $where = null)
    { 
        if ($level == 'admin') {
            $this->db->select('*');
            $this->db->from('tb_order');
            $this->db->where('approve', $where);
        } elseif ($level == 'user') {
            $this->db->select('*');
            $this->db->from('tb_order');
            $this->db->where('approve', $where);
            $this->db->where('sales', $this->session->id);
        } elseif ($level == 'leader') {
            $hash = $this->session->userdata('hash');
            $this->db->select('a.user_input,
                                b.`name`,
                                a.approve,
                                b.leader');
            $this->db->from('tb_order a');
            $this->db->join('tb_user b', 'a.user_input = b.`hash`', 'left');
            if(in_array($where, ['pending','proses','spora','paid','unpaid','cancel','presales','ossgform','portfull','reject','complete'])){
                $this->db->where('a.approve', $where);
            }
            $this->db->where('a.hash_leader', $hash);
        }
        $q = $this->db->count_all_results();
        return $q;
    }

    public function get_data_order_total($level = null)
    {
        if ($level == 'admin') {
            $this->db->select('*');
            $this->db->from('tb_order');
        } elseif ($level == 'user') {
            $this->db->select('*');
            $this->db->from('tb_order');
            $this->db->where('sales', $this->session->id);
        } elseif ($level == 'leader') {
            $hash = $this->session->userdata('hash');
            $this->db->select('a.user_input,
                                b.`name`,
                                a.approve,
                                b.leader ');
            $this->db->from('tb_order a');
            $this->db->join('tb_user b', 'a.user_input = b.`hash`', 'left');
            $this->db->where('a.hash_leader', $hash);
        }
        $q = $this->db->count_all_results();
        return $q;
    }

    public function get_data_order_chart($where = null)
    {
        $month = $this->input->post('month');
        $year = $this->input->post('year');

        $this->db->select('a.`name`, COUNT( b.sales ) AS total');
        $this->db->from('tb_user a');
        $this->db->join('tb_order b', 'a.id = b.sales', 'left');
        $this->db->where('a.`level`', 'user');
        if ($where == '1') {
            $this->db->where('MONTH(b.date)', $month);
        } elseif ($where == '2') {
            $this->db->where('YEAR(b.date)', $year);
        } elseif ($where == '3') {
            $this->db->where('MONTH(b.date)', $month);
            $this->db->where('YEAR(b.date)', $year);
        } else {
            $this->db->where('YEAR(b.date)', date('Y'));
        }
        $this->db->group_by('a.id');
        $this->db->order_by('total', 'DESC');
        if ($where == null) {
            $this->db->limit(8);
        }
        $q = $this->db->get();
        return $q;
    }

    public function get_data_order_chart_by_category($where = null)
    {
        $cat = $this->input->post('category');
        $month = $this->input->post('month2');
        $year = $this->input->post('year2');

        $this->db->select('a.sales,
                            c.`name` AS name_sales,
                            b.id AS id_category,
                            b.`name` AS `name`,
                            a.date,
                            MONTH ( a.date ) AS `month`,
                            YEAR ( a.date ) AS `year`,
                            COUNT( a.category_product ) AS total');
        $this->db->from('tb_order a');
        $this->db->join('tb_category_product b', 'a.category_product = b.id', 'left');
        $this->db->join('tb_user c', 'a.sales = c.id', 'left');
        if ($where == 'search') {
            $this->db->where('b.id', $cat);
            $this->db->where('MONTH ( a.date ) =', $month);
            $this->db->where('YEAR ( a.date ) =', $year);
        }
        $this->db->group_by('a.category_product');
        $this->db->group_by('a.sales');
        $this->db->order_by('total', 'DESC');
        // $this->db->order_by('a.id', 'ASC');
        if ($where !== 'search') {
            $this->db->limit(8);
        }
        $q = $this->db->get();
        return $q;
    }

    public function get_all_announcement_active()
    {   
        $q = $this->db->select("a.*, DATE_FORMAT(a.start_date, '%d/%m/%Y') AS new_start, DATE_FORMAT(a.end_date, '%d/%m/%Y') AS new_end, DATEDIFF( CURRENT_DATE, a.end_date ) AS hitung, CAST(DATEDIFF(CURRENT_DATE, DATE(a.date_input)) AS SIGNED) as new")
                ->from('tb_announcement a')
                ->where('a.`status`', 'Y')
                ->where('a.`publish`', 'publish')
                // ->having('hitung <= ', 0)
                ->order_by('a.id', 'desc')
                ->get();
        return $q;
    }

    public function cek_announcement_active()
    {   
        $q = $this->db->select('id,
                            masa,
                            `subject`,
                            start_date,
                            end_date,
                            `status`,
                            DATEDIFF( end_date, CURRENT_DATE ) AS hitung ')
                ->from('tb_announcement')
                ->having('masa', 'D')
                ->having('`status`', 'Y')
                ->having('hitung < ', 0)
                ->get();
        return $q;
    }

    public function update_announcement($id)
    {
        $this->db->where(array('id' => $id));
        $this->db->update('tb_announcement', array('status' => 'N'));
    }

}
