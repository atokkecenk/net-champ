const flashdata = $(".flash-data").data("flashdata");
const flashtipe = $(".flash-data").data("flashtipe");

if (flashdata && flashtipe == "success") {
	sweetAlert({
		title: "Success",
		text: flashdata,
		type: "success",
		showConfirmButton: false,
		timer: 2600,
	});
} else if (flashdata && flashtipe == "error") {
	sweetAlert({
		title: "Error",
		text: flashdata,
		type: "error",
		showConfirmButton: false,
		timer: 2600,
	});
}

$("#date, #startDate, #endDate").datepicker({
	format: "dd/mm/yyyy",
	autoclose: true,
	todayHighlight: true,
	todayBtn: "linked",
});

// document.querySelector(".btnPress4U").onclick = function () {
// 	const rmk = document.querySelector('#remarks').value;
// 	document.querySelector("#remarksModal").value = rmk;
// };

$(function () {
	$('[data-toggle="tooltip"]').tooltip();
});

var mode_view = $("#modeViewOrder").val();
$("#tbl-view-order").DataTable({
	responsive: true,
	autoWidth: false,
	processing: true,
	serverSide: true,
	aaSorting: [[0, "DESC"]],
	ajax: {
		url: "../get-order/" + mode_view,
		type: "POST",
	},
	columnDefs: [
		{
			targets: [0, 8],
			className: "text-center",
		},
		{
			targets: [5],
			className: "text-right",
		},
		{
			orderable: false,
			targets: [7],
		},
	],
	language: {
		zeroRecords: "No Data Found !!",
	},
});

$("#tbl-view-order-leader").DataTable({
	responsive: true,
	autoWidth: false,
	processing: true,
	serverSide: true,
	aaSorting: [[0, "DESC"]],
	ajax: {
		url: "../get-order-leader/" + mode_view,
		type: "POST",
	},
	columnDefs: [
		{
			targets: [0, 8],
			className: "text-center",
		},
		{
			targets: [7],
			className: "text-right",
		},
	],
	language: {
		zeroRecords: "No Data Found !!",
	},
});

$("#tbl-view-order-user").DataTable({
	responsive: true,
	autoWidth: false,
	processing: true,
	serverSide: true,
	aaSorting: [[0, "DESC"]],
	ajax: {
		url: "../get-order-user/" + mode_view,
		type: "POST",
	},
	columnDefs: [
		{
			targets: [0],
			className: "text-center",
		},
		{
			targets: [6],
			className: "text-right",
		},
	],
	language: {
		zeroRecords: "No Data Found !!",
	},
});

$("#tbl-view-id").DataTable({
	responsive: true,
	autoWidth: false,
	processing: true,
	serverSide: true,
	ajax: {
		url: "User/get_all_id",
		type: "POST",
	},
	columnDefs: [
		{
			targets: [0, 5],
			className: "text-center",
		},
		{
			orderable: false,
			targets: [5],
		},
	],
	language: {
		zeroRecords: "No Data Found !!",
	},
});

$("#tbl-view-user").DataTable({
	responsive: true,
	autoWidth: false,
	processing: true,
	serverSide: true,
	ajax: {
		url: "User/get_all_user_admin",
		type: "POST",
	},
	columnDefs: [
		{
			targets: [0, 7],
			className: "text-center",
		},
		{
			orderable: false,
			targets: [7],
		},
	],
	language: {
		zeroRecords: "No Data Found !!",
	},
});

$("#order-status").html(
	'<i class="feather icon-help-circle" style="font-size: 1.5em; color: #37474f; background-color: #fff; padding: 1.3em 1.7em 1.3em 1.7em; border: 1px solid #37474f; border-radius: 8px;"> Press the button</i>'
);

$("#paid").on("click", function () {
	$('[name="order_status"]').val("paid");
	$("#order-status").html(
		'<i class="feather icon-check-circle" style="font-size: 1.5em; color: #fff; background-color: #1de9b6; padding: 1.3em 1.7em 1.3em 1.7em; border-radius: 8px;"> Paid</i>'
	);

	sweetAlert({
		title: "Paid",
		text: "",
		type: "success",
		showConfirmButton: false,
		timer: 2400,
	});
});

$("#unpaid").on("click", function () {
	$('[name="order_status"]').val("unpaid");
	$("#order-status").html(
		'<i class="feather icon-x-circle" style="font-size: 1.5em; color: #fff; background-color: #f44236; padding: 1.3em 1.7em 1.3em 1.7em; border-radius: 8px;"> Unpaid</i>'
	);

	sweetAlert({
		title: "Unpaid",
		text: "",
		type: "error",
		showConfirmButton: false,
		timer: 2400,
	});
});

$("#btnCopyURL").click(function () {
	var copyText = document.getElementById("source");
	copyText.select();
	copyText.setSelectionRange(0, 99999); /* For mobile devices */
	document.execCommand("copy");
});

$(".uploadAnncImg").change(function () {
	$("#uploadAnncImg").html(
		'<img src="assets/images/setting/check-1.png" height="20px"> Image ready to upload'
	);
});

$(".upload1").change(function () {
	$("#upload1")
		.attr("src", "assets/images/setting/check-1.png")
		.attr("width", 80)
		.attr("height", 80);
});

$(".upload2").change(function () {
	$("#upload2")
		.attr("src", "assets/images/setting/check-1.png")
		.attr("width", 80)
		.attr("height", 80);
});

$(".upload3").change(function () {
	$("#upload3")
		.attr("src", "assets/images/setting/check-1.png")
		.attr("width", 80)
		.attr("height", 80);
});

$(".upload4").change(function () {
	$("#upload4")
		.attr("src", "assets/images/setting/check-1.png")
		.attr("width", 80)
		.attr("height", 80);
});

$(".upload5").change(function () {
	$("#upload5")
		.attr("src", "assets/images/setting/check-1.png")
		.attr("width", 80)
		.attr("height", 80);
});

$("#upload1, #upload2, #upload3, #upload4, #upload5")
	.attr("src", "assets/images/order/noimage.png")
	.attr("width", 80)
	.attr("height", 80);

function favicon(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$("#favicon")
				.attr("src", e.target.result)
				.attr("width", 100)
				.attr("height", 100);
		};
		reader.readAsDataURL(input.files[0]);
	}
}

$(".favicon").change(function () {
	favicon(this);
});

function logo(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$("#logo")
				.attr("src", e.target.result)
				.attr("width", 100)
				.attr("height", 100);
		};
		reader.readAsDataURL(input.files[0]);
	}
}

$(".logo").change(function () {
	logo(this);
});

function logoLogin(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$("#logoLogin")
				.attr("src", e.target.result)
				.attr("width", 100)
				.attr("height", 100);
		};
		reader.readAsDataURL(input.files[0]);
	}
}

function landing(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$("#landing").attr("src", e.target.result).attr("width", 510);
		};
		reader.readAsDataURL(input.files[0]);
	}
}

$(".landing").change(function () {
	landing(this);
});

$(".logo-login").change(function () {
	logoLogin(this);
});

$(document).on("click", ".modalpaid", function () {
	var id = $(this).data("id");
	var no = $(this).data("ordernumber");
	$("#paid1").text(no);
	$("#paid2").val(id);
});

$(document).on("click", ".modalunpaid", function () {
	var id = $(this).data("id");
	var no = $(this).data("ordernumber");
	$("#unpaid1").text(no);
	$("#unpaid2").val(id);
});

function showPass() {
	var pass = document.getElementById("password");
	if (pass.type === "password") {
		pass.type = "text";
		$("#passIcon").html('<i class="feather icon-eye"></i>');
	} else {
		pass.type = "password";
		$("#passIcon").html('<i class="feather icon-eye-off"></i>');
	}
}
$("#passIcon").html('<i class="feather icon-eye-off"></i>');

function addSubPackage() {
	var id = document.getElementById("id").value;
	var inputbaru;
	var idf;
	inputbaru =
		"<div class='row' id='txtField" +
		id +
		"'><div class='col-sm-7'><div class='form-group'><label>Name Sub Package</label><input type='text' class='form-control' name='sub_package[" +
		id +
		"]' placeholder='Name Sub Package' required=''/></div></div><div class='col-sm-4'><div class='form-group'><label>Price</label><input type='text' class='form-control' name='price[" +
		id +
		"]' placeholder='25.50' required=''/><small class='text-c-red'>Use point (.)</small></div></div><div class='col-sm-1'><div class='form-group'><label>Delete</label><button type='button' class='btn btn-danger btn-view-detail-user del-sub' title='Delete' onclick='delSubPackage(txtField" +
		id +
		"); return false;'><i class='fas fa-times-circle' style='margin: 0 !important;'></i></button></div></div></div>";
	$("#pemain").append(inputbaru);
	idf = parseInt(id) + 1;
	document.getElementById("id").value = idf;
	console.log("price" + id);
}

function delSubPackage(id) {
	$(id).remove();
	var id = document.getElementById("id").value;
	idf = parseInt(id) - 1;
	document.getElementById("id").value = idf;
}

function delSub(id) {
	$.post("delete/del-sub", { id: id }, function () {
		location.reload();
	});
}

$(document).ready(function () {
	$("#masa").on("change", function () {
		var masa = $(this).val();
		if (masa == "D") {
			$("#setByDate1, #setByDate2").show();
		} else {
			$("#setByDate1, #setByDate2").hide();
		}
	});

	$("#level").on("change", function () {
		var nilai = $(this).val();
		if (nilai == "user") {
			$("#divLeader").show();
		} else {
			$("#divLeader").hide();
		}
	});

	$(document).on("click", ".addSubPackage", function () {
		var pkg = $(this).data("package");
		$("#ModaltxtPackage").html('<b>"' + pkg + '"</b>');
	});

	$(document).on("click", ".modalDeleteUser", function () {
		var id = $(this).data("id");
		$("#idUser").val(id);
	});

	$(document).on("click", ".modalResetUser", function () {
		var id = $(this).data("id");
		var idnohash = $(this).data("idnohash");
		$("#idUserReset").val(id);
		$("#idUserReset2").val(idnohash);
	});

	$(document).on("click", ".modalDeleteId", function () {
		var id = $(this).data("id");
		$("#idCode").val(id);
	});

	$(document).on("click", ".modalDeleteZone", function () {
		var id = $(this).data("id");
		$("#idZone").val(id);
	});

	$(document).on("click", ".modalDeleteSegment", function () {
		var id = $(this).data("id");
		$("#idSegment").val(id);
	});

	$(document).on("click", ".modalDeleteType", function () {
		var id = $(this).data("id");
		$("#idType").val(id);
	});

	$(document).on("click", ".modalDeleteCat", function () {
		var id = $(this).data("id");
		$("#idCat").val(id);
	});

	$(document).on("click", ".modalDeleteAnnc", function () {
		var id = $(this).data("id");
		$("#idAnnc").val(id);
	});

	$(document).on("click", ".modalDraftAnnc", function () {
		var id = $(this).data("id");
		$("#idDraftAnnc").val(id);
	});

	$(document).on("click", ".modalPublishAnnc", function () {
		var id = $(this).data("id");
		$("#idPublishAnnc").val(id);
	});

	$(document).on("click", ".btnPendingEditOrder", function () {
		$("#jenisStatus").val("pending");
	});

	$(document).on("click", ".btnPending", function () {
		var order = $(this).data("order");
		$("#uploadRemarksNum").text(order);
		$("#jenisStatus").val("pending");
	});

	$(document).on("click", ".btnProses", function () {
		var order = $(this).data("order");
		$("#uploadRemarksNum").text(order);
		$("#jenisStatus").val("proses");
	});

	$(document).on("click", ".btnPaidUploadReceipt", function () {
		var order = $(this).data("order");
		$("#uploadReceiptNum").text(order);
		$("#jenisStatus").val("paid");
	});

	$(document).on("click", ".btnUnpaidUploadReceipt", function () {
		var order = $(this).data("order");
		$("#uploadRemarksNum").text(order);
		$("#jenisStatus").val("unpaid");
	});

	$(".uploadEdit1").change(function () {
		$("#uploadEdit1").html(
			'<img src="../assets/images/setting/check-1.png" class="img-responsive m-r-10" alt="file1" width="30px" height="30px">File ready to upload, click Upload File'
		);
	});

	$(".uploadEdit2").change(function () {
		$("#uploadEdit2").html(
			'<img src="../assets/images/setting/check-1.png" class="img-responsive m-r-10" alt="file2" width="30px" height="30px">File ready to upload, click Upload File'
		);
	});

	$(".uploadEdit3").change(function () {
		$("#uploadEdit3").html(
			'<img src="../assets/images/setting/check-1.png" class="img-responsive m-r-10" alt="file3" width="30px" height="30px">File ready to upload, click Upload File'
		);
	});

	$(".uploadEdit5").change(function () {
		$("#uploadEdit5").html(
			'<img src="../assets/images/setting/check-1.png" class="img-responsive m-r-10" alt="file5" width="30px" height="30px">File ready to upload, click Upload File'
		);
	});

	$(".uploadEdit5").change(function () {
		$("#uploadEdit5").html(
			'<img src="../assets/images/setting/check-1.png" class="img-responsive m-r-10" alt="file5" width="30px" height="30px">File ready to upload, click Upload File'
		);
	});

	$("#uploadEdit1, #uploadEdit2, #uploadEdit3, #uploadEdit4, #uploadEdit5")
		.attr("src", "../assets/images/order/noimage.png")
		.attr("width", 80)
		.attr("height", 80);

	$(document).on("click", ".editId", function () {
		$(".editIdInput").hide();
	});

	$(document).on("click", ".btnDeletePackage", function () {
		var id = $(this).data("id");
		$("#idPackage").val(id);
	});

	$(document).on("click", ".addSubPackage", function () {
		var nilai = $(this).data("id");
		$("#idPkg").val(nilai);
	});

	// TAMBAHAN
	$(document).on("click", ".btnChangePwd", function () {
		var id = $(this).data("idd");
		var hash = $(this).data("hash");
		console.log(id);
		$("#idUserChg").val(id);
		$("#idHash").val(hash);
	});

	$(
		"#modalPending, #modalPress, #modalProses, #modalPaidUploadReceipt, #modalPress, #modalUnpaidUploadReceipt, #modalCancel, #modalPresales, #modalOSSGFORM, #modalPortFull, #modalRejectUploadReceipt"
	).on("hidden.bs.modal", function (e) {
		$(
			"#msgpending, #msgspora, #msgproses, #msgpaid, #msgspora, #msgunpaid, #msgcancel, #msgpresales, #msgossgform, #msgportfull, #msgreject"
		).html("");
	});
});

// TAMBAHAN
function show() {
	var x = document.getElementById("input1");
	var c = document.getElementById("t_pwd");
	if (x.type === "password") {
		x.type = "text";
		c.innerHTML = "Hide";
	} else {
		x.type = "password";
		c.innerHTML = "Show";
	}
}

function show2() {
	var x2 = document.getElementById("input2");
	var c2 = document.getElementById("t_pwd2");
	if (x2.type === "password") {
		x2.type = "text";
		c2.innerHTML = "Hide";
	} else {
		x2.type = "password";
		c2.innerHTML = "Show";
	}
}

function show3() {
	var x3 = document.getElementById("input3");
	var c3 = document.getElementById("t_pwd3");
	if (x3.type === "password") {
		x3.type = "text";
		c3.innerHTML = "Hide";
	} else {
		x3.type = "password";
		c3.innerHTML = "Show";
	}
}

function show4() {
	var c4 = document.getElementById("t_pwd4");
	var x4 = document.getElementById("input4");
	if (x4.type === "password") {
		x4.type = "text";
		c4.innerHTML = "Hide";
	} else {
		x4.type = "password";
		c4.innerHTML = "Show";
	}
}
